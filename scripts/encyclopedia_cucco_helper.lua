function Encyclopedia.RegisterSprite(gfxroot, anmtoplay, anmframe, newspr, layer, returnobject)
	local NewSprite = Sprite()
	NewSprite:Load(tostring(gfxroot), true)
	
	if not anmframe then
		NewSprite:Play(anmtoplay and tostring(anmtoplay) or NewSprite:GetDefaultAnimationName(), true)
	else
		NewSprite:SetFrame(anmtoplay and tostring(anmtoplay) or NewSprite:GetDefaultAnimationName(), anmframe)
	end
	
	if newspr then
		if layer == true then
			for i = 0, NewSprite:GetLayerCount() - 1 do
				NewSprite:ReplaceSpritesheet(i, newspr)
			end
			NewSprite:LoadGraphics()
		else
			NewSprite:ReplaceSpritesheet(layer or 0, newspr)
			NewSprite:LoadGraphics()
		end
	end

	return returnobject and {Sprite = sprite, Anm2Root = gfxroot, SprRoot = newspr, AnmName = anmtoplay, AnmFrame = anmframe or 0} or NewSprite
end

local ItemConfig = Isaac.GetItemConfig()
function Encyclopedia.GetMaxCollectibleID()
    local id = CollectibleType.NUM_COLLECTIBLES - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetCollectible(id+step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.GetMaxTrinketID()
    local id = TrinketType.NUM_TRINKETS - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetTrinket(id + step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.GetMaxCardID()
    local id = Card.NUM_CARDS - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetCard(id + step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.GetMaxPillID()
    local id = PillEffect.NUM_PILL_EFFECTS - 1
    local step = 16
    while step > 0 do
        if ItemConfig:GetPillEffect(id + step) ~= nil then
            id = id + step
        else
            step = step // 2
        end
    end
    
    return id
end

function Encyclopedia.pairsByKeys(t, f)
	local a = {}
	for n in pairs(t) do table.insert(a, n) end
	table.sort(a, f)
	local i = 0 
	return function()
		i = i + 1
		if a[i] == nil then return nil
		else return a[i], t[a[i]]
		end
	end
end
