Encyclopedia.TrinketsWiki = {
	TRINKET_SWALLOWED_PENNY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns one coin of any type upon taking damage."},
			{str = "- [REP] When held by Keeper and Tainted Keeper, spawns 0-1 coins."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Swallowed Penny can also be found by destroying urns and special shopkeepers."},
			{str = "Though similar to the Piggy Bank item, the Swallowed Penny is not limited to just pennies."},
			{str = "[-REP] Unlike Piggy Bank, playing as Keeper with this trinket always drops a coin, allowing semi-infinite life."},
			{str = "- A trick for farming money when playing as Keeper is to find a room with some static hazard like spikes and taking damage, causing a coin to appear. A penny will only refill life while a nickel will give four cents, and a dime will give nine cents."},
			{str = "- This trick can also be used to survive multiple uses of a sacrifice room, healing with the dropped coin between each sacrifice."},
			{str = "- [REP] As it can now give 0 coins when playing as the Keeper, this no longer works. When combined with Piggy Bank, you will still gain money on average, but you are no longer guaranteed a payout. Using IV Bag as your source of damage increases the average amount of coins per damage to 1.5, meaning you can usually generate a pool of coins you can use during an unlucky break or for later healing."},
		},
	},
	TRINKET_PETRIFIED_POOP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases pickup drop rate from poops to 50%."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Can be used when fighting bosses that spawn poop like Larry Jr. or Dingle, although the drop rate is greatly decreased in these rooms."},
			{str = "Very effective with A Card Against Humanity."},
			{str = "Not affected by Luck."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is used in Challenge no.2 (High Brow)."},
			{str = "This trinket seems to be in reference to Coprolites, very old pieces of poop that have become fossilized over a very long time"},
		},
	},
	TRINKET_AAA_BATTERY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "If an activated item would regain its penultimate charge upon clearing a room, it is instead fully charged. This effectively reduces its charge time by one bar."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Does not affect items with only one bar of charge or those with automatic recharge."},
			{str = "Recharging an item by taking damage with Habit, the instant charge given by 9 Volt, or any source of charge other than clearing a room, will not activate the trinket's effect."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Schoolbag: The trinket activates based on the primary item slot, but it grants an extra point of charge to the second item as well."},
			{str = "9 Volt: If active item requires three or more charges, trinket gives an extra charge, effectively utilizing itself with 9 Volt."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "The Battery: No effect on the second level charge."},
			{str = "Watch Battery: Watch Battery triggering after clearing a room will also trigger AAA Battery, provided the activated item was missing exactly 3 charges prior to clearing the room."},
		},
	},
	TRINKET_PURPLE_HEART = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles the chance for champion enemies to appear."},
			{str = "Also increases the chance of encountering champion bosses."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Purple Heart is a military decoration awarded to soldiers who are wounded or killed while serving in the U.S. Military."},
		},
	},
	TRINKET_BROKEN_MAGNET = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Pulls all types of coins towards Isaac. Ignores all other pickups."},
		},
	},
	TRINKET_BROKEN_REMOTE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Triggers the Teleport effect whenever an activated item is used."},
		},
		{ -- Infinite Synergies (Best)
			{str = "Infinite Synergies (Best)", fsize = 2, clr = 3, halign = 0},
			{str = "42 - Bob's Rotten Head (works before throwing the head)"},
			{str = "49 - Shoop Da Whoop! (works before firing the laser)"},
			{str = "133 - Guppy's Paw (works after converting the health, but still works with no red hearts)"},
			{str = "137 - Remote Detonator (works even if there are no bombs planted in the room)"},
			{str = "147 - Notched Axe (works before swinging the axe)"},
			{str = "164 - The Candle (works before firing the candle)"},
			{str = "177 - Portable Slot (works after consuming a coin, but still works with 0 coins)"},
			{str = "282 - How to Jump (works before jumping)"},
			{str = "289 - Red Candle (works before firing the candle)"},
			{str = "290 - The Jar (works before putting hearts in the jar)"},
			{str = "295 - Magic Fingers (works after consuming a coin, but still works with 0 coins)"},
			{str = "296 - Converter (works after converting the health, but still works with no soul hearts)"},
			{str = "338 - The Boomerang (works before throwing the boomerang)"},
			{str = "352 - Glass Cannon (works before shooting)"},
			{str = "396 - Ventricle Razor (works after creating a portal)"},
			{str = "434 - Jar of Flies (works after releasing the flies)"},
			{str = "487 - Potato Peeler (works after converting the health, but still works with no soul hearts)"},
			{str = "512 - Black Hole (works before shooting the hole)"},
			{str = "604 - Mom's Bracelet (works immediately without having to pick up a rock)"},
			{str = "623 - Sharp Key (works before firing a key)"},
			{str = "640 - Urn of Souls (works after capping/uncapping the urn)"},
			{str = "653 - Vade Retro (works even if there are no souls to explode on the screen)"},
			{str = "728 - Gello (works before firing the familiar)"},
			{str = "729 - Decap Attack (works before firing the familiar)"},
		},
		{ -- Infinite Synergies (With Delay)
			{str = "Infinite Synergies (With Delay)", fsize = 2, clr = 3, halign = 0},
			{str = "294 - Butter Bean"},
			{str = "323 - Isaac's Tears"},
			{str = "326 - Breath of Life"},
			{str = "383 - Tear Detonator"},
			{str = "484 - Wait What?"},
			{str = "504 - Brown Nugget"},
			{str = "507 - Sharp Straw"},
			{str = "522 - Telekinesis"},
			{str = "635 - Stitches"},
			{str = "705 - Dark Arts"},
			{str = "709 - Suplex! (you need to double-tap move)"},
			{str = "722 - Anima Sola"},
		},
		{ -- Infinite Synergies (Conditional)
			{str = "Infinite Synergies (Conditional)", fsize = 2, clr = 3, halign = 0},
			{str = "40 - Kamikaze! (only with Pyromaniac or Host Hat)"},
			{str = "126 - Razor Blade (only with a way to regenerate your health or prevent the damage)"},
			{str = "135 - IV Bag (only with a way to regenerate your health or prevent the damage)"},
			{str = "186 - Blood Rights (only with a way to regenerate your health or prevent the damage)"},
			{str = "536 - Sacrificial Altar (only if you have no familiars)"},
		},
		{ -- Infinite Synergies (Pre-Repentance)
			{str = "Infinite Synergies (Pre-Repentance)", fsize = 2, clr = 3, halign = 0},
			{str = "130 - A Pony (no longer teleports before the charge is expended)"},
			{str = "181 - White Pony (no longer teleports before the charge is expended)"},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Upon using The Bible while fighting Mom, Isaac will get teleported out of the room before the death animation is finished, resulting in the boss room being reset."},
			{str = "This item can only teleport Isaac to rooms that can be shown on the map. It is not possible to teleport to the I AM ERROR room, Devil Room, Angel Room, Crawl Space, Black Market, Boss Rush, the room with the access to ???, or Mega Satan."},
			{str = "During The Beast fight, Broken Remote will teleport you randomly on the screen."},
			{str = "Spin to Win does not work at all with Broken Remote."},
			{str = "Mine Crafter does not work with Broken Remote if there is already a TNT barrel in the room."},
			{str = "For some reason, Mom's Bracelet produces a faster teleport with Broken Remote than with any other item."},
		},
	},
	TRINKET_ROSARY_BEAD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases the chance to have an Angel Room replace a Devil Room, from 50% (base chance) to 75%."},
			{str = "Increases the chance to find The Bible to 45% in the Library and in the Shop to 13%."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If a Devil Deal or [-REP] Black Market deal has been paid, an Angel Room will not spawn, even with Rosary Bead."},
			{str = "This item does not increase the chance for an Angel/Devil Room Door to spawn when a Boss is killed, unlike Goat Head or The Book of Belial."},
			{str = "Despite indicating ''Faith Up'', there is no ''Faith'' stat in the game. ''Faith Up'' only serves to inform the player that it is linked to Angel Rooms in an unspecified way."},
		},
	},
	TRINKET_CARTRIDGE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a chance to trigger The Gamekid effect upon taking damage."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The activation chance depends on the luck stat and goes up to 100% with 38 Luck."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Cartridge resembles an NES (Nintendo Entertainment System) cartridge."},
			{str = "The art depicted on the cartridge is a picture of Isaac, implying that it is a copy of The Binding of Isaac in the form of an NES game."},
		},
	},
	TRINKET_PULSE_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Tears pulsate in size between 1x to 1.6x as they travel. The effect changes the hitbox of the tear but does not affect damage."},
		},
	},
	TRINKET_WIGGLE_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac's tears travel in a wave-like pattern."},
			{str = "+0.4 tears up."},
			{str = "- [-AB+] The tears up is not applied until another item updates Isaac's tears stat."},
			{str = "[REP] Grants spectral tears while held."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Wiggle Worm was a passive item in the original Binding of Isaac. The likely reason for the change to a trinket is the situational usefulness of this shot pattern, along with the new ability to drop trinkets on demand."},
		},
	},
	TRINKET_RING_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac's tears travel in a high-speed spiral pattern."},
			{str = "[REP] Grants spectral tears."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The tear pattern slightly increases Isaac's effective range."},
			{str = "[-REP] The trinket has no effect on any non-tear based attack, such as Brimstone or Mom's Knife."},
			{str = "- [REP]The trinket now affects all tear/non-tear modifiers, with the only exceptions being Dr. Fetus, Epic Fetus and Tech X, which don't work."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Backstabber: Due to the unusual tear travel, Ring Worm may be used to hit enemies in the back, without having the need to be placed behind them."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The bullet pattern might be a reference to a power-up in the NES game Uncle Fester's Quest."},
			{str = "Ring worm (or Dermatophytosis) is a fungal infection very common in children and teenagers. It involves scaly rings on the skin of the scalp, chest, arms, stomach and other places, and is not caused by any sort of worm despite its name."},
		},
	},
	TRINKET_FLAT_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Makes Isaac's tears 50% wider, giving them an oblong appearance."},
			{str = "Increases tears' natural knockback slightly."},
		},
	},
	TRINKET_STORE_CREDIT = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "While holding the trinket, all items in the shop are free."},
			{str = "- After purchasing an item, the trinket disappears."},
			{str = "- This trinket does not work on Devil Deals, I AM ERROR rooms or the Black Market."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Smelter/Marbles/Gulp!: Store Credit's effect will only happen once after being absorbed."},
		},
	},
	TRINKET_CALLUS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Prevents damage from creep and floor spikes."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Also grants immunity to slowdown from black and white creep, but not from Cobwebs."},
			{str = "Does not prevent any damage from Curse Room doors."},
			{str = "Does not prevent damage from Spiked Chests."},
			{str = "Does not prevent damage from Spiked Rocks."},
			{str = "Does not prevent damage from Greed mode's stop button."},
			{str = "[-REP] Prevents the use of Sacrifice Rooms, unlike flight."},
			{str = "- [REP] Can use Sacrifice Rooms now."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "A callus is an area of thickened skin that forms as a response to repeated friction, pressure, or other irritation."},
		},
	},
	TRINKET_LUCKY_ROCK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Rocks and their variants drop 1 penny when destroyed."},
			{str = "- [REP] Rocks and their variants have a 33% chance to drop 1 penny when destroyed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The spawned coin is always a regular penny, it cannot be a (sticky) nickel, dime, or lucky penny."},
			{str = "Coins from obstacles are dropped from any source that destroys them, including enemies such as the Round Worm."},
		},
	},
	TRINKET_MOMS_TOENAIL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's foot comes down every full minute (based on the in-game timer), dealing 300 damage."},
			{str = "- Still hurts Isaac for a heart."},
			{str = "- Can blast open doors."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] As the foot counts as an explosion, the damage can be avoided with Pyromaniac or Host Hat."},
			{str = "Unlike the foot spawned by II - The High Priestess or Mom's Shovel, this foot doesn't target enemies nor Isaac."},
		},
	},
	TRINKET_BLACK_LIPSTICK = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles the chance of Black Hearts appearing."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Black Feather: Black Feather grants a damage boost for Black Lipstick, provided the player has an extra trinket slot or has gulped or used Smelter on either of the trinkets."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Despite its pickup quote, this trinket does not alter the chance of finding Devil Rooms in any way."},
			{str = "Black hearts are still very rare even with this trinket equipped."},
		},
	},
	TRINKET_BIBLE_TRACT = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases chance for Eternal Hearts to spawn."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Despite its pickup quote, this trinket does not alter the chance of finding Angel Rooms in any way."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is a reference to religious tracts."},
		},
	},
	TRINKET_PAPER_CLIP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Allows Isaac to unlock golden chests without using keys."},
			{str = "- Locked doors, key blocks, and mega chests still require keys."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The pickup quote of this item is a reference to an infamous line from the first Resident Evil game."},
		},
	},
	TRINKET_MONKEY_PAW = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "If Isaac has half a heart or less after taking damage, this trinket drops a black heart."},
			{str = "The effect can trigger up to three times, after which the trinket will disappear."},
			{str = "- The fingers stuck out by the paw will change to display the number of triggers left."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The effect can trigger more than once in the same room."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Scapular: If the Scapular's effect is triggered, the soul heart awarded will not cancel the effect of Monkey Paw."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This item is a reference to the supernatural short story The Monkey's Paw by W.W Jacobs."},
		},
	},
	TRINKET_MYSTERIOUS_PAPER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "On every frame, it has a random chance to grant the effect of one of the following items/trinkets:"},
			{str = "- The Polaroid"},
			{str = "- The Negative"},
			{str = "- A Missing Page"},
			{str = "- Missing Poster"},
			{str = "If Isaac does not already have the item/trinket, then he will be given the item (but only for that particular frame). This means that Isaac has a chance to go to Chest or the Dark Room without The Polaroid or The Negative, he has a chance to revive as The Lost upon death, and so forth."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[REP] This trinket cannot be used to substitute The Polaroid or The Negative to open the strange door."},
			{str = "[REP] The black heart damage buff of A Missing Page will work, provided a heart/item affected by it will trigger in the frame it's active."},
		},
	},
	TRINKET_DAEMONS_TAIL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "80% of heart drops are replaced with keys."},
			{str = "All hearts drops turn into Black Hearts, if possible. (Sometimes heart types are tied to the room, such as in certain Super Secret Rooms.)"},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Objects and items such as tinted rocks, Red Chests, Fortune Telling Machines, tarot cards, Little C.H.A.D., Bloody Penny, or Blue Fire Places can't drop Black Hearts. Thus, they will still drop Soul/Red Hearts with Daemon's Tail."},
			{str = "All heart drops from Ace of Hearts will be Black Hearts."},
			{str = "Sources of heart drops that are reduced include cleared rooms, bosses, Slot Machines, Fortune Telling Machines, Beggars, Fire Places, Chests, Red Chests, poops, tinted rocks, items ( Gimpy, Old Bandage, Fanny Pack), trinkets ( Bloody Penny), and familiars ( Little C.H.A.D., The Relic, etc)."},
			{str = "- Hearts that are present upon entering a room for the first time are unaffected by the reduction (for example, the ones found in Curse/Devil/Angel Rooms), but will be converted to Black Hearts."},
			{str = "-- The exceptions to this are particular Super Secret Rooms, which have the effect of forcing all heart drops to be the kind found within them."},
			{str = "- The V - The Hierophant and VI - The Lovers tarot cards have a chance to drop one heart instead of two. The hearts they drop will not be Black Hearts."},
			{str = "-- [AB+] The cards drop two of their respective hearts every time."},
			{str = "Things that can drop pickups other than hearts will keep the same chance to drop a consumable, but with an increased chance for drops other than hearts and a decreased chance for heart drops. In particular, this increases the chances of acquiring:"},
			{str = "- Tarot cards and trinkets from Fortune Telling Machines"},
			{str = "- Bombs and Keys from tinted rocks"},
			{str = "- Pills and items from Red Chests"},
			{str = "- Coins from poops and Fire Places"},
			{str = "- Any pickup from Slot Machines, cleared rooms, beggars, chests, and the Fanny Pack"},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Black Feather: Black Feather grants a damage boost for Daemon's Tail, provided the player has an extra trinket slot or has gulped/smelted either of the trinkets."},
		},
		{ -- Tips
			{str = "Tips", fsize = 2, clr = 3, halign = 0},
			{str = "Due to its specific mechanic, Daemon's Tail has no synergy with items increasing chances for Soul/Eternal/Black Hearts to drop instead of a Red Heart, like Mitre, Mom's Pearl, or Bible Tract."},
			{str = "Since the frequency of heart drops is lowered, holding this trinket can be dangerous to Isaac if he has numerous Red Heart containers, as he can easily find himself short on health."},
			{str = "- This trinket is more effective for ??? or Azazel, as they start with no Red Heart containers."},
			{str = "Mom's Pearl or Bible Tract are trinkets which only need to be held to be useful, whereas Daemon's Tail is more effective if it is held and dropped at the right times, to get an increased chance for consumables from certain mechanics (to counter a lack of bombs/keys/coins, or to get Black Heart protection), and to still get enough heart drops to stay alive at other times (from bosses or familiars)."},
			{str = "- This trinket nearly requires this kind of strategy to pay off, making it tough to use as the player will have to sort and remember the moments where they must pick up or drop the trinket, as it interacts with nearly every consumable dropping mechanic."},
		},
	},
	TRINKET_MISSING_POSTER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-AB] If Isaac dies to the center spikes in a Sacrifice Room while holding it, a particular puzzle piece will be displayed as the cause of his death on his last will."},
			{str = "[AB] Dying to the center spikes in a Sacrifice Room while holding it will unlock The Lost."},
			{str = "[AB] If Isaac dies while holding this trinket and has already unlocked The Lost, he will be revived in the previous room as The Lost and Missing Poster will be consumed."},
			{str = "- This counts as a free extra life for The Lost."},
			{str = "- [REP] If Tainted Lost dies while holding the trinket, he does not become the normal Lost and is revived as is."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "None of the effects will occur during any Challenges."},
			{str = "[AB] Missing Poster has the lowest priority of all resurrection items."},
			{str = "- [REP] Birthright, while playing as Tainted Lost, has a lower priority than missing poster."},
			{str = "[AB] The Lost will not be unlocked if the player is in a Victory Lap or RERUN."},
			{str = "[-REP] Even if Holy Mantle has been unlocked as one of The Lost's starting items, if any character dies while holding the Missing Poster, The Lost will not hold the Holy Mantle upon revival."},
			{str = "- [REP] Due to The Lost's rework in Repentance, his passive Holy Mantle trait carries over to Missing Poster as well, making it much safer to utilize."},
			{str = "[AB+] If Gulp!, Smelter, or Marbles were used to absorb the poster, its effects will apply and can be used to unlock The Lost and respawn (but the effect still only applies once)."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The first scenes in Ending 15 and Ending 17 show The Missing Poster pinned to a utility pole, with a silhouette of Isaac's mother who seems to be searching for Isaac in the background."},
			{str = "There are many puzzle pieces in total. Assembling all of the jigsaw puzzles and figuring out what they meant was initially designed to be a collaborative effort of the community. As such, Edmund and Tyrone had openly expressed disappointment towards the datamining."},
			{str = "The unlock pop-up of Missing Poster calls it ''Lost Poster''. Unknown if it's a mistake or a clever hint,"},
		},
	},
	TRINKET_BUTT_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon picking up a coin, Isaac farts, knocking back nearby enemies."},
			{str = "- [-REP] The fart does not deal any damage or inflict poison."},
			{str = "- [REP] The fart inflicts poison."},
			{str = "Greatly increases the chance of coins dropping from poop."},
		},
	},
	TRINKET_MYSTERIOUS_CANDY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac has a chance to fart or poop every 30 seconds. [-REP] Getting hit can also trigger the effect."},
			{str = "[REP] Farts deal 6 damage and blow away projectiles."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "With smart positioning, poops can be placed and bombed to create bridges for 1 tile gaps, allowing Isaac to reach otherwise unobtainable consumables."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Brown Cap: Poops may be used to access secret rooms and blow rocks and debris."},
			{str = "- + Yes Mother?: Not recommended. Poops will blow up as soon as they spawn."},
			{str = "- + Pyromaniac: Provides a slow, but effective source of healing."},
			{str = "Dirty Mind: Allows a steady supply of Dips, with time. Useful for later chapters."},
			{str = "Gigante Bean: Farts are affected, covering a wide area and dealing 10 damage instead."},
			{str = "Meconium: Poops spawned by Mysterious Candy have a chance to be black poop."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Hallowed Ground/ Midas' Touch: Poops spawned by the Candy will never be white or golden poops, respectively."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Mysterious Candy is a chocolate laxative."},
			{str = "[REP] This trinket is used in Scat Man (challenge no.36)."},
		},
	},
	TRINKET_HOOK_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Tears move shifting left to right forming a path of right angles."},
			{str = "+10 range."},
			{str = "[REP] Grants spectral tears."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Effective range remains the same because the range buff only compensates for the tears tracing a longer path."},
			{str = "Shooting with this trinket equipped when next to a wall or block may cause the tear to travel into the wall."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Azazel: Azazel gains the wider beam, but no range increase."},
			{str = "Brimstone: Effect applies to the Brimstone beam, effectively increasing the width of the beam. It also adds an extra tick to the beam."},
			{str = "Tiny Planet: No effect."},
			{str = "Tractor Beam: The beam overrides the worm's tear movement effect, but keeps the +10 range."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Hookworms are parasitic worms that can enter through the skin from dirt that contains their eggs and larvae."},
		},
	},
	TRINKET_WHIP_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Shot Speed by 0.5."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Whipworm is an intestinal parasite that most commonly infects dogs."},
		},
	},
	TRINKET_BROKEN_ANKH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "When held, grants a 22.22% chance to be revived as ??? upon death. It can occur multiple times in the same run."},
			{str = "- Resurrection occurs in the previous room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Resurrection items activate in a set order. Broken Ankh is 9th in this order before Judas' Shadow, Missing Poster and Birthright."},
			{str = "This trinket isn't affected by the luck stat."},
			{str = "[REP] Tainted ??? will respawn as Tainted ???."},
		},
	},
	TRINKET_FISH_HEAD = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Blue Fly every time Isaac takes damage."},
		},
	},
	TRINKET_PINKY_EYE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a 10% chance to shoot a poison tear, dealing damage over time when inflicted."},
			{str = "The activation chance depends on the Luck stat and goes up to 100% with 18 Luck."},
		},
	},
	TRINKET_PUSH_PIN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a 10% chance to shoot a piercing and spectral tear."},
			{str = "The chance depends on luck and goes up to 100% at 18 luck."},
		},
	},
	TRINKET_LIBERTY_CAP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a chance of triggering one of the following effects for the current room upon entering it:"},
			{str = "- Mini Mush"},
			{str = "- Odd Mushroom (Large)"},
			{str = "- Odd Mushroom (Skinny)"},
			{str = "- [-REP] The Compass"},
			{str = "- [REP] Magic Mushroom"},
			{str = "- [REP] God's Flesh"},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If the Compass effect triggers while having the Treasure Map, the Compass effect is permanent for the remainder of the floor. (Other Mushroom effects will still apply in each room with their normal odds.)"},
			{str = "In addition to regular trinket drop sources, the Liberty Cap has a chance of dropping upon destroying a mushroom."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This item quote is a reference to the Yoshi's Island level 1-7: ''Touch Fuzzy Get Dizzy''."},
			{str = "Psilocybe semilanceata, commonly known as ''Liberty Cap'', is widely present in nature and is a very powerful psychedelic mushroom which contains psilocybin."},
			{str = "The Liberty Cap's ability to grant the Compass effect was a bug in the original Binding of Isaac which was carried over due to popularity with the fans."},
		},
	},
	TRINKET_UMBILICAL_CORD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] When Isaac's health is brought to a Half Red Heart and no Soul Hearts, a Little Steven familiar spawns for the current room."},
			{str = "[REP] When Isaac enters a room and has either a Half Red Heart or no Red Hearts at all, a Little Steven familiar spawns and will remain until red health is restored to above a Half Red Heart, disappearing after entering another room."},
			{str = "- Isaac can only gain one Little Steven familiar from this trinket."},
			{str = "- As The Lost or Tainted Lost, you will always gain a Little Steven."},
			{str = "[REP] When Isaac takes damage, high chance to spawn a Gemini familiar for the current room. This effect is stackable."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "An umbilical cord is a tube that is connected to a fetus' stomach and is used to give nutrients, blood, and oxygen to the growing child."},
		},
	},
	TRINKET_CHILDS_HEART = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a 10% chance of changing the room drop reward to a heart."},
			{str = "Opening chests and destroying Tinted Rocks, Slot Machines or Fortune Telling Machines have a chance of adding a heart to the rewards."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is used in Cursed! (challenge no.10) and Bloody Mary (challenge no.37)"},
		},
	},
	TRINKET_CURVED_HORN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases damage by 2, after normal calculations but before damage multipliers."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Normally, extra damage is reduced the more damage Isaac gets. Since Curved Horn is applied after every calculation, it will always be +2 damage."},
			{str = "Picking up Curved Horn while having damage multipliers, like Magic Mushroom or Cricket's Head, makes Curved Horn even more beneficial from the multiplier (bringing, in the case of a x1.5 multiplier, the damage it grants to +3)."},
			{str = "Curved Horn's damage does not ignore damage reduction multipliers, such as Odd Mushroom (damage x 0.9 - 0.4 damage down), Technology 2 (-35% damage down), and Soy Milk (-80% damage down)."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is a curved ram horn, similar to those granted by Aries or shown on The Lamb."},
		},
	},
	TRINKET_RUSTED_KEY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a 10% chance of changing the room drop reward to a key."},
			{str = "Opening chests and destroying tinted rocks, Slot Machines or Fortune Teller Machines has a chance of adding a key to the rewards."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "In the original Binding of Isaac, this trinket was broken and didn't do anything. This was eventually fixed in the Eternal Edition update, which was released after Rebirth."},
		},
	},
	TRINKET_GOAT_HOOF = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases speed and maximum speed by 0.15."},
		},
	},
	TRINKET_MOMS_PEARL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a secondary chance of any non-specific heart drop turning into a soul heart."},
		},
	},
	TRINKET_CANCER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "-2 tear delay (increased rate of fire)"},
			{str = "- Since this directly affects tear delay instead of changing the tears stat, it allows Isaac's rate of fire to go over the normal maximum rate."},
			{str = "- Tear delay decreases are more noticeable and are a greater increase to DPS when the tears stat is high."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Cancer's tear delay reduction scales with the current tear modifier's fire rate multiplier, the only exception being Chocolate Milk."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The pickup quote is a tribute to the community of the original game. Due to the usefulness of the trinket in the original The Binding of Isaac, it became a popular joke to consider ''getting cancer'' as a good thing."},
		},
	},
	TRINKET_RED_PATCH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a chance of increasing damage by 1.8 for the current room upon taking damage."},
			{str = "The chance is affected by luck and goes up to 100% at 8 luck."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Can trigger multiple times in the same room and stack with itself or Razor Blade which provides a similar effect."},
		},
	},
	TRINKET_MATCH_STICK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a 10% chance of changing the room drop reward to a bomb."},
			{str = "Opening chests and destroying tinted rocks, Slot Machines or Fortune Teller Machines has a chance of adding a bomb to the rewards."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[AB] Match Stick is the only trinket able to replace the Tick."},
			{str = "- This does not work if Isaac has Mom's Purse or Belly Button."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The pickup quote is a reference to The Simpsons."},
			{str = "This item is used in Challenge no.9 (Demo Man)."},
			{str = "The Match Stick being able to replace the Tick in Afterbirth is a reference to a common remedy in removing a tick from one's skin, which is putting a flame beside it until the tick disengages."},
		},
	},
	TRINKET_LUCKY_TOE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "+1 Luck."},
			{str = "33% chance to drop 1 additional Pickup when opening a golden chest, destroying a Machine, or blowing up a tinted rock."},
			{str = "Increases chances of getting a room clearing drop (independent of the luck stat)."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Lucky Foot: The extra luck-independent chance of room clearing drops from both Lucky Toe and Lucky Foot applies only in a diminished form, unless at zero or negative luck."},
		},
	},
	TRINKET_CURSED_SKULL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "If Isaac has less than one full Red Heart after taking damage, he will be immediately teleported to a random room."},
			{str = "- [AB+] The teleportation happens only at half a heart or less left, regardless of health type. Empty Bone Hearts are not counted as health even if they can absorb a hit."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Scapular: Provided Scapular can always trigger, grants effective immortality, as the soul heart is always given before the teleport takes place."},
			{str = "- + The Wafer: Grants true immortality, plus one hit on the soul heart, allowing the player to get hit once before teleporting."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Does not affect The Lost, as being hit with Holy Mantle does not count as taking damage."},
			{str = "It is not recommended to have this item equipped during a boss fight as the boss's health will be restored to 100% if the player is teleported out of the room and returns, placing the player at a disadvantage unless they have acquired a Placenta."},
		},
	},
	TRINKET_POLAROID_OBSOLETE = {
		-- I don't think this is used at all?
	},
	TRINKET_SAFETY_CAP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a 10% chance of changing the room drop reward to a pill."},
			{str = "Opening chests and destroying Tinted Rocks, Slot Machines or Fortune Tellers has a chance of adding a pill to the rewards."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Quote is likely a reference to the song ''Don't Swallow The Cap'' by The National"},
		},
	},
	TRINKET_ACE_SPADES = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a 10% chance of changing the room drop reward to a tarot or playing card."},
			{str = "Opening chests and destroying Tinted Rocks, Slot Machines or Fortune Telling Machines has a chance of adding a card to the rewards."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Ace of Spades is often seen as the most powerful and highest card in an average 52 card deck."},
			{str = "The Ace of Spades is found many times on the battlefield, especially American soldiers. It was typically shown as a foreshadowing of death and war."},
		},
	},
	TRINKET_ISAACS_FORK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a luck-dependent chance of being healed 1/2 a Red Heart upon clearing a room."},
			{str = "- The chance goes up to 100% at 18 luck."},
		},
	},
	TRINKET_MISSING_PAGE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[REP] Adds +40 damage to The Necronomicon and similar effects, such as Black Hearts, essentially doubling it."},
			{str = "Grants a chance to deal 40 damage to all enemies in the room on being hit, similar to The Necronomicon."},
			{str = "- The activation chance depends on the luck stat, and goes up to around 50% with very high Luck (between 50-60 luck)."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Blood Rights/XIII - Death/Dry Baby/The Negative: Doubles damage of these items, from 40 to 80."},
			{str = "- + Missing Page 2: Triples damage, from 40 to 120."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This item is not triggered when a hit is absorbed by Holy Mantle, making it useless while playing with The Lost."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Necronomicon, A Missing Page, and Missing Page 2 are all references to the Necronomicon and its missing pages appearing in Evil Dead."},
			{str = "- Which, in turn, is a reference to the Necronomicon, a fictional grimoire appearing in the works of horror writer H.P. Lovecraft."},
		},
	},
	TRINKET_BLOODY_PENNY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a chance to drop a Half Red Heart when money is collected."},
			{str = "- [-REP] 50% chance."},
			{str = "- [REP] 25% chance."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[AB] It can be found by destroying special shopkeepers."},
			{str = "[AB] This trinket is very useful in Greed mode, as the coins dropped for completing a wave now provide health."},
			{str = "[AB+] Makes the Ultra Greedier fight trivial, as many of his attacks drop coins, thus providing free health."},
		},
	},
	TRINKET_BURNT_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a chance to drop a Bomb when money is collected."},
			{str = "- [-REP] 50% chance."},
			{str = "- [REP] 25% chance."},
			{str = "- These bombs may be any normal type with their respective usual chances: Bombs, Troll Bombs, or Golden Bombs."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Rarely drops from TNT barrels."},
			{str = "[AB] Can drop out of special shopkeepers."},
			{str = "Can show up in the Beans! challenge, without being unlocked."},
		},
	},
	TRINKET_FLAT_PENNY = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a chance of spawning a Key upon collecting any coin."},
			{str = "- [-REP] 50% chance."},
			{str = "- [REP] 25% chance."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[AB] Flat Penny can drop out of special shopkeepers."},
		},
	},
	TRINKET_COUNTERFEIT_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Every time the player picks up a coin of any kind, there is a 50% chance that the player will receive a single additional penny."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "In addition to regular drop options for all trinkets, this trinket can be found in golden poop, by killing a Beggar, or winning at a Shell Game."},
			{str = "[AB] Counterfeit Penny allows the chance for Keeper to gain the full value of a coin while healing."},
			{str = "[REP] If picked up by Jacob and Esau, only the brother holding Counterfeit Penny can trigger the effect."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: No effect."},
		},
	},
	TRINKET_TICK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Removes 15% of any enemy's health if they have over 60 HP."},
			{str = "Gives Isaac one Red Heart if he enters an uncleared Boss Room."},
			{str = "Once picked up, it can't be removed."},
			{str = "- [AB] The Tick can be replaced by Match Stick."},
			{str = "-- The Tick cannot be replaced with the Match Stick if Isaac has Mom's Purse or Belly Button."},
			{str = "- [AB+] The Tick can be removed by absorption by Marbles, Smelter and Gulp!."},
			{str = "- [REP] The golden version can be removed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket can be used to infinitely replenish health if Isaac has access to any infinitely usable teleportation effect (such as Broken Remote + How to Jump)."},
			{str = "Only works on the first stage of multistage bosses."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "[AB] Being able to replace the Tick with Match Stick is a reference to how one can remove a tick that has latched onto their skin in real life with a heated tip of a match stick, however, removing a tick like this can expose oneself to disease-causing bacteria present in the tick. It is also a feature that was carried over from the original The Binding of Isaac, though it's unclear why it wasn't originally added in Rebirth."},
			{str = "The Tick is the only trinket that passively changes Isaac's appearance."},
			{str = "The Tick bears similarity to a real-life deer tick."},
			{str = "This trinket is used in The Host (challenge no.18)."},
		},
	},
	TRINKET_ISAACS_HEAD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac's severed head will follow him as a familiar firing piercing tears that deal 3.5 damage with each shot."},
			{str = "- [-REP] The head shoots once per second."},
			{str = "- [REP] The head shoots once every 2/3rds of a second."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "While holding this trinket, if there's another familiar that follows Isaac's Head, dropping this trinket will move the familiar to the first familiar slot."},
			{str = "- This resets upon quitting and re-entering the game."},
			{str = "- This is useful with familiars that are more effective when closer to Isaac, such as Lil Brimstone and Incubus."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "In the original Binding of Isaac, Isaac's Head was unlocked by beating??? (Boss) as Isaac."},
		},
	},
	TRINKET_MAGGYS_FAITH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives an eternal heart at the start of every floor."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The eternal heart is added directly to Isaac's heart containers, as opposed to appearing as a pickup."},
			{str = "The effect will also be triggered when the floor is reset (for example, with Forget Me Now)."},
			{str = "Eden will not be given the eternal heart on the first floor in case they start with this trinket."},
			{str = "[AB+] When held as The Forgotten, the eternal heart will be given to whichever character is being controlled when entering the next floor."},
			{str = "[REP] When going though the Ascent, eternal hearts will be given for each floor traveled up."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Maggy's Faith is most likely a reference to Mary Magdalene's faith in Jesus. Mary was said to be one Jesus trusted most and she stayed with Him past the abandonment of His disciples, as well as being the first person He showed Himself to after His resurrection.[1]"},
			{str = "''Faith's reward'' may constitute a subversion of the adage ''Faith is its own reward'', although use of the word 'faith' in the game is conflicted, as it also used in the quote for the trinket ''Rosary Bead'', in reference to Angel Rooms."},
			{str = "[REP] This trinket is used in Baptism by Fire (challenge no.38)."},
		},
	},
	TRINKET_JUDAS_TONGUE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Items in the Devil Room only cost 1 heart container."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Soul heart trades still cost 3 Soul hearts, rendering this item useless for characters that can only have soul or bone hearts."},
			{str = "Judas' Tongue can retroactively change the price of devil deals."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Keeper's Bargain: The tongue provides no discounts for any item costing money."},
		},
	},
	TRINKET_SOUL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a familiar that floats slowly around the room on a zig-zag pattern similar to The Peeper and fires spectral and homing tears that deal 3.5 damage."},
			{str = "- [-REP] The familiar shoots once per second."},
			{str = "- [REP] The familiar shoots once every 2/3rds of a second."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Unlike The Peeper, ???'s Soul does no contact damage."},
			{str = "[-REP] ???'s Soul is unable to shoot while flying over obstacles (which is the same behavior that Ghost Baby has)."},
			{str = "???'s Soul doesn't duplicate when using Box of Friends."},
		},
	},
	TRINKET_SAMSONS_LOCK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "1/15 chance to increase damage by 0.5 for the current room each time an enemy dies."},
			{str = "- Samson's Lock can activate up to 10 times per room."},
			{str = "- The activation chance depends on the Luck stat and goes up to 100% with 10 Luck."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "According to the Bible, Samson was given supernatural strength by God to fight his enemies, the Philistines, and perform heroic acts, but when he was tricked into allowing his hair to be cut by a Philistine woman named Delilah, Samson was powerless."},
			{str = "A lock of hair is a piece(s) of hair which has been cut from a human head, typically being held together in some manner."},
			{str = "This trinket is a reference to the effect of Bloody Lust (Samson's starting item) in the original game, which was later changed in Rebirth and added in Afterbirth as the item Lusty Blood."},
		},
	},
	TRINKET_CAINS_EYE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "When starting a floor, 25% chance to get the The Compass effect for the duration of a floor."},
			{str = "- [AB+] The chance increases based on the luck stat, and maxes out at 100% at 3 luck."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Due to the very low luck requirement, this trinket will service well any explorer, effectively saving 15 cents in the long run."},
		},
	},
	TRINKET_EVES_BIRD_FOOT = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Chance to spawn a Dead Bird familiar for the current room each time an enemy dies."},
			{str = "- The Dead Bird deals 4 damage per second."},
			{str = "- The chance increases with luck, starting at 5% at 0 luck and reaching 100% at 8 luck."},
			{str = "- The familiar spawns already activated, with no taking damage required unlike the item."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket alone cannot spawn more than one Dead Bird per room. It can, however, stack with Dead Bird to spawn two in one room."},
			{str = "The bird familiar spawned by this trinket is transparent and has one missing leg compared to the one spawned by the actual item."},
		},
	},
	TRINKET_LEFT_HAND = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Replaces all types of chests with red chests."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] The effect includes chests already spawned. Entering a room that previously contained other types of chests while holding The Left Hand will turn them into Red Chests permanently."},
			{str = "- [REP] Chests that have already spawned are not converted into red chests."},
			{str = "It is advised to drop The Left Hand (by holding either CTRL key for few seconds, holding R2 or RT for 3 seconds on PS4 and Vita and Xbox One, respectively, or holding ZR on the New Nintendo 3DS / Nintendo Switch for a few seconds) before entering Chest, as it will convert the golden chests there which always contain free items."},
			{str = "- [-REP] The player may prefer red chests for their possible soul hearts or red chest items, at the expense of guaranteed items."},
			{str = "- [REP] Golden chests converted in Chest's starting room will contain Devil deals, allowing for more powerful items, but at the cost of hearts."},
			{str = "--Red Chests found past the starting rooms of either Chest or Dark Room will contain regular red chest drops, dropping the trinket is advised."},
			{str = "[REP] Stone Chests are affected by The Left Hand."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Gilded Key: Overridden by The Left Hand."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Left Hand is likely a reference to a myth that left handed people were witches in league with the devil."},
			{str = "The Left Hand is the only item in the game with 2 separate conditions for unlocking it, being able to be unlocked by killing Ultra Pride, or by defeating??? in the Chest as Judas"},
		},
	},
	TRINKET_SHINY_ROCK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Tinted rocks and rocks with crawl spaces will flash white when Isaac enters the room, then once every 10 seconds afterward."},
		},
	},
	TRINKET_SAFETY_SCISSORS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Troll bombs turn into bomb pickups before they explode."},
			{str = "- Mega troll bombs turn into 1+1 free bombs."},
			{str = "- [REP] Golden troll bombs turn into Golden bombs before they explode."},
			{str = "- [REP] Giga bombs (thrown by Bombgaggers and Ultra War) turn into giga bomb pickups."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The disarming effects of the troll bombs will apply to Chub when she swallows it, as the bomb pickups will come out shortly after, not damaging her."},
			{str = "Players can farm large amounts of bomb pickups if used against Little Horn, War, Pride, or other Troll Bomb spawning enemies or items like Anarchist Cookbook."},
			{str = "Does not affect Wrath's bombs."},
			{str = "- [AB+] Super Wrath's bombs turn into their respective pickups."},
			{str = "Does not affect Big Horn's hot bombs."},
			{str = "[REP] Can be used to farm Bomb Bum for unlimited bombs."},
		},
	},
	TRINKET_RAINBOW_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac's tears gain a worm trinket effect that changes every 3 seconds of the in-game timer. The effects include:"},
			{str = "- Pulse Worm"},
			{str = "- Ring Worm"},
			{str = "- Tape Worm"},
			{str = "- Whip Worm"},
			{str = "- Wiggle Worm"},
			{str = "- Flat Worm"},
			{str = "- Hook Worm"},
			{str = "- Lazy Worm"},
			{str = "The order of the effects is always as above, in a cycle."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is used in Challenge no.28 (PRIDE DAY!)."},
			{str = "Rainbow worm can't give you the effects of worms you're currently holding (with Mom's Purse or Belly Button) or ones that were gulped."},
			{str = "- Therefore, somehow gulping all the worm trinkets in one run makes Rainbow Worm useless."},
		},
	},
	TRINKET_TAPE_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] x2 range."},
			{str = "[REP] +3 range."},
			{str = "x0.5 tear height."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Azazel's Brimstone range as well, but doesn't double it."},
			{str = "Causes Dr. Fetus to shoot bombs very fast and far."},
			{str = "Only tear height affects [-REP] Incubus and Fate's Reward, usually lowering effective range."},
			{str = "[-REP] Reduces the effective range of Ipecac shots."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Tapeworm is a well-known species of Cestoda, a class of parasitic flatworms of the phylum Platyhelminthes."},
		},
	},
	TRINKET_LAZY_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] -0.40 Shot Speed."},
			{str = "[REP] -0.50 Shot Speed."},
			{str = "[-REP] +4.0 Range up."},
			{str = "+2.0 Tear Height."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Godhead - Tears will take longer to hit enemies, increasing the damage dealt by the aura."},
			{str = "The Ludovico Technique - Decreased shot speed makes it harder to keep the tear on enemies."},
			{str = "My Reflection / Pop! / Lachryphagy - Decreases effective range."},
		},
	},
	TRINKET_CRACKED_DICE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "When Isaac takes damage, one of the following dice effects occurs:"},
			{str = "- The D6"},
			{str = "- D8"},
			{str = "- D12"},
			{str = "- D20"},
			{str = "-- If Cracked Dice selects a die that doesn't affect anything in the room, nothing happens."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket can be used in conjunction with the IV Bag or a Blood Donation Machine to generate unlimited pickups and items."},
		},
	},
	TRINKET_SUPER_MAGNET = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Pickups and enemies are attracted to Isaac."},
			{str = "- The attraction enables pickups to pass over pits and rocks, but not through Key Blocks."},
			{str = "- Hearts are only attracted if Isaac could pick them up with his existing heart containers."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Does not pull other trinkets, chests or grab bags."},
			{str = "If the player has The Ludovico Technique while also holding the Super Magnet, the speed at which the tear slowly crawls nearer to you is increased."},
			{str = "One tactic for using the Super Magnet is to drop it in the floor's starting room and come back for it once rooms are cleared. This allows it to be used to attract unreachable pickups without the downside of attracting enemies."},
			{str = "While fighting Ultra Greed, the doors are affected by the magnet as well."},
		},
	},
	TRINKET_FADED_POLAROID = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Occasionally causes Isaac to blend into the ground, which briefly confuses all enemies in the room."},
			{str = "- Confusion is only applied once Isaac begins fading into the background. Any enemy that spawns while Isaac is already faded won't be confused until Isaac fades in, then out again."},
			{str = "[REP] Can be used to open the Strange Door in The Depths to Mausoleum/ Gehenna II, instead of The Polaroid or The Negative, destroying the trinket in the process."},
			{str = "- This won't work if the trinket was consumed by ''Gulp!'' pill, Marbles or Smelter."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "[REP] Faded Polaroid being used to open the Strange Door is a nod to a mechanic in the original Binding of Isaac: Wrath of The Lamb flash game, where The Polaroid was a trinket in that game and was required to access The Chest after beating Cathedral."},
		},
	},
	TRINKET_LOUSE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Occasionally spawns a Blue Spider."},
			{str = "- Effect only occurs when in an uncleared room."},
			{str = "- After the first spider spawns, there is a 50% chance of one spawning after 30 seconds and a guaranteed spawn a minute later."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The phrase ''Itchy, tasty...'' is a reference to the Keeper's Diary from Resident Evil. The Diary describes the slow transformation of a researcher into a zombie as his entries become less and less coherent due to neural decay caused by t-Virus infection. His final entry consists of two words: ''Itchy. Tasty.''."},
		},
	},
	TRINKET_BOBS_BLADDER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Bombs leave green creep underneath themselves."},
			{str = "[REP] The creep deals 23.3 damage per tick."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Remote Detonator: Allows for maximization of creep damage, potentially dealing more damage over time to a boss than just a singular bomb."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Bombs will leave a trail of creep when moved or pushed around."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Bob's Bladder has the same visual pattern as Monstro's Lung."},
		},
	},
	TRINKET_WATCH_BATTERY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases the chance of a Little Battery pickup after clearing a room by 6.67%."},
			{str = "Increases the chance of random pickups being a Little Battery by 2%."},
			{str = "Has a 5% chance to add an extra charge to Isaac's activated item upon completing a room."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "AAA Battery: Watch Battery triggering after clearing a room will also trigger AAA Battery, provided the activated item was missing exactly 3 charges prior to clearing the room."},
		},
	},
	TRINKET_BLASTING_CAP = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Upon exploding, bombs have a 10% chance to drop a Bomb pickup."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "With a Golden Bomb and a lot of patience, 99 Bombs can be generated."},
			{str = "Has no effect with bombs produced with Dr. Fetus."},
		},
	},
	TRINKET_STUD_FINDER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Gives rocks an additional chance to reveal a Crawl Space when broken."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Stud Finder's effect is applied when a rock is destroyed and does not depend on the rock's location."},
			{str = "- Any of the rocks already seen before picking up Stud Finder can be affected by the trinket."},
			{str = "- Shiny Rock doesn't synergize with Stud Finder as there is no set location for the rocks that will reveal a Crawl Space when destroyed with Stud Finder."},
			{str = "- Unlike Tinted Rocks and normal Crawl Space rocks whose generation is dependent on the room they're in, Stud Finder's Crawl Space generation effect mainly depends on the number of rocks destroyed, which means that the more rocks destroyed, the higher the chance to find a Crawl Space."},
			{str = "The chance increases with Mom's Box."},
		},
	},
	TRINKET_ERROR = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a random trinket effect for each room."},
			{str = "- There is no indication as to which trinket effect Isaac gets."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Each room is locked to a single trinket effect, meaning that even if Isaac exits and re-enters a room, that room will always have the same trinket effect."},
			{str = "In rooms where the Tick effect is given, Isaac is unable to swap Error with another trinket."},
			{str = "If the Store Credit effect is given in a Shop, all items will be free and Error will not be consumed."},
			{str = "If the Monkey Paw effect is given, Error will never be consumed and can continue to spawn Black Hearts indefinitely."},
			{str = "If the Walnut effect is given, the trinket will not be consumed and will continue to spawn items upon taking damage."},
			{str = "- Touching a Stoney and Movable TNT counts as taking damage and will spawn a lot of items."},
			{str = "If the Your Soul effect is given, all Devil Deals will be free and Error will not be consumed."},
			{str = "Error can not give the effects of trinkets Isaac is currently holding (with Mom's Purse or Belly Button) or ones that were gulped."},
			{str = "- Therefore, somehow gulping all the trinkets in one run will make this trinket useless."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Error 404 is a common network error when a user attempts to follow a broken or dead link."},
		},
	},
	TRINKET_POKER_CHIP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "If a chest would contain hearts, coins, bombs and/or keys, it has a 50/50 chance to either drop extra consumables or replace all of its contents with an Attack Fly."},
			{str = "The trinket has no effect on Red Chests, or if the chest would contain a pill, a card, a trinket or an item."},
			{str = "- Consequently, the Chip has no effect in Chest or Dark Room."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Guppy's Eye: Provided the Poker Chip is not smelted, it can be used in conjunction with the eye to open all chests with extra pickups, while dropping the chip for any chests containing flies, nullifying the downside of the trinket."},
		},
	},
	TRINKET_BLISTER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases the knockback of Isaac's tears."},
		},
		{ -- Tips
			{str = "This trinket is useful against chasing enemies and enemies that explode upon death such as Mullibooms or Boom Flies."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "8 Inch Nails/ Pisces: The knockback from Blister is stronger than from the items, and knockback boosts do not stack."},
			{str = "Brimstone: The beam pushes back enemies."},
			{str = "The Ludovico Technique: The controlled tear gains knockback."},
			{str = "Mom's Knife: The knife gains knockback."},
			{str = "Piercing tears: Tears can again knock enemies back."},
			{str = "Tech.5/ Tech X/ Technology/ Technology 2: Pushes the enemies back along the lasers. For Tech X, the direction is counter-clockwise around the ring."},
			{str = "Trisagion: Adds knockback, which can deal devastating damage to enemies caught in the blast."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "In real life, a blister is a small pocket of body fluid that forms within the upper layers of the skin."},
		},
	},
	TRINKET_SECOND_HAND = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Increases the time that status effects stay on enemies."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "The Bean / The Black Bean / Contagion: The poison duration from these items is not affected by Second Hand."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Second Hand's appearance was changed in Booster Pack no.5."},
		},
	},
	TRINKET_ENDLESS_NAMELESS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "When Isaac uses a consumable item like a pill or card, there is a 25% chance to spawn another copy of it."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Endless Nameless is especially useful when used with item multiplier playing cards."},
			{str = "The new consumable will spawn before the effect goes through. This means teleportation will leave the new copy in the previous room, and item conversion or destruction cards like Ace of Diamonds and Black Rune will consume the new copy."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Vurp! + D20: Consuming two Vurp! pills consecutively spawns an infinite amount of Vurp! pills, which can then be transformed with the D20."},
			{str = "Golden Pills: A new golden pill may be spawned with each use, allowing for very easy exploitation. Works best with PHD/ Virgo, to avoid potential stat downgrades."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Endless Nameless is a reference to the hidden track of the same name on Nirvana's album, Nevermind."},
			{str = "The Pickup Quote is a reference to Time Fcuk, a 2009 flash game by Edmund McMillen."},
		},
	},
	TRINKET_BLACK_FEATHER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Increases Isaac's damage by 0.2 for each of the following items or trinkets he possesses:"},
			{str = "[REP] Increases Isaac's damage by 0.5 for each of the following items or trinkets he possesses:"},
			{str = "- Abaddon"},
			{str = "- Black Candle"},
			{str = "- Black Lipstick"},
			{str = "- Ceremonial Robes"},
			{str = "- Daemon's Tail"},
			{str = "- False PHD"},
			{str = "- Goat Head"},
			{str = "- Match Book"},
			{str = "- Missing Page 2"},
			{str = "- Safety Pin"},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Having more than one of the same item will still increase its damage."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Most items that are affected by Black Feather contain the phrase ''Evil up'' or ''Sin up'' in their description. The only exception is Goat Head."},
		},
	},
	TRINKET_BLIND_RAGE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles the invincibility frames Isaac gets after taking damage."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] This item makes it possible to use the Blood Donation Machine twice without taking damage the second time."},
			{str = "[-REP] This item makes it possible to use the Devil Beggar three times without taking damage the second and third times."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket refers to Samson being blinded after revealing his weakness to Delilah."},
		},
	},
	TRINKET_GOLDEN_HORSE_SHOE = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "+15% chance for a Treasure Room with two items to spawn on subsequent floors."},
			{str = "- Only one of the two items can be taken, the other will disappear."},
		},
	},
	TRINKET_STORE_KEY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Shops can be opened without using keys."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Keeper starts with this trinket after he defeats Satan."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Pay To Play: Overrides Store Key. Shops still require a coin to enter."},
		},
	},
	TRINKET_RIB_OF_GREED = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Prevents Greed and Super Greed from appearing in shops and secret rooms."},
			{str = "Increases the chance of coin drops when clearing rooms at the expense of heart drops."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Rib of Greed is picked up after a Greed fight in a shop or a secret room, reentering the room will turn the room into a regular shop with items for sale or a secret room respectively (while keeping the previously dropped items on the floor)."},
		},
	},
	TRINKET_KARMA = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Donating to a donation machine has a chance to do one of the following:"},
			{str = "- Heal Isaac for a full red heart"},
			{str = "- Spawn a beggar"},
			{str = "- Get a penny back, directly added to Isaac's total"},
			{str = "Increases the chance that donating to a Donation Machine will increase Isaac's luck."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Symbol is a Triskelion, also known as a Triskele, which is often used to represent karma."},
		},
	},
	TRINKET_LIL_LARVA = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns one or more Blue Flies from every poop Isaac destroys."},
		},
	},
	TRINKET_MOMS_LOCKET = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants half a red heart each time a key is used to unlock a door, golden chest or key block."},
			{str = "Turns half red heart pickups into full hearts. Half hearts already on the ground will also be upgraded upon re-entering the room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Having a Golden Key or Paper Clip will render the trinket unable to grant half hearts from keys."},
			{str = "Having Mom's Locket while playing Keeper will restore a full health coin when a key is used."},
			{str = "Donating keys to a Key Master won't heal Isaac."},
			{str = "A very useful trinket for unlocking the It's the Key achievement, in which Isaac cannot pick up coins, bombs, and most importantly hearts."},
			{str = "[REP] Hearts dropped from Tainted Magdalene's mechanic will be always full red hearts."},
		},
	},
	TRINKET_NO = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Prevents activated collectibles from being generated, if possible, from all item pools while this trinket is being held."},
			{str = "[REP] If multiple copies are held, rerolls items with quality 0."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Activated items will start appearing again once the item pool's non-activated items are exhausted or become scarce."},
			{str = "When the trinket is dropped, the item generation mechanics go back to normal, and active items will be generated normally again."},
			{str = "[-REP] Does not affect Libraries until all the books have been collected, as the library pool consists only of activated items."},
			{str = "Fixed boss drops and machines are not affected by NO!:"},
			{str = "- Yum Heart from Super Lust"},
			{str = "- Mr. Boom from Wrath"},
			{str = "- Anarchist Cookbook from Pride"},
			{str = "- Shoop Da Whoop! from Envy"},
			{str = "- Bob's Rotten Head from Sloth"},
			{str = "- A Pony and White Pony from Horsemen"},
			{str = "- Head of Krampus from Krampus"},
			{str = "- IV Bag from blood donation machine"},
			{str = "- Crystal Ball from fortune teller"},
			{str = "In runs where many items have been collected/rerolled, the trinket can function as a sort of warning to the player that an item pool will soon be exhausted if activated items begin to show up."},
			{str = "[REP] Book of Virtues will not be prevented from spawning, since it's treated as a passive item."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Items with 0 quality are rerolled."},
		},
	},
	TRINKET_CHILD_LEASH = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Causes Isaac's familiars to stay closer to Isaac."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Friendship Necklace: It's affected by Child Leash, resulting in familiars orbiting much closer to Isaac."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The pickup quote is a reference to the saying ''Keep your friends close, and your enemies closer''."},
		},
	},
	TRINKET_CRACKED_CROWN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] -20% tear delay (increased rate of fire)"},
			{str = "- [-REP] Since this directly affects tear delay instead of changing the tears stat, it allows Isaac's rate of fire to go over the normal maximum rate."},
			{str = "- [-REP] Tear delay decreases are more noticeable and are a greater increase to DPS when the tears stat is high."},
			{str = "Boosts speed, range, shot speed and damage if they are above their base values."},
			{str = "- [REP] Also boosts luck and fire rate."},
			{str = "- Affects stat bonuses from passive items, trinkets that are absorbed or in a second trinket slot and permanent bonuses given by items such as Void."},
			{str = "- [-REP] The extra stat upgrades are approximately 33% of the original."},
			{str = "- [REP] The extra stat upgrades are approximately 20% of the original."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket does nothing for the listed boostable stats if they are at or below the base value, the boost is calculated on the total bonus, not per item."},
			{str = "[-REP] Luck is not affected by Cracked Crown."},
			{str = "[REP] For damage upgrades, the 20% bonus is given to the change to Isaac's actual damage, not the damage upgrade itself."},
			{str = "[REP] For tears upgrades, the 20% bonus is given to the change to Isaac's tears fired per second, not the tears upgrade itself. Items that boost tears fired per second directly are unaffected."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Mom's Box: The tear delay reduction rises to about 36%, and the stat boost is increased to about 77%."},
		},
	},
	TRINKET_USED_DIAPER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives the Skatole effect in certain rooms. This chance is seeded into each room and isn't affected by Luck."},
			{str = "- For a list of what flies are affected and how, see the Skatole page."},
			{str = "Dropping the trinket will not revert Black Flies, but will undo the other benefits."},
		},
	},
	TRINKET_BROWN_CAP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Destroying poop causes it to explode, dealing [-REP] 60/[REP] 100 damage like a normal bomb. The explosion range is shorter, however."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Red Poops will not explode when destroyed."},
		},
	},
	TRINKET_MECONIUM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a chance for poop to turn into black poop."},
			{str = "Adds a chance for black poops to drop a Black Heart upon being destroyed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Not affected by Luck."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Meconium is the earliest stool of a mammalian infant."},
		},
	},
	TRINKET_FISH_TAIL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Many items and trinkets that generate Blue Flies or Locusts will generate twice as many."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Flies may spawn on top of each other, appearing as a single fly initially."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "7 Seals: No effect."},
		},
	},
	TRINKET_BLACK_TOOTH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a chance to fire a black tooth that poisons enemies."},
			{str = "- Black teeth deal double Isaac's normal tear damage."},
			{str = "- The chance scales with luck very slowly, with barely noticeable increase until close to 30 luck, and at 32 luck every tear is a tooth."},
		},
	},
	TRINKET_OUROBOROS_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Tears travel in a large spiral pattern."},
			{str = "Grants spectral tears."},
			{str = "+4.0 range."},
			{str = "+2.0 tear height."},
			{str = "A luck-based chance for a homing shot, reaching 100% at 9 luck."},
			{str = "- The homing shots are not different in appearance."},
			{str = "- The homing does not override the spiral pattern, making them still unreliable."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Dr. Fetus: Since the bombs don't follow the spiral pattern, only the extra range, spectral bombs, and chance for a homing bomb apply."},
			{str = "Tractor Beam: The tears follow a straight path, ignoring the spiral pattern, the tears also stall at a specific point giving a greater chance to land tears."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Azazel: Increases the range of his short-ranged Brimstone. No homing chance."},
			{str = "[-REP] Brimstone: Overrides Ouroboros Worm."},
			{str = "- [REP] The Brimstone laser is shot in a spiral pattern."},
			{str = "The Ludovico Technique: The tear spirals around in a difficult to control manner, with the added homing chance further complicating things."},
			{str = "[-REP] Mom's Knife: Overrides Ouroboros Worm."},
			{str = "- [REP] Works as intended. The homing, however, is questionable."},
			{str = "Proptosis: The curious pattern allows dealing full damage from a safer range."},
			{str = "[-REP] Tech X: Overrides Ouroboros Worm."},
			{str = "- [REP] Tech X now inherits the homing chance benefit, without being affected by the pattern."},
			{str = "[-REP] Technology: Grants spectral tears without a change in pattern or a chance of homing shots."},
			{str = "- [REP] The tech laser is now affected by the pattern. The homing DOES take affect, but it's fairly hard to notice."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is a reference to Ouroboros, which was a prototype of the game ''The End is Nigh'' by Edmund McMillen and Tyler Glaiel."},
			{str = "The Ouroboros is an ancient symbol, showing a serpent or dragon eating its tail."},
		},
	},
	TRINKET_TONSIL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "After taking 12-20 hits, a removed tonsil familiar starts following Isaac, blocking enemy shots."},
			{str = "- The effect can happen twice, after which the trinket will be destroyed."},
			{str = "- The familiar does not deal damage to enemies."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "When the familiar is generated, Isaac will gain the familiar permanently as a passive item. This allows Isaac to drop the trinket while keeping the familiar."},
			{str = "- The passive Tonsil item does not exist in any item pool, thus, this is the only way to find it and make it appear in the Collection Page aside from starting with it as Eden."},
			{str = "-- [REP] The Tonsil collectible has since been removed from the collection page and thus this is no longer necessary for collection page completion."},
			{str = "If Eden starts with the passive tonsil item and picks up the trinket, the trinket will spawn only one familiar before being consumed."},
			{str = "Sources of self-inflicted damage, like IV Bag or Blood Donation Machines, can spawn a familiar."},
			{str = "Damage that is blocked by Holy Mantle or the Holy Card does not count to the number of hits that are needed for the tonsil familiar to spawn."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Tonsils are soft tissue located near the back of the throat which, in the event of inflammation to the point of obstructing the throat, can be removed without much noticeable difference."},
		},
	},
	TRINKET_NOSE_GOBLIN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a chance to fire a homing booger tear that sticks to an enemy and deals damage over time."},
			{str = "Multiple boogers can stick to one enemy to deal extra damage."},
			{str = "Each booger deals damage equal to Isaac's tear damage every second."},
			{str = "The booger lasts for 60 seconds or until the enemy dies."},
			{str = "Boogers cannot pierce and stick to the first enemy that they come in contact with."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Nose Goblin's effect is almost identical to Sinus Infection beyond the added homing effect. Consequently, all its synergies apply to Nose Goblin."},
			{str = "- The effects have independent chances to proc. If Isaac has both, there is a chance to fire either homing or non-homing boogers."},
			{str = "Not affected by Luck."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Godhead: Shots retain their aura while stuck to an enemy, but the aura can only damage enemies other than the booger victim. If the enemy dies while duration remains, it will resume homing and will often stick to a second enemy."},
			{str = "Dr. Fetus: Bombs shot out can randomly become homing."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "[REP] Brimstone: When dealing damage, there is a chance for a booger tear to attach to the affected enemy. Sometimes the brim will snap because of its homing effect."},
			{str = "[-REP] Technology/Tech X: Nose Goblin has no effect."},
			{str = "[REP] Technology/Technology 2/Tech X: When dealing damage, there is a chance for a booger tear to attach to the affected enemy."},
			{str = "The Ludovico Technique: The tear has a random chance to turn green and disappear. This doesn't make the tear stick to enemies or grant any additional damage. The tear is respawned at Isaac's location."},
			{str = "[REP] Trisagion: Overridden by Trisagion"},
			{str = "[REP] Spirit Sword When dealing damage, there is a chance for a booger tear to attach to the affected enemy. Applies to both swings and projectile swords."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket's name and description appear to be a reference to the episode ''Nurse Stimpy'' from the cartoon ''Ren and Stimpy''."},
		},
	},
	TRINKET_SUPER_BALL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a chance to fire tears that bounce off of enemies and obstacles."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[REP] Tears will bounce off of walls while fighting The Beast, despite the lack of visible walls."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Bob's Rotten Head: The head will bounce off walls at high speeds when thrown."},
		},
	},
	TRINKET_VIBRANT_BULB = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases all stats while Isaac's active item is fully charged:"},
			{str = "- +0.25 speed."},
			{str = "- +0.75 range."},
			{str = "- +0.20 tears."},
			{str = "- +0.10 shot speed."},
			{str = "- +0.50 damage."},
			{str = "- +1 luck."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket creates no effect if Isaac has no active item."},
			{str = "[-REP] Active items that are reusable indefinitely without a charge bar, such as How to Jump, will allow Vibrant Bulb's effects to be permanent (until the active item is swapped out)."},
			{str = "Vibrant Bulb is slightly less than half as powerful as Dim Bulb, however, its effects are active more often."},
			{str = "Glowing Hour Glass, when used, will not make the Vibrant Bulb inactive and will still provide the bonus until it has one bar of charge."},
			{str = "Having additional charge beyond full with The Battery does not provide an additional bonus. It does give an advantage however, since using the fully charged item once still provides Vibrant Bulb's effect."},
			{str = "If two active items are held ( Schoolbag), the one currently active must be at full charge for the trinket to take effect (the other one doesn't need to be fully charged)."},
			{str = "[REP] Vibrant Bulb produces no effect with active items in consumable slots, e.g. Tainted Apollyon's Abyss or Tainted Bethany's Lemegeton."},
			{str = "[REP] If Bethany or Tainted Bethany are holding enough soul/red hearts to charge her current active item, the Vibrant Bulb will be active, even if there are no regular charges."},
			{str = "- If the active item is fully charged only with soul/blood charges and no regular charges, Vibrant Bulb and Dim Bulb can be both active at the same time if Isaac holds both of the trinkets."},
		},
	},
	TRINKET_DIM_BULB = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases all stats while Isaac's active item is completely uncharged:"},
			{str = "- +0.50 speed."},
			{str = "- +1.50 range."},
			{str = "- [-REP] +0.40 tears."},
			{str = "- [REP] +0.50 tears."},
			{str = "- +0.30 shot speed."},
			{str = "- +1.5 damage."},
			{str = "- +2 luck."},
			{str = "Items that charge automatically will get only a tiny moment of benefit from the Dim Bulb before they gain more charge again."},
			{str = "[-REP] If Isaac's active item has no charge bar and can used anytime, like How to Jump, Dim Bulb's effects will be permanent."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Dim Bulb is more than twice as powerful as Vibrant Bulb, however, its effects are active less frequently."},
			{str = "Dim Bulb has no effect when no active item is held."},
			{str = "If the player has 2 items (via Schoolbag), only 1 of them needs to be empty to receive the benefits."},
			{str = "- This appears to have been patched out on PC. However, if one of your two active items has no charge bar (e.g. Remote Detonator), you can swap to it after using the charged item to preserve the Bulb's effect."},
			{str = "[REP] Dim Bulb produces no effect with active items in consumable slots, e.g. Tainted Apollyon's Abyss or Tainted Bethany's Lemegeton."},
			{str = "[REP] The effect remains active with Bethany's soul charges and Tainted Bethany's blood charges."},
			{str = "- If the active item is fully charged only with soul/blood charges and no regular charges, Dim Bulb and Vibrant Bulb can be both active at the same time if Isaac holds both of the trinkets."},
		},
	},
	TRINKET_FRAGMENTED_CARD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Makes two Secret Rooms spawn upon entering a new floor with this trinket."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Only adds a Secret Room, not a Super Secret Room."},
			{str = "Fragmented Card only needs to be held when entering the floor. The Secret Room will remain after dropping the trinket, even if it hasn't been explored yet."},
			{str = "Has no effect in Greed mode."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Luna: The effects stack, spawning three secret rooms on the floor."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket is a torn card of XVIII - The Moon."},
		},
	},
	TRINKET_EQUALITY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Turn pickups into their doubled versions while Isaac's coins, bombs, and keys are equal in number."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Upon picking up a non-doubled pickup that makes Isaac's pickups equal in number, a doubled variant will be spawned as well."},
			{str = "Doubled pickups will not revert if Isaac's pickups become unequal."},
			{str = "This also affects Greed mode by doubling the coin pickups generated at the start of waves."},
			{str = "If keys, bombs or red hearts are doubled in a shop, they become free."},
		},
	},
	TRINKET_WISH_BONE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Taking damage has a 2% chance to destroy the trinket and spawn a random item using the item pool of the current room."},
		},
	},
	TRINKET_BAG_LUNCH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Taking damage has a 2% chance to destroy the trinket and spawn Lunch."},
		},
	},
	TRINKET_LOST_CORK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases the radius of all creep produced by Isaac and his familiars."},
		},
	},
	TRINKET_CROW_HEART = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Red Heart damage will be taken before Soul Heart, Black Heart, and Rotten Heart damage."},
			{str = "- With Bone Hearts, Red Hearts contained within the Bone Hearts will be taken before regular Red Heart damage, but the Bone Hearts will not be fully depleted."},
			{str = "- If Isaac only has half a Red Heart left, Soul and Black Heart damage will be taken, and Bone Hearts can be destroyed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] This is useful for getting into Boss Challenge Rooms when you want to maintain your Soul/Black Hearts but have more than one filled Red Heart container."},
			{str = "Red Heart damage taken via the trinket affects your Devil/Angel Room chances normally."},
			{str = "- As a result, this trinket makes Soul/Black Hearts useless at protecting Devil/Angel Room chances."},
			{str = "- Bone Hearts will be emptied before Red Hearts, making them very valuable to protect your Devil/Angel room chances."},
			{str = "This is useful when playing as The Forgotten, as it will preserve his Bone Hearts (and Devil/Angel Room chance) until all of his Bone Hearts are empty."},
			{str = "- This trinket does nothing while controlling The Soul."},
		},
	},
	TRINKET_WALNUT = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "After Isaac is hit by 1-9 explosions, the Walnut will be destroyed and spawns a coin, a heart, a key and another trinket. The pickups may be of any normal variety."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Taking damage from the explosion is not required to trigger the effect."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Bomber Boy: As Bomber Boy causes 9 explosions at once, laying a bomb and not moving before it explodes will always trigger the trinket."},
			{str = "Host Hat / Pyromaniac: Not only will Isaac become immune to explosions, but the amount of required hits is reduced to 1-5."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Smelter: Walnut will still only activate once after being absorbed."},
		},
	},
	TRINKET_DUCT_TAPE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Stops orbital familiars from rotating around Isaac."},
			{str = "Stops shooting familiars from moving."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Familiars are stopped in the exact position they are when Duct Tape is picked up. Position of familiars can be changed by dropping Duct Tape and picking it up again."},
			{str = "Blue Flies orbiting Isaac move less relative to Isaac."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Ball of Bandages/ Cube of Meat: No effect at level 3 and 4."},
			{str = "Big Chubby: Will still charge forward sluggishly, but return to the same spot each time."},
			{str = "Bob's Brain: Will still charge forward slowly, but return to the same spot each time."},
			{str = "Gemini: Will stay in place relative to Isaac, but the effect of chasing enemies still persists."},
			{str = "Jaw Bone: Will still charge forward slowly, but return to the same spot each time."},
			{str = "Little Chubby: Will still charge forward, but return to the same spot each time."},
			{str = "Psy Fly: Will stick wherever taped, but will still deflect bullets whenever possible."},
			{str = "Smart Fly: Will stay in place when orbiting Isaac but will still pursue enemies."},
		},
		{ -- No Effect
			{str = "No Effect", fsize = 2, clr = 3, halign = 0},
			{str = "The following familiars will not change their behaviour:"},
			{str = "???'s Soul'"},
			{str = "7 Seals"},
			{str = "Abel"},
			{str = "Angry Fly"},
			{str = "Bird Cage"},
			{str = "BBF"},
			{str = "Blood Puppy"},
			{str = "Bumbo"},
			{str = "Bum Friend"},
			{str = "Cain's Other Eye"},
			{str = "Cube Baby"},
			{str = "Daddy Longlegs"},
			{str = "Dark Bum"},
			{str = "Dead Bird"},
			{str = "Finger!"},
			{str = "Fruity Plum"},
			{str = "Guppy's Hair Ball"},
			{str = "The Intruder"},
			{str = "Key Bum"},
			{str = "King Baby"},
			{str = "Leech"},
			{str = "Lil Dumpy"},
			{str = "Lil Gurdy"},
			{str = "Lil Haunt"},
			{str = "Lost Fly"},
			{str = "Lost Soul"},
			{str = "Mom's Toenail"},
			{str = "Multidimensional Baby"},
			{str = "Obsessed Fan"},
			{str = "Papa Fly"},
			{str = "Pointy Rib"},
			{str = "The Peeper"},
			{str = "Punching Bag"},
			{str = "Quints"},
			{str = "Robo-Baby 2.0"},
			{str = "Samson's Chains"},
			{str = "Shade"},
			{str = "Sissy Longlegs"},
			{str = "Spider Mod"},
			{str = "Succubus"},
			{str = "Twisted Pair"},
			{str = "Worm Friend"},
			{str = "YO LISTEN!"},
		},
	},
	TRINKET_SILVER_DOLLAR = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Shops appear in Womb, Utero, and Scarred Womb."},
			{str = "- [REP] Also spawns Shops in Corpse"},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket must already be held upon starting the Womb/Utero/Scarred Womb/Corpse floor for the effect to take place, i.e. the trinket needs to be carried over from the previous floor."},
			{str = "Silver Dollar only needs to be held when entering the floor. The Shop will remain after dropping the trinket, even if it hasn't been explored yet."},
		},
	},
	TRINKET_BLOODY_CROWN = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Treasure Rooms appear in , Utero, and Scarred Womb."},
			{str = "- [REP] Treasure Rooms also appear in Corpse."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket must already be held upon starting the Womb/Utero/Scarred Womb floor for the effect to take place, i.e., the trinket needs to be carried over from the previous floor."},
			{str = "Bloody Crown only needs to be held when entering the floor. The Treasure Room will remain after dropping the trinket, even if it hasn't been explored yet."},
			{str = "XL Womb, Utero, Scarred Womb, and Corpse will have 2 treasure rooms, like any other XL floor."},
		},
	},
	TRINKET_PAY_TO_WIN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Restock Boxes always spawn in Treasure Rooms."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket's effect applies when the floor is generated, so it will only take effect if already held when entering a new floor. This means that finding it on a floor will not change the Treasure Room on that floor, nor will dropping it remove its effect on that floor's Treasure Room if Pay to Win was held when the floor was entered."},
			{str = "This trinket does not affect the two Treasure Rooms spawned in ???."},
			{str = "In Greed Mode, this affects both normal and silver types of Treasure Rooms."},
			{str = "[REP] When brought to the alternate path floors, there seems to be a high chance of the treasure room having one extra item available to be picked up alongside the usual choice of two items."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The item is a reference to microtransactions, which are common in modern video games. They serve the ''pay-to-win'' goal by making it either impossible or extremely tedious for players to either progress through the game (with so-called ''roadblocks'') or earn certain features such as weapons unless they purchase their ''way out.''"},
		},
	},
	TRINKET_LOCUST_OF_WRATH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering a room with enemies, spawns a Red Locust which detonates upon impact with enemies, dealing double tear damage for the impact of the Locust and 60 damage for the explosion."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] This trinket can be dangerous to hold, as the locust will instantly target the nearest enemy as Isaac enters the room, which can be close enough to catch Isaac in its blast radius."},
			{str = "The explosion takes most bomb effects from items that Isaac has, with some exceptions."},
			{str = "- The Locust does not gain additional explosion damage or radius from Mr. Mega."},
			{str = "- The explosion damage is halved to 30 by Bomber Boy without additional explosions."},
			{str = "-- [REP] Additional smaller explosions will spawn in a tight + formation."},
			{str = "- Mini bombs from Scatter Bombs are not spawned."},
			{str = "- Pickups will not spawn with Glitter Bombs."},
			{str = "-- [REP] Glitter bomb pickups have a chance to spawn."},
			{str = "- [REP] Brimstone Bombs will spawn thinner beams, dealing only 4.8 damage per tick."},
			{str = "The Locust will also spawn each time a wave in Boss Rush starts."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Hive Mind: The locust's impact damage is doubled and the explosion damage is increased to 85."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Before Booster Pack no.3, Locust of War was named Locust of Wrath. Unlike other Locusts, the name of Locust of Wrath was from one of seven Deadly Sins instead of four Harbingers."},
		},
	},
	TRINKET_LOCUST_OF_PESTILENCE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering a room with enemies, spawns a Green Locust that deals double Isaac's damage and poisons enemies, which deals double Isaac's damage over time."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The Locust will also spawn each time a wave in Boss Rush starts."},
			{str = "During Greed Mode, locusts will not spawn when waves start."},
		},
	},
	TRINKET_LOCUST_OF_FAMINE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering a room with enemies, spawns a Yellow Locust that deals double Isaac's damage and slows enemies."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The Locust will also spawn each time a wave in Boss Rush starts."},
			{str = "During Greed Mode, locusts will not spawn when waves start."},
		},
	},
	TRINKET_LOCUST_OF_DEATH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering a room with enemies, spawns a Black Locust that deals quadruple Isaac's damage, applied as two hits of double damage."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The Locust will also spawn each time a wave in Boss Rush starts."},
			{str = "The Locust will not spawn each time a wave in Greed Mode starts."},
			{str = "If the player has exited the game, the Locust will spawn upon continuing from the main menu, allowing multiple locusts to spawn for the same room by repeatedly exiting."},
		},
	},
	TRINKET_LOCUST_OF_CONQUEST = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering a room with enemies, spawns 1-4 White Locusts that deal double Isaac's damage."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The Locusts will also spawn each time a wave in Boss Rush starts."},
			{str = "The Locusts will not spawn each time a wave in Greed Mode starts."},
		},
	},
	TRINKET_BAT_WING = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon killing an enemy, has a chance to grant flight for the rest of the room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Destroying a Shopkeeper counts as a kill."},
		},
	},
	TRINKET_STEM_CELL = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Heals for half a Red Heart upon traveling to the next floor."},
			{str = "[REP] Heals Isaac's Red Heart health to 50% of maximum upon traveling to the next floor. If his health is already equal to or above 50%, it heals half a Red Heart instead."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Blood Oath: The healing will occur before any health is drained."},
			{str = "Tainted Bethany: Adds 1 to her ''blood charges'' resource."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "A Stem Cell is an undifferentiated cell of a multicellular organism that is capable of giving rise to indefinitely more cells of the same type, and from which certain other kinds of cells arise by differentiation."},
		},
	},
	TRINKET_HAIRPIN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Fully recharges Isaac's active item upon entering a boss fight."},
			{str = "- This effect only occurs the first time a Boss Room is entered, leaving and re-entering the Boss Room will not result in more recharges."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket's effect will trigger once for each and every Boss Room. Even if there are multiple Boss rooms on the same floor for whatever reason (Curse of the Labyrinth, The Void)"},
			{str = "In Greed mode, grants a full charge only for the two boss fights on the final level."},
			{str = "The effect is identical to picking up a battery, with caveats listed in interactions."},
			{str = "[REP] Does not fully recharge between Dogma and The Beast."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "The Battery: If the item doesn't have a full first charge level, it will be filled. If the first level is full, the second charge level will be filled."},
			{str = "- [REP] The item will gain both its charges."},
			{str = "Mega Blast: Mega Blast gains only 3 charge units."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Hairpin has been added in Booster Pack no.1, after the initial release of the DLC."},
			{str = "Hairpin is based on this Steam Workshop trinket by zarat.us."},
			{str = "Hairpin's original description was ''Poke that power outlet! Bosses charge you''."},
		},
	},
	TRINKET_WOODEN_CROSS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants a shield which nullifies a single hit once per floor."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The shield granted by Wooden Cross stacks with Holy Mantle and Blanket."},
			{str = "- It does not, however, stack with the Holy Card, both are treated as the same shield."},
			{str = "The shield of Wooden Cross has a higher priority than Holy Mantle or Blanket and will be lost first when hit."},
			{str = "[REP] Golden Wooden Cross has no additional effect."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Wooden Cross is based on a Steam Workshop trinket by cathery and Mills."},
		},
	},
	TRINKET_BUTTER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Using an active item will drop it onto a pedestal on the ground."},
			{str = "- [REP] This does not apply to permanent active items, which are located in the pill/card slot."},
			{str = "When Isaac takes damage, there is a 2% chance one of Isaac's passive items will drop on the ground."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Butter! allows the transportation of active item pedestal from room to room. On its own this ability is useless, but can be used to set up plays involving room-wide pedestal effects, after gathering all available active item pedestal in a single room, Isaac can then:"},
			{str = "- Use The D6, Dice Shard, Cracked Dice etc, to re-roll every single item pedestal in the room."},
			{str = "- Use Void, Black Rune, or Abyss to consume all pedestal for their respective benefits."},
			{str = "- Use Diplopia (or more riskily Crooked Penny ) to double all item pedestals before doing any of the above."},
			{str = "- Isaac can also damage himself to try to fish for the 2% chance of dropping a passive item in the same room as well."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Blank Card / Clear Rune / Placebo: Because the active item is dropped upon usage, the recharge time for them is reset to 4, allowing to use more expensive cards/runes much more frequently."},
			{str = "Void: Items that are normally unusable when holding Butter! can still activate their effects if absorbed by Void."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Consumable actives: Active items that remove themselves upon use do not drop and are consumed as normal."},
			{str = "Controllable actives: Active items that require additional input to control after being used (like The Candle or Breath of Life) create no effect and are dropped on the ground. They retain charge when picked up."},
			{str = "Familiar spawning actives: Active items that spawn Blue Flies, Blue Spiders, or items like Monster Manual will spawn those familiars before dropping."},
			{str = "- This applies to active items that spawn pickups."},
			{str = "Tainted characters with active items in their consumable slots can drop them from getting hit, however they will only be able to pick it back up into the active item slot."},
			{str = "The Bible: The flight granted by the Bible persists even though the item is dropped."},
			{str = "Glowing Hour Glass: Is removed from Isaac and dropped, but is given back to Isaac discharged upon going back in time into the previous room. The room in which Glowing Hour Glass was used will not contain another Glowing Hour Glass."},
			{str = "How to Jump: Isaac animates a jump but his movement remains normal and he cannot go over any obstacles."},
			{str = "Metronome: Effect from the Metronome is immediately canceled beyond the gain of hearts, coins, bombs, or keys. Visual effects may persist until leaving the room."},
			{str = "The Pinking Shears: The flight granted by The Pinking Shears persists, but the body familiar is immediately canceled."},
			{str = "Razor Blade: Deals damage to Isaac as normal but does not increase damage for the current room."},
			{str = "Tainted Cain: Getting hit may cause Tainted Cain to lose the Bag of Crafting. Trying to pick it back up will turn it into pickups, leaving Tainted Cain with no way to gain items."},
			{str = "Tainted Isaac: Using an active item drops it and begins the cycling between the used item and a new one, allowing to change an active item into virtually any other item."},
			{str = "Damocles: Getting hit may cause Isaac to drop an activated Damocles, one of the only ways to remove the item without the sword falling."},
		},
		{ -- No Effect
			{str = "No Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Butter! will immediately cancel the effects of and discharge these active items when dropped, unless they are consumed with Void:"},
			{str = "Anarchist Cookbook"},
			{str = "Black Hole"},
			{str = "Bob's Rotten Head"},
			{str = "The Book of Belial"},
			{str = "The Boomerang"},
			{str = "Breath of Life"},
			{str = "Broken Shovel"},
			{str = "Brown Nugget"},
			{str = "Crack The Sky"},
			{str = "D Infinity"},
			{str = "Doctor's Remote"},
			{str = "Friendly Ball"},
			{str = "The Gamekid"},
			{str = "Glass Cannon"},
			{str = "The Hourglass"},
			{str = "My Little Unicorn"},
			{str = "The Nail"},
			{str = "Notched Axe"},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Butter! has been added in Booster Pack no.1, after the initial release of the DLC."},
			{str = "Butter! was made by Florian Himsl (developer and programmer of the original The Binding of Isaac game)."},
		},
	},
	TRINKET_FILIGREE_FEATHERS = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Angels drop items from the Angel Room item pool instead of key pieces when defeated."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Items are dropped even if both key pieces are already acquired."},
			{str = "[-REP] If one key piece was acquired before picking up Filigree Feather, the next angel will drop the other key piece even with Filigree Feather equipped."},
			{str = "If both Angels are fought at the same time, only one item will be acquired."},
			{str = "- [REP] This seems to have been changed."},
			{str = "Angels rarely found in closets in the Cathedral do not drop items, but ones found in Super Secret Rooms do."},
			{str = "Angels spawned from Sacrifice Rooms will also drop items."},
			{str = "The Fallen Angels that spawn during the Mega Satan fight do not drop items."},
			{str = "The Angels that spawn during the Boss Rush do not drop items."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: No effect."},
			{str = "Golden Trinket: No effect."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Filigree Feather has been added in Booster Pack no.2, after the initial release of the DLC."},
		},
	},
	TRINKET_DOOR_STOP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "While held, the last door Isaac went through stays open, allowing him to leave the room even if enemies are in it."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Does not work on Secret Room openings."},
			{str = "This allows Isaac to leave rooms, including boss rooms, if the fight goes poorly."},
			{str = "- This becomes particularly useful in The Void, where there are multiple boss rooms, Door stop lets the player peek behind each boss room, and thus able to decide whether to face Delirium or other bosses first."},
			{str = "Leaving immediately after enemies have spawned gives the player information on the fight, potentially improving decision making."},
			{str = "If a Devil Room has a Krampus fight in it, Isaac may choose not to fight him and leave."},
			{str = "If an Angel fight is started in an Angel Room the door will close and prevent leaving despite the Door Stop."},
			{str = "If Greed spawns in a Secret Room or Shop, Door Stop can be used to leave and come back to fight Greed again to potentially get more than one Steam Sale."},
			{str = "Does not affect Mom's boss room."},
			{str = "Can be used to reset Holy Mantle shield. Very useful playing as The Lost."},
			{str = "[-REP] If a room contained champion enemies, upon leaving and re-entering the room, all previous champion enemies will have turned to regular enemies."},
			{str = "Cannot be used to escape Boss Rush after the fight has started."},
			{str = "Cannot be used to escape the Mega Satan fight."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Door Stop has been added in Booster Pack no.3, after the initial release of the DLC."},
			{str = "Door Stop is based on a Steam Workshop trinket by Strawrat and CupertinoEffect."},
		},
	},
	TRINKET_EXTENSION_CORD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Beams of yellow electricity flow between Isaac and his familiars, damaging any enemies between them."},
			{str = "- The beams do 6 points of damage per strike."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The Forgotten's body acts as a familiar while controlling The Soul, causing electricity to bounce to it. [REP] The same applies for Tainted Forgotten."},
			{str = "[REP] If taken as Jacob and Esau, Extension Cord will connect to familiars held by both characters, not just the familiars owned by the character holding the cord. It will also connect Jacob and Esau to each other, even if neither have any familiars."},
			{str = "- This also applies to co-op players and their familiars."},
			{str = "Does not affect temporary familiars."},
			{str = "The beams will damage destructible obstacles, such as TNT and Fire Places, as a normal tear would."},
			{str = "Does not affect familiars spawned with Quints."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Extension Cord has been added in Booster Pack no.4, after the initial release of the DLC."},
			{str = "Extension Cord is based on a Steam Workshop mod by Strawrat and CupertinoEffect, which was originally a passive collectible."},
		},
	},
	TRINKET_ROTTEN_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Blue Fly every time a coin is picked up."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Rotten Penny is based on a Steam Workshop item by Eufuu."},
		},
	},
	TRINKET_BABY_BENDER = {
		{ -- Effect
			{str = "Effect", fsize = 2, clr = 3, halign = 0},
			{str = "Grants Isaac's familiars homing shots."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Using I - The Magician with Tarot Cloth will unlock this item."},
			{str = "- [REP] Tarot Cloth with the card has a unique effect and does not count as two uses for unlocking purposes."},
			{str = "Using Telepathy for Dummies twice in the same room, either with Car Battery, ? Card or through a recharge, will unlock this item."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Baby-Bender was added in Booster Pack no.5, after the initial release of the DLC."},
			{str = "Baby-Bender is based on a Steam Workshop item by Ink Toodles and Del."},
		},
	},
	TRINKET_FINGER_BONE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "[-REP] Grants a 2% chance to gain a Bone Heart upon taking damage."},
			{str = "[REP] Grants a 5% chance to gain a Bone Heart upon taking damage."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Finger Bone has been added in Booster Pack no.5, after the initial release of the DLC."},
		},
	},
	TRINKET_JAW_BREAKER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a 10% chance to fire teeth that deal 3.2x Isaac's damage, similar to Tough Love."},
			{str = "- The chance for this to occur increases with Luck. It goes up to 100% at 9 luck."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is identical in function with Tough Love."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This item is a reference to a real-life candy also known as a ''gobstopper''."},
		},
	},
	TRINKET_BLESSED_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives a chance to drop a Half Soul Heart when money is collected. The chance is based on the Coin's worth[1]"},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is very useful in Greed Mode, as the constant supply of coins helps with accumulating lots of soul hearts."},
		},
	},
	TRINKET_BROKEN_SYRINGE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "When Isaac enters a new room, has a chance to give the effect of any syringes for the current room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Experimental Treatment activates, it will give 3 random stat increases and decreases, as opposed to 4 stat increases and 2 decreases. Health cannot be one of the stats it selects."},
			{str = "The chance for Broken Syringe to activate is unaffected by luck."},
		},
	},
	TRINKET_CHEWED_PEN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a 10% chance to fire tears that slow enemies."},
			{str = "- This chance is affected by Luck, and reaches 100% at 18 Luck"},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is extremely similar to the Game Squid trinket found in the Eternal Edition of the original The Binding of Isaac, which was never added to Rebirth."},
		},
	},
	TRINKET_FIRECRACKER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Bombs placed by Isaac explode in 0.66 seconds instead of 1.5 seconds."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Dr. Fetus: Bombs from Dr. Fetus also explode more quickly."},
			{str = "Epic Fetus: No effect."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket originated in Antibirth, where it was originally called Exploded Firecracker."},
		},
	},
	TRINKET_GIANT_BEAN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Makes farts (eg. Butter Bean) significantly larger."},
			{str = "- Also affects enemy farts."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mucormycosis: Spore explosions count as farts, and increase in size."},
		},
	},
	TRINKET_LIGHTER = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Has a chance to inflict burning on enemies upon entering a new room."},
			{str = "- The chance to inflict burning increases with luck. It goes up to 100% at 40 luck."},
			{str = "- The burn inflicted by Lighter deals Isaac's damage per tick, up to 5 times."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The description is likely a reference to Alfred's line in The Dark Knight ''Some men just want to watch the world burn.''"},
		},
	},
	TRINKET_BROKEN_PADLOCK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Doors that require a key can be opened with explosions."},
			{str = "- Also works on key blocks or golden chests."},
			{str = "- Does not work on the golden door that leads to Mega Satan."},
			{str = "- Doors that require two keys (Vaults and Dice Rooms) only require one bomb to open."},
			{str = "- The door to the Mausoleum/ Gehenna can also be entered with one bomb without the need to spend any hearts."},
			{str = "- The Strange Door can also be opened without the need to sacrifice The Polaroid/ The Negative"},
		},
	},
	TRINKET_MYOSOTIS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns pickups left on the previous floor in the starting room of the current floor."},
			{str = "- A maximum of 4 pickups can be spawned."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The ''previous floor'' status of trinkets can be reset by picking them up and dropping them again, allowing multiple other trinkets to be carried between floors."},
			{str = "Trinkets that were left in the Treasure Room still transform into Cracked Keys during the Ascent, potentially allowing you to gain large amounts of Cracked Keys."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Forget me not is a common name for the flower genus Myosotis."},
			{str = "This trinket originated in Antibirth. It was originally capped at 8 pickups instead of 4."},
		},
	},
	TRINKET_M = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Using an activated item rerolls it into a different active item."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "One-time use items: The item disappears before it can be rerolled."},
			{str = "Book of Virtues: If an activated item is combined with the Book, only the activated one is rerolled but the Book remains unchanged."},
			{str = "Breath of Life: Breath of Life is rerolled away once the charge is fully expended. After one second, Isaac starts getting hurt as if he was still using Breath of Life until he leaves the room."},
			{str = "Glowing Hour Glass: Glowing Hour Glass rewinds time before the reroll goes through, preventing it from happening."},
			{str = "Notched Axe: No effect. The item cannot be rerolled by either using it or depleting its charge."},
			{str = "Schoolbag: Only the primary item is rerolled."},
			{str = "Butter!: The active item is rerolled before being dropped by Butter!"},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Like Missing No., the name 'M is a reference to a glitch from the Generation I Pokmon games, often thought of as a counterpart to MissingNo. itself. This is also referenced by its appearance, as random jumbles of pixels is very similar to the corrupted sprites you would see from an sprite error in Generation I Pokmon games."},
			{str = "The description is a combination of Broken Remote and The D6's description, ''It's broken'' and ''Reroll your destiny''."},
		},
	},
	TRINKET_TEARDROP_CHARM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Luck-based tear effects have a higher chance of occurring."},
			{str = "- The bonus is equivalent to +3 Luck, but the other benefits of Luck do not apply."},
		},
	},
	TRINKET_APPLE_OF_SODOM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Picking up Red Hearts can convert them into 1-4 Blue Spiders and cause a fart similar to Butt Penny."},
			{str = "- Isaac can pick up hearts even while at full health. In this case, the effect always works."},
			{str = "- While injured, there is a moderate chance the heart will be picked up normally. If spiders are spawned, no health is gained."},
			{str = "- This allows ???, The Lost, Dark Judas, Tainted ???, Tainted Lost, Tainted Judas, and Tainted Soul to pick up red hearts."},
			{str = "- In co-op, this allows Keeper and Tainted Keeper to convert red hearts into blue spiders."},
			{str = "- Hearts in the Shop can be converted into spiders for free unless Isaac has 0 coins."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Converting hearts dropped by Options? will not cause the other consumable to disappear."},
			{str = "Has no effect on Rotten Hearts."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Apple of Sodom was a cut trinket from Antibirth. It was going to be an unlock for the unused character, Edith."},
			{str = "This trinket is likely a reference to the cryptid called the ''Vine of Sodom.'' A plant that produces ''Apples of Sodom'' which supposedly explode and are poisonous."},
		},
	},
	TRINKET_FORGOTTEN_LULLABY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Doubles the rate of fire of Isaac's familiars."},
			{str = "- Familiars with charged shots will have their charge time halved. The recovery time between shots is not affected."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket was originally called Song of the Siren in Antibirth."},
			{str = "Forgotten Lullaby, the trinket's new name in Repentance, is also the title of the secret room theme in Mudeth's original soundtrack for the Antibirth mod."},
		},
	},
	TRINKET_BETHS_FAITH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "At the beginning of each floor, spawn 4 wisps that orbit Isaac and fires tears alongside him. The wisps are the same as the wisps that spawn with the Book of Virtues while you do not have another active item combined with it."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Isaac already has any wisps before entering a new floor, the trinket will only add enough new wisps to bring the total wisps to 8."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box/Golden Trinket: Gives 8 wisps at once."},
		},
	},
	TRINKET_OLD_CAPACITOR = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Active items don't charge when clearing rooms. Adds a chance to spawn a Lil' Battery when clearing a room, independent of the room's original drop."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "4.5 Volt: Since the 4.5 volt also makes it so that active items don't charge when clearing rooms, the item can be picked up with no downside."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The pickup quote is a reference to an old joke game by LeatherIceCream, one of the Antibirth devs, where he makes a Don't Starve dating sim which he titled ''Voltage Voltage Starving''."},
		},
	},
	TRINKET_BRAIN_WORM = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Tears snap 90 degrees to target enemies that they may have missed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The effect will not work if Isaac is too close to enemies."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Brimstone / Montezuma's Revenge: Laser will snap regardless of length from enemy. Also works with Azazel's Brimstone."},
			{str = "Cricket's Body: Both the main and split tears are affected by Brain Worm."},
			{str = "Dr. Fetus: No effect on bombs."},
			{str = "Mom's Knife: No effect."},
			{str = "Revelation: Same as Brimstone."},
			{str = "Tech X: No effect."},
			{str = "Tractor Beam: No Effect."},
		},
	},
	TRINKET_PERFECTION = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives +10 luck."},
			{str = "This trinket is destroyed if Isaac takes non-self damage while holding it (i.e getting hit by Monsters, walking on Spikes, getting hit by an explosion, etc.)."},
			{str = "This trinket can only be obtained by clearing 3 consecutive floors without taking damage. It will drop upon the final boss room of said consecutive floors being cleared."},
			{str = "- In Greed Mode, it will drop after one of the boss waves of a floor."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Blood Donation Machines, Devil Beggars, Hell Games, Confessionals, Curse Rooms, Spiked Chests, Bad Trip pills, White Fire Places and the Mausoleum doors count as self-damage and won't destroy Perfection."},
			{str = "Every XL floor cleared counts as a single floor for the purposes of obtaining this trinket."},
			{str = "Perfection can intentionally be dropped if Isaac must take damage for whatever reason, allowing him to keep it."},
			{str = "Due to the unique method of obtaining it, Perfection can never naturally spawn as a Golden Trinket."},
			{str = "10 Luck is coincidentally the highest amount of luck Isaac can have that would help with room clear rewards paying out."},
			{str = "If Isaac holds two Perfections via Mom's purse, Belly Button or one/both of them are gulped, upon taking a hit only one of them will get destroyed."},
			{str = "If Isaac gulps Perfection as he's taking damage (e.g. because of Marbles), the trinket will stay in your inventory as a passive item until the next instance of taking damage."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Blanket/ Holy Card/ Holy Mantle/ Wooden Cross/ Dogma/ The Lost/ Soul of the Lost/White Fire Places: Losing the shield does not destroy Perfection."},
			{str = "Dull Razor: Does not destroy Perfection."},
			{str = "Sharp Plug: Does not destroy Perfection."},
			{str = "Rock Bottom: The +10 luck persists after switching trinkets as stats are locked to their highest values ever reached post-rock-bottom. However, after losing Perfection, additional luck-ups will have no effect until the luck bonus lost is made up. (The stats are still being calculated behind-the-scenes, but until it surpasses the run's post-rock-bottom luck record, the record will be used instead)."},
			{str = "The Lost / Tainted Lost: Since these characters are likely to die instantly after taking a single hit, Perfection is almost guaranteed to drop after the third floor boss room is cleared, and is also almost guaranteed to stay with them until their deaths."},
			{str = "- This means getting luck based items at any point of the run, especially ones with luck scaling that that tops out at about 10 luck, is a very good idea."},
			{str = "- The ability to Smelt/Gulp trinkets will also add to the synergy, as there are many luck based trinkets as well."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Smelter/ Marbles/Gulp!: The effect is still lost after taking damage."},
			{str = "Blood Oath: The forced damage will not destroy the trinket, or prevent its acquisition."},
		},
	},
	TRINKET_DEVILS_CROWN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Treasure Rooms are replaced with Red/Devil Treasure Rooms that contain items from the Devil Room pool that can be bought for the usual number of Heart containers normally requested (for the spawned item) in devil rooms."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The Devil Treasure Room generated will be the same as the treasure room that would have been generated, minus the item and room textures."},
			{str = "- If the Treasure Room would have been a double Treasure Room, the Devil Treasure Room will also have two items. Unlike the double Treasure Room, however, both items can be taken. This also goes for Treasure Rooms on the alternate path (the blind item will still be blind)."},
			{str = "This trinket also converts Treasure rooms on the current floor when picked up. However, this only affects Treasure Rooms that have not been visited yet."},
			{str = "- The same goes vice-versa: dropping the trinket will convert red treasure rooms to normal ones unless they've been visited."},
			{str = "Items bought in the Devil Treasure Rooms will affect Angel Room chance like a normal devil deal would."},
			{str = "Does not impact Treasure Rooms for the Ascent."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "More Options: All Devil Treasure rooms will have two items, both of which can be purchased."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Redemption: Devil Treasure Rooms will count as regular Devil Rooms for the purposes of this item."},
		},
	},
	TRINKET_CHARGED_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Picking up coins has a chance to add one charge to Isaac's currently held Active Item."},
			{str = "This chance is equal to 1/6th of the value of the coin, and cannot activate more than once per coin collected."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This item used to be called Electric Penny in Antibirth, and had a different sprite."},
		},
	},
	TRINKET_FRIENDSHIP_NECKLACE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Familiars that normally follow Isaac orbit around him instead."},
			{str = "- Familiars that do not normally block shots will not block shots when converted to orbitals."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Child Leash: Familiars will orbit much closer to Isaac."},
			{str = "King Baby: Familiars will orbit King Baby at all times instead of Isaac, even while not firing. King Baby itself is unaffected."},
		},
	},
	TRINKET_PANIC_BUTTON = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Activates Isaac's active item just before damage would be taken."},
			{str = "- This still uses the item's charges so if the item is not charged, the trinket will do nothing."},
			{str = "- Single-use items will be consumed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Because the automatic item activation takes place just before taking would-be damage, this trinket becomes very powerful when holding damage-negating active items."},
			{str = "Taking damage from explosions, spikes, the stomp attacks from Mom and Satan, brimstone lasers, self-damage items, Devil Beggars, Blood Donation Machines, and Confessionals will not trigger the effect."},
			{str = "Super-charged items will use both of their charges. This is NOT an effect by Mom's Box or the Golden Trinket variant."},
			{str = "Having Holy Mantle does not stop the trinket from activating, making you still take damage from items such as IV Bag, while bypassing Holy Mantle's shield."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Any self-damaging item: Self-damage occurs before enemy damage, making it almost impossible to lose Devil/Angel Room chances."},
			{str = "Blood Rights: Triggers the effect on hit, at the cost of every hit making Isaac take full-heart damage."},
			{str = "Butter Bean / Wait What?: Isaac farts before being hit, knocking back enemies and projectiles."},
			{str = "Dull Razor: Activates the item and gives Isaac invincibility frames before being hit, making it work similarly to Holy Mantle every two rooms. If combined with 9 Volt or AAA Battery, works every room, just like Holy Mantle."},
			{str = "How to Jump: Makes Isaac immune to contact and tear damage while moving."},
			{str = "IV Bag: Upon getting hit, activates the IV Bag instead, causing red heart damage and producing money."},
			{str = "Kamikaze!: Causes a highly-damaging explosion to occur every time Isaac is hit, similarly to Swallowed M80."},
			{str = "Razor Blade: Isaac gains a damage boost every time he's hit, acting similarly to Bloody Lust except only for the current room, at the cost of every hit making Isaac take full-heart damage."},
			{str = "Spin to Win: Has a chance to spawn additional wheels upon taking damage that last only for the current room. This is most likely a bug."},
			{str = "Stitches: Isaac will teleport before damage is taken, making him much harder to hit."},
			{str = "Telekinesis: Telekinesis will automatically block any incoming shots, but the push is not strong enough to prevent contact damage."},
			{str = "Yum Heart: Since the effect is trigger before taking damage, Panic Button will waste the charge with no benefit unless Isaac is already missing Red health."},
		},
	},
	TRINKET_BLUE_KEY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Entering a room that requires a key does not consume a key, but brings Isaac to a room resembling ???, which acts as an in-between of the two rooms."},
			{str = "- These rooms contain assortments of Blue Gapers, Blue Boils, and/or Blue Conjoined Fatties, and are always long rooms (either normal or slim). They have a chance to give a reward when cleared, like normal rooms."},
			{str = "- Once exited, the room cannot be re-entered."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Doors that were unlocked with the trinket have a blue hue emitting from them instead of the normal floor-dependent hue."},
			{str = "Rooms that require money to enter (Arcades or rooms affected by Pay to Play) can also be opened for free."},
			{str = "Doors that require two keys to open, such as Vaults or Dice rooms, will be opened instantly instead of waiting for the animation of both keys to be used."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Door Stop/ Mercurius: The blue room can be exited without fighting the enemies, and the door will remain unlocked."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket is most likely a reference to SCP-860, a fictional key that acts in a near-identical manner."},
		},
	},
	TRINKET_FLAT_FILE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Makes all spikes retract, preventing them from dealing damage."},
			{str = "- Also affects Spiked Rocks and Spiked/Mimic Chests."},
			{str = "The spikes around Curse Room doors are removed, allowing them to be entered and exited without taking damage."},
			{str = "Removes the spikes from Pokies, Slides, and Wall Huggers."},
			{str = "- Causes Spikeballs to despawn."},
			{str = "- Has no effect on Grudges and Ball and Chains."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Flat File is dropped in a room with spikes, the spikes will not extend again unless the room is left and re-entered."},
			{str = "Flat File also retracts the spikes in the center of Sacrifice Rooms, making them unusable while the trinket is held. The spikes grow back if Isaac leaves the room, drops the trinket, and re-enters it."},
			{str = "Also removes spikes from doors in the Cursed! challenge, making it much easier."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The Flat File assumably ''files'' down the spikes, like one would file the imperfections from their nails, or file metal bars to escape from jail."},
		},
	},
	TRINKET_TELESCOPE_LENS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases the chance for a Planetarium to spawn by 9%."},
			{str = "Keeps the chance at 15% chance for a Planetarium to spawn again after entering a first one."},
			{str = "Makes it possible for Planetariums to spawn in Chapter 4 and Corpse."},
		},
	},
	TRINKET_MOMS_LOCK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Has a chance to grant the effects of a random Mom item each room."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Increases the chance of the effect occuring."},
		},
	},
	TRINKET_DICE_BAG = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Has a 50% chance to give Isaac a Dice item as a consumable when he enters a new room. The Dice disappears when he leaves the room."},
			{str = "Also grants Isaac the ability to hold two consumable items for that room, so Isaac gets a Dice even if he is already holding a consumable item."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Isaac leaves and re-enters a room, Dice Bag will not try to give another Dice when Isaac re-enters it."},
			{str = "Any dice item can be given to Isaac, including ones that haven't been unlocked yet."},
			{str = "Dice Bag cannot give Spindown Dice, D Infinity, Cracked Dice, or Dice Shard."},
			{str = "Dropping the trinket on the ground does not remove the Dice consumable until Isaac leaves the room."},
		},
		{ -- Strategy
			{str = "Strategy", fsize = 2, clr = 3, halign = 0},
			{str = "Be cautious when carrying cards and/or pills, the dice item Dice Bag gives will always be first in the consumable cue if it gives one, one unaware press of the ''use consumable'' button can lead to Isaac's build getting fully rerolled."},
			{str = "Useful Cards, pills, and runes/soul stones can be held until Dice Bag gives a D1 to gain an extra copy of them."},
			{str = "Cards/items that can spawn items when used (such as XX - Judgement or Mystery Gift) can be held until Dice Bag gives an (Eternal) D6 to reroll the items they give before the dice goes away."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Increases the chance to get a dice to a 100%."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Starter Deck/ Little Baggy/ Polydactyly: The two cards/pills and die can be held at the same time, and can be cycled through as normal with the swap key."},
			{str = "Glowing Hour Glass: The dice given in the room Isaac used Glowing Hour Glass in will be granted in the next new room Isaac visits, regardless of what it is."},
			{str = "Jacob and Esau: Regardless of who is holding the dice, it is activated by pressing the consumable key. If Esau is holding an active item, it will be used at the same time as the dice. Possibly unintended behavior."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket resembles a bag from Crown Royal, a popular brand of liquor that comes in a purple sack. In real life, these bags are commonly used as dice bags due to their availability."},
		},
	},
	TRINKET_HOLY_CROWN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Causes a Treasure Room and Shop to generate in Cathedral."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The spawned Treasure Room will have one of 23 unique layouts for this floor."},
			{str = "This trinket has a demonic counterpart in Wicked Crown."},
		},
	},
	TRINKET_MOTHERS_KISS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants one heart container while held."},
			{str = "- This heart container will be filled the first time the trinket is picked up."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If the only heart container Isaac has is the one given by Mother's Kiss, the trinket will be destroyed if the heart container is traded/consumed."},
			{str = "If Isaac drops the trinket while he only has one heart container for health, he will have no health and die instantly."},
			{str = "The extra red heart container can exceed the maximum coin heart container limit of Keeper and Tainted Keeper, allowing them to obtain more than 3 or 2 coin hearts respectively."},
			{str = "The heart added is a soul heart for ??? and Tainted Bethany, and a black heart for Dark Judas and Tainted Judas."},
			{str = "The Forgotten will always get a Bone Heart regardless of whether the body or the soul picked up the trinket."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Guppy's Paw: The trinket will be consumed but you get to keep the Soul Hearts from the conversion."},
			{str = "Converter: Can practically turn the red heart container into 3 red heart containers."},
			{str = "Mom's Box: Adds 2 heart containers."},
		},
	},
	TRINKET_TORN_CARD = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Every 15 shots, Isaac will shoot an Ipecac + My Reflection tear with a very high range value."},
			{str = "The tear will follow Isaac until it hits an enemy or lands, where it will explode."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is a torn Suicide King card, and its tear effect is the same combination used in the Suicide King challenge (which unlocks the card)."},
			{str = "- The Suicide King challenge uses Lazarus as the starting character, which is also referenced by it being one of the Tainted Lazarus unlocks."},
		},
	},
	TRINKET_TORN_POCKET = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon taking damage, Isaac drops 2 random pickups from his inventory (except hearts)."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "All pickups are subjected to standard pickup spawn rules, which means that trinket's effect won't necessarily result in a net neutral."},
			{str = "- Coins dropped have a chance of being replaced with Lucky Pennies, Nickels, Sticky Nickels or Dimes."},
			{str = "- Bombs dropped have a chance of being replaced with double Bombs, Golden Bombs, Troll Bombs or Megatroll Bombs."},
			{str = "-- When a dropped bomb is replaced with a Troll/Megatroll bomb, the game will still subtract that bomb from the total bomb count."},
			{str = "-- Giga bombs, however, can't be dropped if Isaac has them, but their count will still be retained unless the bomb count reaches 0."},
			{str = "- Keys dropped have a chance of being replaced with Golden Keys or Charged Keys."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Humbling Bundle: All dropped pickups have a chance to be replaced with doubled variants."},
			{str = "Bogo Bombs: Dropped bombs will be their doubled variants."},
			{str = "Penny trinkets: Dropped coins can be used by penny trinkets as normal."},
			{str = "D20/ GB Bug: Dropped pickups can be rerolled into other pickups."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Tainted ???: The character won't drop his poops after taking damage."},
		},
	},
	TRINKET_GILDED_KEY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Replaces all new chests with Golden Chests."},
			{str = "Modifies the contents of Golden Chests to be able to give cards and trinkets from a single chest alongside normal pickups."},
			{str = "- Also enables Golden Chests to drop pills."},
			{str = "Gives Isaac one key when picked up for the first time."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Old Chests and Mega Chests cannot be overwritten by Golden Chests."},
			{str = "The contents of Golden Chests can be changed retroactively by picking up/dropping Gilded Key. This can even be observed if Isaac has Guppy's Eye, allowing him to see what the chest would have if he did(n't) have the key and choose which payout he wants."},
			{str = "- This also applies to chests that use Golden Chests' loot systems, such as Stone Chests and Eternal Chests."},
			{str = "- Mega Chests will also have their contents modified if they didn't contain items."},
			{str = "- The difference in contents between the two chest payouts are independent from each other, regardless of if the original chest had an item, card, trinket, or pickups, the modified chest can have any combination of them."},
			{str = "-- Modified chests are still unable to spawn pickups alongside items."},
			{str = "Gilded Key does not retroactively change chests that have already spawned into Golden Chests, or change their contents."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "The Left Hand: Overrides Gilded Key."},
			{str = "Paper Clip: If you have Mom's Purse, you can take Gilded Key and Paper Clip at the same time and get a strong synergy, since Paper Clip allows to open Golden Chests without a key, this synergy becomes even stronger with Mom's Key, since this item gives better chest loot."},
		},
	},
	TRINKET_LUCKY_SACK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a Grab Bag upon traveling to the next floor."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Found Soul: An extra sack will spawn next to Found Soul."},
			{str = "Mom's Box: Spawns 2 sacks instead of one."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Strawman: Does not spawn an extra sack for the Keeper."},
		},
	},
	TRINKET_WICKED_CROWN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Causes a Treasure Room and Shop to generate in Sheol."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The spawned Treasure Room will have one of 22 unique layouts for this floor."},
			{str = "This trinket has an angelic counterpart in Holy Crown."},
		},
	},
	TRINKET_AZAZELS_STUMP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Gives Isaac a chance to turn into Azazel when clearing a room. Isaac keeps his hearts, items and stats, but changes his tears to be a short Brimstone and gaining the ability to fly."},
			{str = "The effect lasts until clearing and leaving another room."},
			{str = "The effect can be taken to the next floor."},
			{str = "Dropping the trinket after this transformation will not reverse it, until a new room is cleared"},
			{str = "The effect won't change the player's post-it into Azazel's post-it."},
		},
	},
	TRINKET_DINGLE_BERRY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a random friendly Dip upon clearing a room."},
			{str = "Turns enemy Dips into friendly Dips, similar to Dirty Mind."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Spawns 2 dips instead of 1."},
		},
	},
	TRINKET_RING_CAP = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Places one extra Bomb for each bomb placed."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The extra bomb comes for free, meaning Isaac will only spend one bomb per use."},
			{str = "The extra bomb is spawned in a random location next to the original bomb placed."},
			{str = "The additional bomb is affected by any items that affect bombs."},
			{str = "Ring Cap will also work with Giga Bombs collected via Safety Scissors."},
			{str = "Can be used to open the door to the Mines with only one bomb."},
			{str = "Tainted ??? is not affected by this trinket."},
			{str = "This trinket has no effect on the Explosive Diarrhea pill."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Adds a third bomb."},
			{str = "Nancy Bombs: Grants a random bomb effect to each bomb separately."},
			{str = "Rocket in a Jar: Fires two rockets simultaneously."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Best Friend: No effect."},
			{str = "Bob's Rotten Head: No effect."},
			{str = "Dr. Fetus/ Epic Fetus: No effect."},
			{str = "Mr. Boom: No effect."},
			{str = "No. 2: No effect."},
		},
	},
	TRINKET_NUH_UH = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "On Chapter 4 and beyond, replaces all coin and key spawns with a random pickup:"},
			{str = "- Bomb: 26.6%"},
			{str = "- Heart: 26.6%"},
			{str = "- Pill: 16.6%"},
			{str = "- Card: 16.6%"},
			{str = "- Trinket: 6.6%"},
			{str = "- Battery: 6.6%"},
			{str = "If the pickup is a coin, it also has a 20% chance to be replaced with a fly."},
			{str = "This trinket has no effect in Greed Mode."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "All items, trinkets or machines that spawn coins and keys synergize with this trinket and will cause a random pickup to be spawned instead."},
			{str = "The floors during the Ascent are not affected."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: No effect."},
		},
	},
	TRINKET_MODELING_CLAY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Has a 50% chance to mimic the effect of a random passive item from the current rooms item pool when entering a room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Modeling Clay can only mimic items that have the summonable item tag, the same way Lemegeton selects the wisps it summons."},
			{str = "Item effects are seeded per room, meaning you'll get the same random effect when you enter the same room."},
			{str = "Items that would give or remove consumables or heart containers will not do so if Modeling Clay mimics them."},
			{str = "If Isaac holds multiple copies of Modeling Clay, they will all mimic the same item."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "TMTRAINER: Modeling Clay cannot copy glitched items, it will do nothing and not have a sprite if it tries to."},
			{str = "Sacrificial Altar: If Modeling Clay is currently mimicking a viable familiar, it will also be sacrificed and spawn an additional Devil Room item, but Modeling Clay will not be destroyed."},
			{str = "If Modeling Clay mimics the following items, they will behave in the following ways:"},
			{str = "- Starter Deck/Little Baggy/Polydactyly: Allows Isaac to hold two cards and/or pills if he picks them up while Modeling Clay mimics the item, and he will continue to hold both even after Modeling Clay changes. If Isaac tries to pick up another card/pill after Modeling Clay changes, the inactive card/pill he was holding will be destroyed and he will only be able to hold one card/pill again."},
			{str = "- Mom's Purse/Belly Button: Allows Isaac to hold another trinket alongside Modeling Clay if he picks it up while Modeling Clay mimics the item, and he will continue to hold both even after Modeling Clay changes. If Isaac tries to pick up another trinket while he already has two, it will instead be destroyed and he will only be able to hold one trinket again."},
			{str = "-- Isaac can safely exchange cards/pills/trinkets by manually dropping them, at the cost of being unable to hold two."},
			{str = "- Bloody Lust/Bloody Gust/Keeper's Sack/Rock Bottom/False PHD: Stat boosts will not persist after Modeling Clay changes. If it mimics that item again, it will remember the stat boosts they gave."},
			{str = "- Schoolbag: If Isaac picks up another active item while Modeling Clay mimics the item, the other active item is dropped after Modeling Clay changes."},
			{str = "- Member Card: The special trapdoor will not spawn."},
			{str = "- 9 Volt: 1-room charge active items will not recharge over time. Other active items are affected normally."},
			{str = "- Delirious/Mr. ME!: Has no effect, even if Isaac uses his active item."},
			{str = "- Black Candle: Does not remove the current floor’s curse."},
			{str = "- The Compass/Treasure Map/Blue Map/The Mind: Does not reveal rooms on the map."},
			{str = "- PHD/False PHD: Pills’ effects will be changed, but they will not be identified."},
			{str = "- Isaac's Tomb/Voodoo Head: If Isaac can enter a trapdoor to the next floor while it’s being mimicked, they will have their normal effects."},
			{str = "-- Other items that activate on floor transition (such as Dream Catcher, Empty Heart, or Lost Soul) will not work."},
			{str = "- Glitched Crown: Items that are spawned while it’s being mimicked will cycle between 5 different items, even after Modeling Clay changes."},
			{str = "- Candy Heart/Soul Locket: Stats granted from picking up hearts will persist after Modeling Clay changes."},
			{str = "- Sanguine Bond: The unique spikes will not spawn."},
			{str = "- Cambion Conception/Immaculate Conception: If Isaac can somehow meet the requirements for these items to spawn familiars, the familiars will persist for only one room after Modeling Clay changes."},
			{str = "- GB Bug: The familiar respawns every time the trinket mimics the item."},
			{str = "- Spelunker Hat/X-Ray Vision/YO LISTEN!/Dog Tooth: Will reveal/open neighboring Secret Rooms."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box/Golden Trinket: Increases the chance of triggering to 100%."},
			{str = "Rock Bottom: Permanently increases Isaac's stats when it transforms into an item that surpasses Isaac's current stats."},
			{str = "- This allows the player to enter and exit a room any number of times in order to get as many stats as possible."},
		},
		{ -- Strategy
			{str = "Strategy", fsize = 2, clr = 3, halign = 0},
			{str = "Because Modeling Clay can constantly cycle between items by constantly exiting and re-entering rooms, many items’ effects can be forced with enough patience to get maximum value out of the trinket. Aside from the interactions listed above, the following items/effects can be forced in order to gain value from the trinket in the following ways:"},
			{str = "- Explosive shots: Allows Isaac to destroy rocks and open Secret Room doors without spending resources."},
			{str = "- Damage-triggered items: To gain extra value from using Machines/Beggars that take health or Sacrifice Rooms. Note that stats given by on-hit items will not persist outside the rooms they were used in."},
			{str = "-- Hallowed Ground can be used first to spawn a white poop next to a Machine/Beggar that takes health to give Isaac the chance to resist damage taken from using them."},
			{str = "- The Wafer/Cancer: Reduces the damage taken from Sacrifice Rooms to half a heart, allowing Isaac to use them more."},
			{str = "- Scapular: To gain a soul heart if at low health. This will not work if the damage source is considered ''health donation'', such as from a Blood Donation Machine, Demon Beggar, Confessional, or Hell Game, but it will work for Curse Rooms and Sacrifice Rooms."},
			{str = "- PHD/Lucky Foot/Virgo: Bad pills will be their good counterparts while mimicking these items, making them much safer to use."},
			{str = "- Tarot Cloth: Cards will have their effects boosted."},
			{str = "- Steam Sale: Shop items and pickups will be discounted."},
			{str = "- Sharp Plug: Isaac’s active item can be instantly recharged at the cost of red hearts."},
			{str = "- The Battery: Allows Isaac's active item to be overcharged, overcharging will persist after Modeling Clay changes."},
			{str = "- Starter Deck/Little Baggy: Aside from holding multiple cards/pills, also allows cards/pills to be transformed if he can drop cards/pills without dropping Modeling Clay in the process. This requires having at least 2 other cards/pills in the room to cycle through."},
			{str = "- Restock: Items will restock themselves as normal."},
			{str = "- Marbles: Allows trinkets to be smelted if Isaac can deal damage to himself while it mimics the item."},
			{str = "- Echo Chamber: Cards and runes’ effects will be stacked if used while it mimics the item. The game will remember what cards and runes were used if it turns into Echo Chamber in the future, even on later floors."},
			{str = "- Pyromaniac: Lets Isaac heal off explosions in the current room. Can be used to greater effect in rooms with TNT or Bomb Bums."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The trinket's icon appears to be a clay model of Meat Boy from Super Meat Boy, another game by Edmund McMillen."},
			{str = "The item is a direct reference to the item of the same name in The Binding of Isaac: Four Souls, which turns into another available item when used."},
		},
	},
	TRINKET_POLISHED_BONE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "After clearing a room, adds a chance to spawn a friendly Bony."},
		},
	},
	TRINKET_HOLLOW_HEART = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants one bone heart container upon entering a new floor."},
			{str = "If the player has 12 heart containers, the added bone heart will replace the rightmost soul heart (if any)."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Adds 2 bone heart containers instead of one."},
		},
	},
	TRINKET_KIDS_DRAWING = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "While held, acts as one of the three items needed for the Guppy transformation."},
			{str = "- The effect is lost if Kid's Drawing is dropped."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If multiple copies of the trinket are obtained, they will each count as a Guppy item."},
			{str = "The Golden Trinket will count as holding 2 Guppy items."},
		},
	},
	TRINKET_CRYSTAL_KEY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Whenever Isaac clears a room, has a chance to unlock a neighboring Red Room."},
			{str = "- This effect can occur while Isaac is inside a Red Room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "It's possible to increase the chance of a Red Room unlocking if multiple Crystal Keys are held, i.e. from absorbing using Smelter, holding Mom's Box, having a golden Crystal Key or other methods. The chance for a Red Room door to unlock depends on the number of Crystal Keys held and if the room cleared is normal or Red."},
			{str = "Each cleared wave of a Challenge Room has a chance to generate an adjacent Red Room."},
			{str = "Rooms that are re-fought using the D7 have a chance to generate additional Red Rooms."},
			{str = "Doors do not have to open to an actual valid Red Room, and can lead outside the map and into I AM ERROR."},
		},
	},
	TRINKET_KEEPERS_BARGAIN = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a chance for Devil Deals, Black Market and A Pound of Flesh shop items cost money instead of health."},
			{str = "- Items that would normally cost 1 heart container will cost 15 coins instead. For items that would normally cost 2 heart containers, it will take 30 coins."},
			{str = "- Items affected by Keeper's Bargain may occasionally be discounted (e.g. a 30 coins item down to 15 coins), just like items are in a regular shop."},
			{str = "- No effect on The Lost or Tainted Lost."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The effects of this trinket are inherent to Keeper and Tainted Keeper, rendering the trinket useless for them."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Judas' Tongue: Does not provide any discount."},
		},
	},
	TRINKET_CURSED_PENNY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Teleports Isaac to a random room upon picking up a coin. Can teleport to the secret rooms."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Tainted Keeper: Allows him to farm infinite money. Killing an enemy drops a coin, which will teleport him to a different room and reset the current one."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Black Candle: Teleport effect is removed."},
			{str = "Mom's Box: No effect."},
		},
	},
	TRINKET_YOUR_SOUL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Grants 1 free Devil Deal item in exchange for the trinket."},
			{str = "- After a Devil Deal has been taken, the trinket is removed from the player's inventory."},
			{str = "- Also works in Black Markets and all other places where the player can exchange heart containers for an item."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "When playing as the Keeper or if the Devil Deal costs money because of items such as A Pound of Flesh, Your Soul has no effect."},
			{str = "Using Your Soul as The Lost/ Tainted Lost is redundant as it will consume the trinket with no actual benefit."},
			{str = "Using Your Soul still counts as taking a Devil Deal."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: No effect."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "The name and mechanics of the trinket refer to ''selling your soul'', which is when a person makes a deal between themselves and Satan (or a lesser demon)."},
		},
	},
	TRINKET_NUMBER_MAGNET = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Increases Devil Room chance by 10%."},
			{str = "Prevents Krampus from appearing in the Devil Deal."},
			{str = "If held before visiting the Devil Deal, the room's layout will be replaced with one of 11 special Devil Room layouts (6 in Greed Mode). These rooms contain 0-3 Devil Room items (most commonly 2-3), 1-3 Black Hearts, and several enemies from Sheol."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket appears to be one of the three sixes from The Mark."},
		},
	},
	TRINKET_STRANGE_KEY = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Unlocks the passageway to ???, regardless of run duration."},
			{str = "Unlocks Pandora's Box, giving it a new effect."},
			{str = "- The normal effect of Pandora's Box for that level is ignored, and instead 6 items from random pools are spawned."},
			{str = "- Both the box and the trinket are consumed."},
			{str = "Both effects work even if the key has been turned into a passive item from items such as Smelter or Marbles."},
			{str = "- Using Pandora's Box with the key in this state will remove the key from Isaac's Inventory, removing the ability to open ???."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "Strange Key is likely a reference to the film by David Lynch, Mulholland Drive (2001), where a small blue box being unlocked by a strange key is a major plot point."},
		},
	},
	TRINKET_LIL_CLOT = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a blood clot familiar on the ground next to Isaac. This familiar moves and shoots through the same inputs as Isaac, effectively acting as a second source of tears."},
			{str = "The familiar uses Isaac's tears, range, shot speed, and tear effects, and deals 0.35x Isaac's damage."},
			{str = "Lil Clot will die after it gets hit 3 times, and will respawn when entering a new room."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Holding the ''drop'' button lets Isaac move independently from Lil Clot."},
			{str = "Lil Clot will persist after the trinket is dropped, but it will not respawn unless the trinket is picked up again."},
			{str = "Lil Clot spawns in a random location near Isaac when the trinket is first picked up."},
			{str = "Duplicating Lil Clot trinkets will cause them to stack, allowing Isaac to control multiple Lil Clots at once."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "BFFS!: Increases the damage to 70% of Isaac's."},
			{str = "Found Soul: The ghost familiar receives its own floating Lil Clot that acts the same way as Isaacs's, vanishing when the ghost dies."},
			{str = "Mom's Box: Summons an additional Lil Clot."},
			{str = "Tractor Beam: Lil Clot's tears travel along the beam regardless of the location of Lil Clot."},
		},
	},
	TRINKET_TEMPORARY_TATTOO = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns a chest after completing a normal Challenge Room."},
			{str = "Spawns an additional item from the Boss Item Pool after completing a boss Challenge Room."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: No effect."},
		},
	},
	TRINKET_SWALLOWED_M80 = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Isaac has a 50% chance to explode when he takes damage, similarly to if he used Kamikaze!."},
			{str = "- The explosion deals 185 damage and uses most of Isaac's bomb synergies."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Causes Blood Donation Machines to act as if they were destroyed from the explosion despite surviving and functioning normally, resulting in extra coin and heart drops."},
			{str = "The explosion cannot be triggered by Curse Room or Sacrifice Room spikes."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Golden version / Mom's Box: raises the chance to trigger an explosion to 100%."},
			{str = "Pyromaniac: Heals on triggered explosion, rendering Isaac invincible if combined with its Golden version / Mom's Box and The Wafer."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Mr. Mega: Damage is not increased further."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This item is a reference to M-80 firecrackers[1]."},
		},
	},
	TRINKET_RC_REMOTE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Instead of trailing behind Isaac, familiars are directly controlled by the same inputs as Isaac's."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Lilith/Incubus: Incubus can be positioned on top of Lilith, effectively removing Lilith's downside."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Does not affect familiars that follow Isaac's movement on a delay."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "Holding the drop button will allow Isaac to move without moving the familiars, identical to Jacob and Esau."},
		},
	},
	TRINKET_FOUND_SOUL = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Summons a familiar that follows Isaac's exact movements, akin to Jacob and Esau, and shoots spectral tears."},
			{str = "- The ghost uses Isaac's tears, range, shot speed, and tear effects, and deals 0.5x Isaac's damage."},
			{str = "- The familiar has collision and can be separated. Holding down the ''drop'' button keeps the familiar in place."},
			{str = "- When Isaac places a bomb, the familiar will drop one as well."},
			{str = "- The familiar can press Buttons."},
			{str = "The familiar dies after taking any damage, and will respawn on the next floor."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Found Soul is dropped before its death animation finishes, it will still be available for the current floor if picked up again."},
			{str = "First time pickup of this trinket may result in it not scaling with your speed properly. Regardless of your current speed, Found Soul's speed will be at 1, which may lead to detrimental desyncs. This issue is usually fixed by traveling to the next floor."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Circle of Protection: The ghost familiar receives its own circle, which is the same size as the one given to Isaac."},
			{str = "Dark Arts: The ghost familiar will use Dark Arts along with Isaac. Enemies can be marked by both Isaac and the ghost familiar. The ghost familiar will gain its own damage boost."},
			{str = "Holy Mantle: The ghost familiar receives its own shield, working separate from Isaac's."},
			{str = "Lil Clot: The ghost familiar receives its own floating Lil Clot that acts the same way as Isaac's, vanishing when the ghost dies."},
			{str = "Locust of Conquest/Locust of Death/Locust of Famine/Locust of Pestilence/Locust of War: Will spawn an additional locust for Found Soul."},
			{str = "Lucky Sack: An extra sack will spawn next to Found Soul."},
			{str = "Purgatory: Spawns an additional red crack while the ghost familiar is alive."},
			{str = "Saturnus: The ghost familiar will spawn a ring of tears each room."},
		},
		{ -- Trivia
			{str = "Trivia", fsize = 2, clr = 3, halign = 0},
			{str = "This trinket is a reference to Lost Soul."},
		},
	},
	TRINKET_EXPANSION_PACK = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon use of an active item, activates the effect of another random active item."},
			{str = "- Timed recharges items or items without charge bar will trigger the effect once per room. Reentering the room allow to trigger the effect again."},
			{str = "- Cannot trigger Metronome or D Infinity"},
		},
	},
	TRINKET_BETHS_ESSENCE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering an Angel Room, spawns 5 wisp orbitals."},
			{str = "Donating to beggars has a chance to create a random wisp orbital."},
		},
	},
	TRINKET_THE_TWINS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Adds a 50% chance to duplicate one of Isaac's familiars when entering a new room."},
			{str = "If Isaac has no familiars, spawns Brother Bobby or Sister Maggy instead."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "The following items/trinkets cannot be duplicated:"},
			{str = "- Halo of Flies/Pretty Flies"},
			{str = "- Wisps given by Book of Virtues, Jar of Wisps, Beth's Essence, Beth's Faith, Lemegeton, or Vengeful Spirit"},
			{str = "- Abyss/ Apollyon's Best Friends unique attack locusts"},
			{str = "- Temporary familiars generated by Monster Manual"},
			{str = "- Dead Bird"},
			{str = "- Dead Cat"},
			{str = "- Isaac's Heart"},
			{str = "- Milk!"},
			{str = "- Giant Cell"},
			{str = "- Spin to Win"},
			{str = "- Stitches"},
			{str = "- Quints"},
			{str = "- The Swarm"},
			{str = "- The Intruder"},
			{str = "- Vanishing Twin"},
			{str = "- ???'s Soul"},
			{str = "- Isaac's Head"},
			{str = "- Umbilical Cord"},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Has an additional 50% chance to proc the effect again, resulting in another familiar doubling. This can be the same familiar that was doubled originally."},
		},
		{ -- Interactions
			{str = "Interactions", fsize = 2, clr = 3, halign = 0},
			{str = "Movement-mimicking familiars: Duplicated familiars will follow the original on the same delay."},
			{str = "- In the case of Shade, a copy of the familiar will spawn even if Shade was already absorbed by Isaac."},
			{str = "Pickup-generating familiars: If they remain duplicated for enough consecutive rooms, they will give a pickup as normal. The following familiars have to be duplicated for the following number of consecutive room clears to give pickups:"},
			{str = "- Sack of Pennies: 1 room (will give a coin every time its duplicated)"},
			{str = "- Bomb Bag/ Little C.H.A.D.: 2 rooms"},
			{str = "- Mystery Sack/ Rune Bag/ Sack of Sacks: 5 rooms"},
			{str = "- Acid Baby: 6 rooms"},
			{str = "- The Relic: 7 rooms"},
			{str = "-- Because Lil Chest and Charged Babys chances to spawn pickups are independent from the total number of rooms cleared with them, they have an extra chance to pay out in any room theyre duplicated in."},
			{str = "Cambion Conception/ Immaculate Conception: Familiars given by these items can be duplicated as normal."},
			{str = "Bum Friend/ Dark Bum/ Key Bum/ Bumbo/ Lil Portal: The pickup count of duplicated familiars is not remembered when they go away."},
			{str = "Ball of Bandages/ Cube of Meat: Upgrades the familiar instead of duplicating it."},
			{str = "Best Bud/ Bird Cage: If the trinket tries to duplicate it, it will instead immediately activate. Taking damage does not spawn another familiar."},
			{str = "- If this happens to Bird Cage as Isaac walks into a room with enemies, Bird Cage will also leap onto the enemies."},
			{str = "Blood Puppy/ Bumbo/ Guppy's Hair Ball: The duplicate will always be at its first stage."},
			{str = "1up!: Despite the fact that it can be duplicated, Isaac will not keep the 1up if he dies while it's duplicated."},
			{str = "Guillotine: The head can be duplicated, and will give an extra damage and tears up if it is. The extra head will be unable to fire tears."},
			{str = "GB Bug: The familiar can be duplicated even if it hasnt respawned, by constantly moving to new rooms, GB Bug can constantly respawn, allowing pickups to be rerolled indefinitely."},
			{str = "Succubus: The damage multiplier from standing in the aura of multiple Succubi multiplicatively stacks."},
			{str = "Buddy in a Box: The duplicated familiar will be randomly generated instead of copying the original."},
			{str = "Hallowed Ground: Duplicate poops will also drop when Isaac takes damage. The bonuses from standing in the aura of multiple white poops does not stack."},
			{str = "YO LISTEN!: An extra +1 luck will be given for every duplicate."},
			{str = "Mystery Egg: The friendly the duplicated egg spawns depends on how many consecutive rooms cleared the duplicate persisted for."},
			{str = "Lost Soul/ Star of Bethlehem: The trinket can try to make a duplicate of the familiar, but it will always fail. This reduces the odds of the trinket giving copies of other familiars or prevents it from giving Brother Bobby or Sister Maggy if he has no other familiars."},
			{str = "Soul of Lilith: Familiars given can be duplicated as normal."},
			{str = "Strawman: Keeper cannot be duplicated, as he is considered a character, not a familiar."},
		},
	},
	TRINKET_ADOPTION_PAPERS = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Shops now sell familiars instead of the usual item selection."},
			{str = "- The familiars are chosen from the Baby Shop item pool."},
			{str = "- While holding the Adoption Papers, the familiars are discounted to 10 coins. If the papers are dropped, they cost 15 coins."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "A shop that has already been entered will not have its item selection changed, nor will dropping the papers revert them to normal."},
		},
	},
	TRINKET_CRICKET_LEG = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Has a chance to spawn a random locust when killing an enemy."},
		},
	},
	TRINKET_APOLLYONS_BEST_FRIEND = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Spawns an attack locust familiar identical to the default ones spawned from Abyss which will fly forward while Isaac is shooting and deal contact damage equal to Isaac's damage stat."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Mom's Box: Spawns 2 locusts instead of one."},
		},
	},
	TRINKET_BROKEN_GLASSES = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "33% chance of adding an extra blind item in item rooms."},
			{str = "- The extra items on the alt path are revealed instead."},
			{str = "- This includes instances where pedestal items would be replaced with something else, such as the Pica Run challenge. If there is not a blind equivalent, both items will simply be shown."},
		},
	},
	TRINKET_ICE_CUBE = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Upon entering a room, there is a chance that enemies will be petrified. Enemies killed that have been petrified by Ice Cube are also frozen, including those killed after the petrification's effect ends."},
			{str = "- Petrification lasts for 4 seconds after entering the room."},
			{str = "- The chance for enemies to be petrified increases with luck."},
			{str = "- Enemies spawned during Greed Mode will not be petrified."},
		},
		{ -- Trivia
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "The reason Ice Cube is unlocked by completing Pica Run is probably due to pagophagia (the desire to eat ice), a common example of pica."},
		},
	},
	TRINKET_SIGIL_OF_BAPHOMET = {
		{ -- Effects
			{str = "Effects", fsize = 2, clr = 3, halign = 0},
			{str = "Shields Isaac for 1 second after killing an enemy."},
			{str = "- If another enemy is killed while Isaac is shielded, another second is added to the shield's duration."},
		},
		{ -- Notes
			{str = "Notes", fsize = 2, clr = 3, halign = 0},
			{str = "If Isaac is in range of an enemy that explodes on death, the shield will not come up before Isaac takes damage from the explosion. Other on death effects, like firing shots, are still guarded against."},
			{str = "In Arcades, Blood Donation Machines or Demon beggars can be used without losing health by killing the red flies from Shell Games and gaining a shield."},
			{str = "Stalagmites destroyed in the Beast fight do not trigger the shield."},
		},
		{ -- Synergies
			{str = "Synergies", fsize = 2, clr = 3, halign = 0},
			{str = "Kamikaze!: Isaac can chain-kill enemies without taking explosion damage while the shield is active."},
		},
	},
}
