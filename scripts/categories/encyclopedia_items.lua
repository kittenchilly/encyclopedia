Encyclopedia.AddItem({ -- 1.COLLECTIBLE_SAD_ONION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SAD_ONION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SAD_ONION,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 2.COLLECTIBLE_INNER_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INNER_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INNER_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 3.COLLECTIBLE_SPOON_BENDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPOON_BENDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPOON_BENDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 4.COLLECTIBLE_CRICKETS_HEAD
    Class = "vanilla",
    ID = CollectibleType[REPENTANCE and "COLLECTIBLE_CRICKETS_HEAD" or "COLLECTIBLE_MAXS_HEAD"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CRICKETS_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 5.COLLECTIBLE_MY_REFLECTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MY_REFLECTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MY_REFLECTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 6.COLLECTIBLE_NUMBER_ONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NUMBER_ONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NUMBER_ONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 7.COLLECTIBLE_BLOOD_OF_THE_MARTYR
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_BLOOD_OF_THE_MARTYR" or "COLLECTIBLE_BLOOD_MARTYR"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_OF_THE_MARTYR,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 8.COLLECTIBLE_BROTHER_BOBBY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BROTHER_BOBBY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BROTHER_BOBBY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 9.COLLECTIBLE_SKATOLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SKATOLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SKATOLE,
})
Encyclopedia.AddItem({ -- 10.COLLECTIBLE_HALO_OF_FLIES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HALO_OF_FLIES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HALO_OF_FLIES,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 12.COLLECTIBLE_1UP
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_1UP" or "COLLECTIBLE_ONE_UP"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_1UP,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 11.COLLECTIBLE_MAGIC_MUSHROOM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGIC_MUSHROOM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGIC_MUSHROOM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 13.COLLECTIBLE_VIRUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VIRUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VIRUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 14.COLLECTIBLE_ROID_RAGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROID_RAGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROID_RAGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 15.COLLECTIBLE_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
	Name = "c3",
})
Encyclopedia.AddItem({ -- 16.COLLECTIBLE_RAW_LIVER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RAW_LIVER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RAW_LIVER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 17.COLLECTIBLE_SKELETON_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SKELETON_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SKELETON_KEY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 18.COLLECTIBLE_DOLLAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DOLLAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DOLLAR,
})
Encyclopedia.AddItem({ -- 19.COLLECTIBLE_BOOM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 20.COLLECTIBLE_TRANSCENDENCE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TRANSCENDENCE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TRANSCENDENCE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 21.COLLECTIBLE_COMPASS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_COMPASS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_COMPASS,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 22.COLLECTIBLE_LUNCH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LUNCH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LUNCH,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 23.COLLECTIBLE_DINNER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DINNER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DINNER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 24.COLLECTIBLE_DESSERT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DESSERT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DESSERT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 25.COLLECTIBLE_BREAKFAST
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BREAKFAST,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BREAKFAST,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 26.COLLECTIBLE_ROTTEN_MEAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROTTEN_MEAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROTTEN_MEAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 27.COLLECTIBLE_WOODEN_SPOON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WOODEN_SPOON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WOODEN_SPOON,
    Pools = {
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 28.COLLECTIBLE_BELT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BELT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BELT,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 29.COLLECTIBLE_MOMS_UNDERWEAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_UNDERWEAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_UNDERWEAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 30.COLLECTIBLE_MOMS_HEELS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_HEELS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_HEELS,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 31.COLLECTIBLE_MOMS_LIPSTICK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_LIPSTICK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_LIPSTICK,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 32.COLLECTIBLE_WIRE_COAT_HANGER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WIRE_COAT_HANGER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WIRE_COAT_HANGER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 33.COLLECTIBLE_BIBLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BIBLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BIBLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 34.COLLECTIBLE_BOOK_OF_BELIAL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOK_OF_BELIAL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_BELIAL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 35.COLLECTIBLE_NECRONOMICON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NECRONOMICON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NECRONOMICON,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 36.COLLECTIBLE_POOP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POOP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POOP,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 37.COLLECTIBLE_MR_BOOM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MR_BOOM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MR_BOOM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 38.COLLECTIBLE_TAMMYS_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TAMMYS_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TAMMYS_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 39.COLLECTIBLE_MOMS_BRA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_BRA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_BRA,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 40.COLLECTIBLE_KAMIKAZE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KAMIKAZE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KAMIKAZE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 41.COLLECTIBLE_MOMS_PAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_PAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_PAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 42.COLLECTIBLE_BOBS_ROTTEN_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOBS_ROTTEN_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOBS_ROTTEN_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 44.COLLECTIBLE_TELEPORT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TELEPORT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TELEPORT,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 45.COLLECTIBLE_YUM_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_YUM_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_YUM_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 46.COLLECTIBLE_LUCKY_FOOT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LUCKY_FOOT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LUCKY_FOOT,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 47.COLLECTIBLE_DOCTORS_REMOTE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DOCTORS_REMOTE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DOCTORS_REMOTE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 48.COLLECTIBLE_CUPIDS_ARROW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CUPIDS_ARROW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CUPIDS_ARROW,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 49.COLLECTIBLE_SHOOP_DA_WHOOP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SHOOP_DA_WHOOP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SHOOP_DA_WHOOP,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 50.COLLECTIBLE_STEVEN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STEVEN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STEVEN,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 51.COLLECTIBLE_PENTAGRAM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PENTAGRAM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PENTAGRAM,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 52.COLLECTIBLE_DR_FETUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DR_FETUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DR_FETUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 53.COLLECTIBLE_MAGNETO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGNETO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGNETO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 54.COLLECTIBLE_TREASURE_MAP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TREASURE_MAP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TREASURE_MAP,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 55.COLLECTIBLE_MOMS_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 56.COLLECTIBLE_LEMON_MISHAP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LEMON_MISHAP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LEMON_MISHAP,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 57.COLLECTIBLE_DISTANT_ADMIRATION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DISTANT_ADMIRATION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DISTANT_ADMIRATION,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 58.COLLECTIBLE_BOOK_OF_SHADOWS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOK_OF_SHADOWS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_SHADOWS,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
    },
})
Encyclopedia.AddItem({ -- 60.COLLECTIBLE_LADDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LADDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LADDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 62.COLLECTIBLE_CHARM_VAMPIRE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CHARM_VAMPIRE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CHARM_VAMPIRE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 63.COLLECTIBLE_BATTERY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BATTERY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BATTERY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 64.COLLECTIBLE_STEAM_SALE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STEAM_SALE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STEAM_SALE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 65.COLLECTIBLE_ANARCHIST_COOKBOOK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANARCHIST_COOKBOOK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANARCHIST_COOKBOOK,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 67.COLLECTIBLE_HOURGLASS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOURGLASS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOURGLASS,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 66.COLLECTIBLE_SISTER_MAGGY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SISTER_MAGGY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SISTER_MAGGY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 68.COLLECTIBLE_TECHNOLOGY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TECHNOLOGY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TECHNOLOGY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 69.COLLECTIBLE_CHOCOLATE_MILK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CHOCOLATE_MILK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CHOCOLATE_MILK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 70.COLLECTIBLE_GROWTH_HORMONES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GROWTH_HORMONES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GROWTH_HORMONES,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 71.COLLECTIBLE_MINI_MUSH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MINI_MUSH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MINI_MUSH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 72.COLLECTIBLE_ROSARY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROSARY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROSARY,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 73.COLLECTIBLE_CUBE_OF_MEAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CUBE_OF_MEAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CUBE_OF_MEAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 74.COLLECTIBLE_QUARTER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_QUARTER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_QUARTER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 75.COLLECTIBLE_PHD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PHD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PHD,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 76.COLLECTIBLE_XRAY_VISION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_XRAY_VISION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_XRAY_VISION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 77.COLLECTIBLE_MY_LITTLE_UNICORN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MY_LITTLE_UNICORN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MY_LITTLE_UNICORN,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 78.COLLECTIBLE_BOOK_OF_REVELATIONS
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_BOOK_OF_REVELATIONS" or "COLLECTIBLE_BOOK_REVELATIONS"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_REVELATIONS,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 79.COLLECTIBLE_MARK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MARK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MARK,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 80.COLLECTIBLE_PACT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PACT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PACT,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 81.COLLECTIBLE_DEAD_CAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_CAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_CAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 82.COLLECTIBLE_LORD_OF_THE_PIT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LORD_OF_THE_PIT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LORD_OF_THE_PIT,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 83.COLLECTIBLE_THE_NAIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_THE_NAIL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_THE_NAIL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 85.COLLECTIBLE_WE_NEED_TO_GO_DEEPER
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_WE_NEED_TO_GO_DEEPER" or "COLLECTIBLE_WE_NEED_GO_DEEPER"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WE_NEED_TO_GO_DEEPER,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 84.COLLECTIBLE_DECK_OF_CARDS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DECK_OF_CARDS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DECK_OF_CARDS,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 86.COLLECTIBLE_MONSTROS_TOOTH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MONSTROS_TOOTH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONSTROS_TOOTH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 87.COLLECTIBLE_LOKIS_HORNS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LOKIS_HORNS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LOKIS_HORNS,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 88.COLLECTIBLE_LITTLE_CHUBBY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LITTLE_CHUBBY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LITTLE_CHUBBY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 89.COLLECTIBLE_SPIDER_BITE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIDER_BITE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIDER_BITE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 90.COLLECTIBLE_SMALL_ROCK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SMALL_ROCK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SMALL_ROCK,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
    },
})
Encyclopedia.AddItem({ -- 91.COLLECTIBLE_SPELUNKER_HAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPELUNKER_HAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPELUNKER_HAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 92.COLLECTIBLE_SUPER_BANDAGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SUPER_BANDAGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SUPER_BANDAGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 93.COLLECTIBLE_GAMEKID
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GAMEKID,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GAMEKID,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 94.COLLECTIBLE_SACK_OF_PENNIES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACK_OF_PENNIES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACK_OF_PENNIES,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 95.COLLECTIBLE_ROBO_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROBO_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROBO_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 96.COLLECTIBLE_LITTLE_CHAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LITTLE_CHAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LITTLE_CHAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 97.COLLECTIBLE_BOOK_OF_SIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOK_OF_SIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_SIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 98.COLLECTIBLE_RELIC
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RELIC,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RELIC,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 99.COLLECTIBLE_LITTLE_GISH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LITTLE_GISH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LITTLE_GISH,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 100.COLLECTIBLE_LITTLE_STEVEN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LITTLE_STEVEN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LITTLE_STEVEN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 101.COLLECTIBLE_HALO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HALO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HALO,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 102.COLLECTIBLE_MOMS_BOTTLE_OF_PILLS
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_MOMS_BOTTLE_OF_PILLS" or "COLLECTIBLE_MOMS_BOTTLE_PILLS"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_BOTTLE_OF_PILLS,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 103.COLLECTIBLE_COMMON_COLD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_COMMON_COLD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_COMMON_COLD,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 104.COLLECTIBLE_PARASITE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PARASITE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PARASITE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 105.COLLECTIBLE_D6
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D6,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D6,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 106.COLLECTIBLE_MR_MEGA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MR_MEGA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MR_MEGA,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 107.COLLECTIBLE_PINKING_SHEARS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PINKING_SHEARS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PINKING_SHEARS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 108.COLLECTIBLE_WAFER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WAFER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WAFER,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 109.COLLECTIBLE_MONEY_EQUALS_POWER
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_MONEY_EQUALS_POWER" or "COLLECTIBLE_MONEY_IS_POWER"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONEY_EQUALS_POWER,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 110.COLLECTIBLE_MOMS_CONTACTS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_CONTACTS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_CONTACTS,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 111.COLLECTIBLE_BEAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BEAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BEAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 113.COLLECTIBLE_GUARDIAN_ANGEL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUARDIAN_ANGEL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUARDIAN_ANGEL,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 112.COLLECTIBLE_DEMON_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEMON_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEMON_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 114.COLLECTIBLE_MOMS_KNIFE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_KNIFE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_KNIFE,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 115.COLLECTIBLE_OUIJA_BOARD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_OUIJA_BOARD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_OUIJA_BOARD,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 116.COLLECTIBLE_9_VOLT
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_9_VOLT" or "COLLECTIBLE_NINE_VOLT"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_9_VOLT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 118.COLLECTIBLE_DEAD_BIRD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_BIRD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_BIRD,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,

    },
})
Encyclopedia.AddItem({ -- 117.COLLECTIBLE_BRIMSTONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BRIMSTONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BRIMSTONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 119.COLLECTIBLE_BLOOD_BAG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOOD_BAG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_BAG,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 120.COLLECTIBLE_ODD_MUSHROOM_THIN
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_ODD_MUSHROOM_THIN" or "COLLECTIBLE_ODD_MUSHROOM_RATE"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ODD_MUSHROOM_THIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 121.COLLECTIBLE_ODD_MUSHROOM_LARGE
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_ODD_MUSHROOM_LARGE" or "COLLECTIBLE_ODD_MUSHROOM_DAMAGE"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ODD_MUSHROOM_LARGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 122.COLLECTIBLE_WHORE_OF_BABYLON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WHORE_OF_BABYLON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WHORE_OF_BABYLON,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 123.COLLECTIBLE_MONSTER_MANUAL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MONSTER_MANUAL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONSTER_MANUAL,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
    },
})
Encyclopedia.AddItem({ -- 124.COLLECTIBLE_DEAD_SEA_SCROLLS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_SEA_SCROLLS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_SEA_SCROLLS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 125.COLLECTIBLE_BOBBY_BOMB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOBBY_BOMB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOBBY_BOMB,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 126.COLLECTIBLE_RAZOR_BLADE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RAZOR_BLADE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RAZOR_BLADE,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 127.COLLECTIBLE_FORGET_ME_NOW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FORGET_ME_NOW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FORGET_ME_NOW,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 128.COLLECTIBLE_FOREVER_ALONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FOREVER_ALONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FOREVER_ALONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 129.COLLECTIBLE_BUCKET_OF_LARD
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_BUCKET_OF_LARD" or "COLLECTIBLE_BUCKET_LARD"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BUCKET_OF_LARD,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 130.COLLECTIBLE_PONY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PONY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PONY,
})
Encyclopedia.AddItem({ -- 131.COLLECTIBLE_BOMB_BAG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOMB_BAG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOMB_BAG,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 132.COLLECTIBLE_LUMP_OF_COAL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LUMP_OF_COAL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LUMP_OF_COAL,
    Pools = {
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 133.COLLECTIBLE_GUPPYS_PAW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUPPYS_PAW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUPPYS_PAW,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 134.COLLECTIBLE_GUPPYS_TAIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUPPYS_TAIL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUPPYS_TAIL,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 135.COLLECTIBLE_IV_BAG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IV_BAG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IV_BAG,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 136.COLLECTIBLE_BEST_FRIEND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BEST_FRIEND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BEST_FRIEND,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 137.COLLECTIBLE_REMOTE_DETONATOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_REMOTE_DETONATOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_REMOTE_DETONATOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 140.COLLECTIBLE_MOMS_PURSE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_PURSE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_PURSE,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 138.COLLECTIBLE_BOBS_CURSE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOBS_CURSE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOBS_CURSE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 139.COLLECTIBLE_STIGMATA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STIGMATA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STIGMATA,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 141.COLLECTIBLE_PAGEANT_BOY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PAGEANT_BOY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PAGEANT_BOY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 142.COLLECTIBLE_SCAPULAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCAPULAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCAPULAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 143.COLLECTIBLE_SPEED_BALL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPEED_BALL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPEED_BALL,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 144.COLLECTIBLE_BUM_FRIEND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BUM_FRIEND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BUM_FRIEND,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 145.COLLECTIBLE_GUPPYS_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUPPYS_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUPPYS_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
    },
})
Encyclopedia.AddItem({ -- 146.COLLECTIBLE_PRAYER_CARD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PRAYER_CARD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PRAYER_CARD,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 147.COLLECTIBLE_NOTCHED_AXE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NOTCHED_AXE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NOTCHED_AXE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 148.COLLECTIBLE_INFESTATION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INFESTATION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INFESTATION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 149.COLLECTIBLE_IPECAC
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IPECAC,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IPECAC,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 150.COLLECTIBLE_TOUGH_LOVE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TOUGH_LOVE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TOUGH_LOVE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 151.COLLECTIBLE_MULLIGAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MULLIGAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MULLIGAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 152.COLLECTIBLE_TECHNOLOGY_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TECHNOLOGY_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TECHNOLOGY_2,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 153.COLLECTIBLE_MUTANT_SPIDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MUTANT_SPIDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MUTANT_SPIDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 154.COLLECTIBLE_CHEMICAL_PEEL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CHEMICAL_PEEL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CHEMICAL_PEEL,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 155.COLLECTIBLE_PEEPER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PEEPER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PEEPER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 157.COLLECTIBLE_HABIT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HABIT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HABIT,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 156.COLLECTIBLE_BLOODY_LUST
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOODY_LUST,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOODY_LUST,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 158.COLLECTIBLE_CRYSTAL_BALL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CRYSTAL_BALL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CRYSTAL_BALL,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 159.COLLECTIBLE_SPIRIT_OF_THE_NIGHT
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_SPIRIT_OF_THE_NIGHT" or "COLLECTIBLE_SPIRIT_NIGHT"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIRIT_OF_THE_NIGHT,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 160.COLLECTIBLE_CRACK_THE_SKY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CRACK_THE_SKY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CRACK_THE_SKY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 161.COLLECTIBLE_ANKH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANKH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANKH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 162.COLLECTIBLE_CELTIC_CROSS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CELTIC_CROSS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CELTIC_CROSS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 163.COLLECTIBLE_GHOST_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GHOST_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GHOST_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 164.COLLECTIBLE_CANDLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CANDLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CANDLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 165.COLLECTIBLE_CAT_O_NINE_TAILS
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_CAT_O_NINE_TAILS" or "COLLECTIBLE_CAT_NINE_TAILS"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAT_O_NINE_TAILS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 166.COLLECTIBLE_D20
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D20,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D20,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 167.COLLECTIBLE_HARLEQUIN_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HARLEQUIN_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HARLEQUIN_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 168.COLLECTIBLE_EPIC_FETUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EPIC_FETUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EPIC_FETUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 169.COLLECTIBLE_POLYPHEMUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POLYPHEMUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POLYPHEMUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 170.COLLECTIBLE_DADDY_LONGLEGS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DADDY_LONGLEGS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DADDY_LONGLEGS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 171.COLLECTIBLE_SPIDER_BUTT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIDER_BUTT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIDER_BUTT,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 172.COLLECTIBLE_SACRIFICIAL_DAGGER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACRIFICIAL_DAGGER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACRIFICIAL_DAGGER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 173.COLLECTIBLE_MITRE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MITRE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MITRE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 174.COLLECTIBLE_RAINBOW_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RAINBOW_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RAINBOW_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 175.COLLECTIBLE_DADS_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DADS_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DADS_KEY,
    Pools = {
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 176.COLLECTIBLE_STEM_CELLS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STEM_CELLS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STEM_CELLS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 177.COLLECTIBLE_PORTABLE_SLOT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PORTABLE_SLOT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PORTABLE_SLOT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 178.COLLECTIBLE_HOLY_WATER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOLY_WATER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOLY_WATER,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 179.COLLECTIBLE_FATE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FATE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FATE,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
    },
})
Encyclopedia.AddItem({ -- 180.COLLECTIBLE_BLACK_BEAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLACK_BEAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLACK_BEAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 181.COLLECTIBLE_WHITE_PONY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WHITE_PONY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WHITE_PONY,
})
Encyclopedia.AddItem({ -- 182.COLLECTIBLE_SACRED_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACRED_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACRED_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 183.COLLECTIBLE_TOOTH_PICKS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TOOTH_PICKS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TOOTH_PICKS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 185.COLLECTIBLE_HOLY_GRAIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOLY_GRAIL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOLY_GRAIL,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 184.COLLECTIBLE_DEAD_DOVE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_DOVE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_DOVE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 186.COLLECTIBLE_BLOOD_RIGHTS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOOD_RIGHTS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_RIGHTS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 187.COLLECTIBLE_GUPPYS_HAIRBALL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUPPYS_HAIRBALL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUPPYS_HAIRBALL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 188.COLLECTIBLE_ABEL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ABEL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ABEL,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 190.COLLECTIBLE_SMB_SUPER_FAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SMB_SUPER_FAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SMB_SUPER_FAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 189.COLLECTIBLE_PYRO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PYRO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PYRO,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 191.COLLECTIBLE_3_DOLLAR_BILL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_3_DOLLAR_BILL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_3_DOLLAR_BILL,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 192.COLLECTIBLE_TELEPATHY_BOOK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TELEPATHY_BOOK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TELEPATHY_BOOK,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 193.COLLECTIBLE_MEAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MEAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MEAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 194.COLLECTIBLE_MAGIC_8_BALL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGIC_8_BALL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGIC_8_BALL,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 196.COLLECTIBLE_MOMS_COIN_PURSE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_COIN_PURSE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_COIN_PURSE,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 195.COLLECTIBLE_SQUEEZY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SQUEEZY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SQUEEZY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 197.COLLECTIBLE_JESUS_JUICE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JESUS_JUICE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JESUS_JUICE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 198.COLLECTIBLE_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 199.COLLECTIBLE_MOMS_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_KEY,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 201.COLLECTIBLE_MOMS_EYESHADOW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_EYESHADOW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_EYESHADOW,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 200.COLLECTIBLE_IRON_BAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IRON_BAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IRON_BAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 202.COLLECTIBLE_MIDAS_TOUCH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MIDAS_TOUCH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MIDAS_TOUCH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 203.COLLECTIBLE_HUMBLEING_BUNDLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HUMBLEING_BUNDLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HUMBLEING_BUNDLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 204.COLLECTIBLE_FANNY_PACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FANNY_PACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FANNY_PACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 205.COLLECTIBLE_SHARP_PLUG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SHARP_PLUG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SHARP_PLUG,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 206.COLLECTIBLE_GUILLOTINE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUILLOTINE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUILLOTINE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 207.COLLECTIBLE_BALL_OF_BANDAGES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BALL_OF_BANDAGES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BALL_OF_BANDAGES,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
    },
})
Encyclopedia.AddItem({ -- 208.COLLECTIBLE_CHAMPION_BELT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CHAMPION_BELT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CHAMPION_BELT,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 209.COLLECTIBLE_BUTT_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BUTT_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BUTT_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 210.COLLECTIBLE_GNAWED_LEAF
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GNAWED_LEAF,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GNAWED_LEAF,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 211.COLLECTIBLE_SPIDERBABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIDERBABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIDERBABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 212.COLLECTIBLE_GUPPYS_COLLAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUPPYS_COLLAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUPPYS_COLLAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 213.COLLECTIBLE_LOST_CONTACT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LOST_CONTACT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LOST_CONTACT,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 214.COLLECTIBLE_ANEMIC
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANEMIC,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANEMIC,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 215.COLLECTIBLE_GOAT_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GOAT_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GOAT_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 216.COLLECTIBLE_CEREMONIAL_ROBES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CEREMONIAL_ROBES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CEREMONIAL_ROBES,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 217.COLLECTIBLE_MOMS_WIG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_WIG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_WIG,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 218.COLLECTIBLE_PLACENTA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PLACENTA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PLACENTA,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 219.COLLECTIBLE_OLD_BANDAGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_OLD_BANDAGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_OLD_BANDAGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 221.COLLECTIBLE_SAD_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SAD_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SAD_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 220.COLLECTIBLE_RUBBER_CEMENT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RUBBER_CEMENT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RUBBER_CEMENT,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 222.COLLECTIBLE_ANTI_GRAVITY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANTI_GRAVITY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANTI_GRAVITY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 223.COLLECTIBLE_PYROMANIAC
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PYROMANIAC,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PYROMANIAC,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 224.COLLECTIBLE_CRICKETS_BODY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CRICKETS_BODY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CRICKETS_BODY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 225.COLLECTIBLE_GIMPY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GIMPY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GIMPY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 226.COLLECTIBLE_BLACK_LOTUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLACK_LOTUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLACK_LOTUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 227.COLLECTIBLE_PIGGY_BANK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PIGGY_BANK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PIGGY_BANK,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 228.COLLECTIBLE_MOMS_PERFUME
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_PERFUME,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_PERFUME,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 229.COLLECTIBLE_MONSTROS_LUNG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MONSTROS_LUNG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONSTROS_LUNG,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 230.COLLECTIBLE_ABADDON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ABADDON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ABADDON,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 231.COLLECTIBLE_BALL_OF_TAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BALL_OF_TAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BALL_OF_TAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 232.COLLECTIBLE_STOP_WATCH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STOP_WATCH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STOP_WATCH,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 233.COLLECTIBLE_INFESTATION_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INFESTATION_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INFESTATION_2,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TRASURE,
    },
})
Encyclopedia.AddItem({ -- 234.COLLECTIBLE_TINY_PLANET
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TINY_PLANET,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TINY_PLANET,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 236.COLLECTIBLE_E_COLI
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_E_COLI,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_E_COLI,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 237.COLLECTIBLE_DEATHS_TOUCH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEATHS_TOUCH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEATHS_TOUCH,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 238.COLLECTIBLE_KEY_PIECE_1
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KEY_PIECE_1,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KEY_PIECE_1,
})
Encyclopedia.AddItem({ -- 239.COLLECTIBLE_KEY_PIECE_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KEY_PIECE_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KEY_PIECE_2,
})
Encyclopedia.AddItem({ -- 240.COLLECTIBLE_EXPERIMENTAL_TREATMENT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EXPERIMENTAL_TREATMENT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EXPERIMENTAL_TREATMENT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 241.COLLECTIBLE_CONTRACT_FROM_BELOW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CONTRACT_FROM_BELOW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CONTRACT_FROM_BELOW,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 244.COLLECTIBLE_TRINITY_SHIELD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TRINITY_SHIELD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TRINITY_SHIELD,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 242.COLLECTIBLE_TECH_5
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TECH_5,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TECH_5,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 243.COLLECTIBLE_INFAMY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INFAMY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INFAMY,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 245.COLLECTIBLE_20_20
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_20_20,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_20_20,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 246.COLLECTIBLE_BLUE_MAP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLUE_MAP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLUE_MAP,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 247.COLLECTIBLE_BFFS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BFFS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BFFS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 249.COLLECTIBLE_HIVE_MIND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HIVE_MIND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HIVE_MIND,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 248.COLLECTIBLE_THERES_OPTIONS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_THERES_OPTIONS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_THERES_OPTIONS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 250.COLLECTIBLE_BOGO_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOGO_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOGO_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 251.COLLECTIBLE_STARTER_DECK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STARTER_DECK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STARTER_DECK,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 252.COLLECTIBLE_LITTLE_BAGGY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LITTLE_BAGGY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LITTLE_BAGGY,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 255.COLLECTIBLE_BLOOD_CLOT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOOD_CLOT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_CLOT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 253.COLLECTIBLE_SCREW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCREW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCREW,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 254.COLLECTIBLE_MAGIC_SCAB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGIC_SCAB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGIC_SCAB,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 256.COLLECTIBLE_HOT_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOT_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOT_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 257.COLLECTIBLE_FIRE_MIND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FIRE_MIND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FIRE_MIND,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 258.COLLECTIBLE_MISSING_NO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MISSING_NO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MISSING_NO,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 259.COLLECTIBLE_DARK_MATTER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DARK_MATTER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DARK_MATTER,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 260.COLLECTIBLE_BLACK_CANDLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLACK_CANDLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLACK_CANDLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 261.COLLECTIBLE_PROPTOSIS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PROPTOSIS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PROPTOSIS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 262.COLLECTIBLE_MISSING_PAGE_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MISSING_PAGE_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MISSING_PAGE_2,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
if REPENTANCE then -- 263.COLLECTIBLE_CLEAR_RUNE
Encyclopedia.AddItem({ -- 263.COLLECTIBLE_CLEAR_RUNE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CLEAR_RUNE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CLEAR_RUNE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
end
Encyclopedia.AddItem({ -- 264.COLLECTIBLE_SMART_FLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SMART_FLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SMART_FLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 265.COLLECTIBLE_DRY_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DRY_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DRY_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 266.COLLECTIBLE_JUICY_SACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JUICY_SACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JUICY_SACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 267.COLLECTIBLE_ROBO_BABY_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROBO_BABY_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROBO_BABY_2,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 268.COLLECTIBLE_ROTTEN_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROTTEN_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROTTEN_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 269.COLLECTIBLE_HEADLESS_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HEADLESS_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HEADLESS_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 270.COLLECTIBLE_LEECH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LEECH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LEECH,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 272.COLLECTIBLE_MYSTERY_SACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MYSTERY_SACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MYSTERY_SACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 271.COLLECTIBLE_BBF
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BBF,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BBF,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 273.COLLECTIBLE_BOBS_BRAIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOBS_BRAIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOBS_BRAIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 274.COLLECTIBLE_BEST_BUD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BEST_BUD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BEST_BUD,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 275.COLLECTIBLE_LIL_BRIMSTONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_BRIMSTONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_BRIMSTONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 277.COLLECTIBLE_ISAACS_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ISAACS_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ISAACS_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 276.COLLECTIBLE_LIL_HAUNT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_HAUNT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_HAUNT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 278.COLLECTIBLE_DARK_BUM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DARK_BUM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DARK_BUM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 279.COLLECTIBLE_BIG_FAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BIG_FAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BIG_FAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 280.COLLECTIBLE_SISSY_LONGLEGS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SISSY_LONGLEGS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SISSY_LONGLEGS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 281.COLLECTIBLE_PUNCHING_BAG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PUNCHING_BAG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PUNCHING_BAG,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 282.COLLECTIBLE_HOW_TO_JUMP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOW_TO_JUMP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOW_TO_JUMP,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 283.COLLECTIBLE_D100
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D100,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D100,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 284.COLLECTIBLE_D4
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D4,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D4,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 285.COLLECTIBLE_D10
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D10,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D10,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 286.COLLECTIBLE_BLANK_CARD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLANK_CARD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLANK_CARD,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 287.COLLECTIBLE_BOOK_OF_SECRETS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOK_OF_SECRETS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_SECRETS,
    Pools = {
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
    },
})
Encyclopedia.AddItem({ -- 288.COLLECTIBLE_BOX_OF_SPIDERS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOX_OF_SPIDERS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOX_OF_SPIDERS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 289.COLLECTIBLE_RED_CANDLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RED_CANDLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RED_CANDLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 290.COLLECTIBLE_THE_JAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_THE_JAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_THE_JAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 291.COLLECTIBLE_FLUSH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FLUSH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FLUSH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 292.COLLECTIBLE_SATANIC_BIBLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SATANIC_BIBLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SATANIC_BIBLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
    },
})
Encyclopedia.AddItem({ -- 293.COLLECTIBLE_HEAD_OF_KRAMPUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HEAD_OF_KRAMPUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HEAD_OF_KRAMPUS,
})
Encyclopedia.AddItem({ -- 294.COLLECTIBLE_BUTTER_BEAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BUTTER_BEAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BUTTER_BEAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 296.COLLECTIBLE_MAGIC_FINGERS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGIC_FINGERS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGIC_FINGERS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 295.COLLECTIBLE_CONVERTER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CONVERTER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CONVERTER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 297.COLLECTIBLE_BLUE_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLUE_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLUE_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 298.COLLECTIBLE_UNICORN_STUMP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_UNICORN_STUMP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_UNICORN_STUMP,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 299.COLLECTIBLE_TAURUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TAURUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TAURUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 300.COLLECTIBLE_ARIES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ARIES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ARIES,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 301.COLLECTIBLE_CANCER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CANCER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CANCER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 302.COLLECTIBLE_LEO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LEO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LEO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 303.COLLECTIBLE_VIRGO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VIRGO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VIRGO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 304.COLLECTIBLE_LIBRA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIBRA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIBRA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 305.COLLECTIBLE_SCORPIO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCORPIO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCORPIO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 306.COLLECTIBLE_SAGITTARIUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SAGITTARIUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SAGITTARIUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 307.COLLECTIBLE_CAPRICORN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CAPRICORN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAPRICORN,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 308.COLLECTIBLE_AQUARIUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_AQUARIUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_AQUARIUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 309.COLLECTIBLE_PISCES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PISCES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PISCES,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 310.COLLECTIBLE_EVES_MASCARA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EVES_MASCARA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EVES_MASCARA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 311.COLLECTIBLE_JUDAS_SHADOW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JUDAS_SHADOW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JUDAS_SHADOW,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 312.COLLECTIBLE_MAGGYS_BOW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGGYS_BOW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGGYS_BOW,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 313.COLLECTIBLE_HOLY_MANTLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOLY_MANTLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOLY_MANTLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 314.COLLECTIBLE_THUNDER_THIGHS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_THUNDER_THIGHS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_THUNDER_THIGHS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 315.COLLECTIBLE_STRANGE_ATTRACTOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STRANGE_ATTRACTOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STRANGE_ATTRACTOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 316.COLLECTIBLE_CURSED_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CURSED_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CURSED_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 317.COLLECTIBLE_MYSTERIOUS_LIQUID
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MYSTERIOUS_LIQUID,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MYSTERIOUS_LIQUID,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 318.COLLECTIBLE_GEMINI
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GEMINI,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GEMINI,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 321.COLLECTIBLE_BLUE_BABYS_ONLY_FRIEND
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_BLUE_BABYS_ONLY_FRIEND" or "COLLECTIBLE_BLUEBABYS_ONLY_FRIEND"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLUE_BABYS_ONLY_FRIEND,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 319.COLLECTIBLE_SAMSONS_CHAINS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SAMSONS_CHAINS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SAMSONS_CHAINS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 320.COLLECTIBLE_CAINS_OTHER_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CAINS_OTHER_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAINS_OTHER_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 322.COLLECTIBLE_MONGO_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MONGO_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONGO_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 323.COLLECTIBLE_ISAACS_TEARS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ISAACS_TEARS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ISAACS_TEARS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 324.COLLECTIBLE_UNDEFINED
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_UNDEFINED,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_UNDEFINED,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 325.COLLECTIBLE_SCISSORS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCISSORS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCISSORS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 326.COLLECTIBLE_BREATH_OF_LIFE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BREATH_OF_LIFE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BREATH_OF_LIFE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 327.COLLECTIBLE_POLAROID
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POLAROID,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POLAROID,
})
Encyclopedia.AddItem({ -- 328.COLLECTIBLE_NEGATIVE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NEGATIVE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NEGATIVE,
})
Encyclopedia.AddItem({ -- 329.COLLECTIBLE_LUDOVICO_TECHNIQUE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LUDOVICO_TECHNIQUE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LUDOVICO_TECHNIQUE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 330.COLLECTIBLE_SOY_MILK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SOY_MILK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SOY_MILK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 331.COLLECTIBLE_GODHEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GODHEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GODHEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 332.COLLECTIBLE_LAZARUS_RAGS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LAZARUS_RAGS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LAZARUS_RAGS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 333.COLLECTIBLE_MIND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MIND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MIND,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 337.COLLECTIBLE_DEAD_ONION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_ONION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_ONION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 334.COLLECTIBLE_BROKEN_WATCH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BROKEN_WATCH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BROKEN_WATCH,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 335.COLLECTIBLE_BODY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BODY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BODY,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,

    },
})
Encyclopedia.AddItem({ -- 336.COLLECTIBLE_SOUL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SOUL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SOUL,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 338.COLLECTIBLE_BOOMERANG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOMERANG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOMERANG,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 339.COLLECTIBLE_SAFETY_PIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SAFETY_PIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SAFETY_PIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 340.COLLECTIBLE_CAFFEINE_PILL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CAFFEINE_PILL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAFFEINE_PILL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 342.COLLECTIBLE_TORN_PHOTO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TORN_PHOTO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TORN_PHOTO,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 341.COLLECTIBLE_BLUE_CAP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLUE_CAP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLUE_CAP,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 343.COLLECTIBLE_LATCH_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LATCH_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LATCH_KEY,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 344.COLLECTIBLE_MATCH_BOOK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MATCH_BOOK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MATCH_BOOK,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 345.COLLECTIBLE_SYNTHOIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SYNTHOIL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SYNTHOIL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 346.COLLECTIBLE_SNACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SNACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SNACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 347.COLLECTIBLE_DIPLOPIA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DIPLOPIA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DIPLOPIA,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 348.COLLECTIBLE_PLACEBO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PLACEBO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PLACEBO,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 349.COLLECTIBLE_WOODEN_NICKEL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WOODEN_NICKEL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WOODEN_NICKEL,
    Pools = {
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 350.COLLECTIBLE_TOXIC_SHOCK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TOXIC_SHOCK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TOXIC_SHOCK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 351.COLLECTIBLE_MEGA_BEAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MEGA_BEAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MEGA_BEAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 352.COLLECTIBLE_GLASS_CANNON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLASS_CANNON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLASS_CANNON,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 353.COLLECTIBLE_BOMBER_BOY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOMBER_BOY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOMBER_BOY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 354.COLLECTIBLE_CRACK_JACKS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CRACK_JACKS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CRACK_JACKS,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 355.COLLECTIBLE_MOMS_PEARLS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_PEARLS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_PEARLS,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 356.COLLECTIBLE_CAR_BATTERY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CAR_BATTERY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAR_BATTERY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 357.COLLECTIBLE_BOX_OF_FRIENDS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOX_OF_FRIENDS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOX_OF_FRIENDS,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 358.COLLECTIBLE_THE_WIZ
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_THE_WIZ,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_THE_WIZ,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 359.COLLECTIBLE_8_INCH_NAILS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_8_INCH_NAILS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_8_INCH_NAILS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 360.COLLECTIBLE_INCUBUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INCUBUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INCUBUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 361.COLLECTIBLE_FATES_REWARD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FATES_REWARD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FATES_REWARD,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_GOLDEN_CHEST,
    },
})
Encyclopedia.AddItem({ -- 362.COLLECTIBLE_LIL_CHEST
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_CHEST,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_CHEST,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 363.COLLECTIBLE_SWORN_PROTECTOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SWORN_PROTECTOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SWORN_PROTECTOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 364.COLLECTIBLE_FRIEND_ZONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FRIEND_ZONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FRIEND_ZONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 365.COLLECTIBLE_LOST_FLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LOST_FLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LOST_FLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 366.COLLECTIBLE_SCATTER_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCATTER_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCATTER_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 367.COLLECTIBLE_STICKY_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STICKY_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STICKY_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 368.COLLECTIBLE_EPIPHORA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EPIPHORA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EPIPHORA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 369.COLLECTIBLE_CONTINUUM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CONTINUUM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CONTINUUM,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 370.COLLECTIBLE_MR_DOLLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MR_DOLLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MR_DOLLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 371.COLLECTIBLE_CURSE_OF_THE_TOWER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CURSE_OF_THE_TOWER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CURSE_OF_THE_TOWER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 372.COLLECTIBLE_CHARGED_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CHARGED_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CHARGED_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 373.COLLECTIBLE_DEAD_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 374.COLLECTIBLE_HOLY_LIGHT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOLY_LIGHT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOLY_LIGHT,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 375.COLLECTIBLE_HOST_HAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOST_HAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOST_HAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 376.COLLECTIBLE_RESTOCK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RESTOCK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RESTOCK,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 377.COLLECTIBLE_BURSTING_SACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BURSTING_SACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BURSTING_SACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 378.COLLECTIBLE_NUMBER_TWO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NUMBER_TWO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NUMBER_TWO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 379.COLLECTIBLE_PUPULA_DUPLEX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PUPULA_DUPLEX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PUPULA_DUPLEX,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 380.COLLECTIBLE_PAY_TO_PLAY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PAY_TO_PLAY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PAY_TO_PLAY,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 381.COLLECTIBLE_EDENS_BLESSING
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EDENS_BLESSING,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EDENS_BLESSING,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 382.COLLECTIBLE_FRIEND_BALL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FRIEND_BALL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FRIEND_BALL,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 385.COLLECTIBLE_LIL_GURDY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_GURDY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_GURDY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 383.COLLECTIBLE_BUMBO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BUMBO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BUMBO,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 384.COLLECTIBLE_TEAR_DETONATOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TEAR_DETONATOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TEAR_DETONATOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 386.COLLECTIBLE_D12
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D12,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D12,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 387.COLLECTIBLE_CENSER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CENSER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CENSER,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 388.COLLECTIBLE_KEY_BUM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KEY_BUM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KEY_BUM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 390.COLLECTIBLE_RUNE_BAG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RUNE_BAG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RUNE_BAG,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 389.COLLECTIBLE_SERAPHIM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SERAPHIM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SERAPHIM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 391.COLLECTIBLE_BETRAYAL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BETRAYAL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BETRAYAL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 392.COLLECTIBLE_ZODIAC
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ZODIAC,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ZODIAC,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 393.COLLECTIBLE_SERPENTS_KISS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SERPENTS_KISS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SERPENTS_KISS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 394.COLLECTIBLE_MARKED
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MARKED,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MARKED,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 395.COLLECTIBLE_TECH_X
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TECH_X,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TECH_X,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 396.COLLECTIBLE_VENTRICLE_RAZOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VENTRICLE_RAZOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VENTRICLE_RAZOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 397.COLLECTIBLE_TRACTOR_BEAM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TRACTOR_BEAM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TRACTOR_BEAM,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 398.COLLECTIBLE_GODS_FLESH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GODS_FLESH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GODS_FLESH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 399.COLLECTIBLE_MAW_OF_THE_VOID
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_MAW_OF_THE_VOID" or "COLLECTIBLE_MAW_OF_VOID"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAW_OF_THE_VOID,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 400.COLLECTIBLE_SPEAR_OF_DESTINY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPEAR_OF_DESTINY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPEAR_OF_DESTINY,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 401.COLLECTIBLE_EXPLOSIVO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EXPLOSIVO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EXPLOSIVO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 402.COLLECTIBLE_CHAOS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CHAOS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CHAOS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 403.COLLECTIBLE_SPIDER_MOD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIDER_MOD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIDER_MOD,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 404.COLLECTIBLE_FARTING_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FARTING_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FARTING_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 405.COLLECTIBLE_GB_BUG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GB_BUG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GB_BUG,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 406.COLLECTIBLE_D8
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D8,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D8,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 407.COLLECTIBLE_PURITY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PURITY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PURITY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 410.COLLECTIBLE_EMPTY_VESSEL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EMPTY_VESSEL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EMPTY_VESSEL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 408.COLLECTIBLE_EVIL_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EVIL_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EVIL_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 409.COLLECTIBLE_ATHAME
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ATHAME,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ATHAME,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 411.COLLECTIBLE_LUSTY_BLOOD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LUSTY_BLOOD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LUSTY_BLOOD,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 412.COLLECTIBLE_CAMBION_CONCEPTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CAMBION_CONCEPTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAMBION_CONCEPTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 413.COLLECTIBLE_IMMACULATE_CONCEPTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IMMACULATE_CONCEPTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IMMACULATE_CONCEPTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 414.COLLECTIBLE_MORE_OPTIONS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MORE_OPTIONS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MORE_OPTIONS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 415.COLLECTIBLE_CROWN_OF_LIGHT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CROWN_OF_LIGHT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CROWN_OF_LIGHT,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 416.COLLECTIBLE_DEEP_POCKETS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEEP_POCKETS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEEP_POCKETS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 417.COLLECTIBLE_SUCCUBUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SUCCUBUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SUCCUBUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
    },
})
Encyclopedia.AddItem({ -- 418.COLLECTIBLE_FRUIT_CAKE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FRUIT_CAKE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FRUIT_CAKE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 419.COLLECTIBLE_TELEPORT_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TELEPORT_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TELEPORT_2,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 420.COLLECTIBLE_BLACK_POWDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLACK_POWDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLACK_POWDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 421.COLLECTIBLE_KIDNEY_BEAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KIDNEY_BEAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KIDNEY_BEAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 422.COLLECTIBLE_GLOWING_HOUR_GLASS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLOWING_HOUR_GLASS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLOWING_HOUR_GLASS,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 423.COLLECTIBLE_CIRCLE_OF_PROTECTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CIRCLE_OF_PROTECTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CIRCLE_OF_PROTECTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 424.COLLECTIBLE_SACK_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACK_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACK_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 425.COLLECTIBLE_NIGHT_LIGHT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NIGHT_LIGHT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NIGHT_LIGHT,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 426.COLLECTIBLE_OBSESSED_FAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_OBSESSED_FAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_OBSESSED_FAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 427.COLLECTIBLE_MINE_CRAFTER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MINE_CRAFTER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MINE_CRAFTER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 428.COLLECTIBLE_PJS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PJS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PJS,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 429.COLLECTIBLE_HEAD_OF_THE_KEEPER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HEAD_OF_THE_KEEPER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HEAD_OF_THE_KEEPER,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
    },
})
Encyclopedia.AddItem({ -- 430.COLLECTIBLE_PAPA_FLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PAPA_FLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PAPA_FLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 431.COLLECTIBLE_MULTIDIMENSIONAL_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MULTIDIMENSIONAL_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MULTIDIMENSIONAL_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 432.COLLECTIBLE_GLITTER_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLITTER_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLITTER_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 433.COLLECTIBLE_MY_SHADOW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MY_SHADOW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MY_SHADOW,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 434.COLLECTIBLE_JAR_OF_FLIES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JAR_OF_FLIES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JAR_OF_FLIES,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 435.COLLECTIBLE_LIL_LOKI
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_LOKI,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_LOKI,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 436.COLLECTIBLE_MILK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MILK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MILK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 437.COLLECTIBLE_D7
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D7,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D7,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 438.COLLECTIBLE_BINKY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BINKY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BINKY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 439.COLLECTIBLE_MOMS_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 440.COLLECTIBLE_KIDNEY_STONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KIDNEY_STONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KIDNEY_STONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 441.COLLECTIBLE_MEGA_BLAST
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_MEGA_BLAST" or "COLLECTIBLE_MEGA_SATANS_BREATH"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MEGA_BLAST,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 442.COLLECTIBLE_DARK_PRINCES_CROWN
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_DARK_PRINCES_CROWN" or "COLLECTIBLE_DARK_PRINCESS_CROWN"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DARK_PRINCES_CROWN,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 443.COLLECTIBLE_APPLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_APPLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_APPLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 444.COLLECTIBLE_LEAD_PENCIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LEAD_PENCIL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LEAD_PENCIL,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 445.COLLECTIBLE_DOG_TOOTH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DOG_TOOTH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DOG_TOOTH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 446.COLLECTIBLE_DEAD_TOOTH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEAD_TOOTH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEAD_TOOTH,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 447.COLLECTIBLE_LINGER_BEAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LINGER_BEAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LINGER_BEAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 448.COLLECTIBLE_SHARD_OF_GLASS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SHARD_OF_GLASS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SHARD_OF_GLASS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 449.COLLECTIBLE_METAL_PLATE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_METAL_PLATE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_METAL_PLATE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 450.COLLECTIBLE_EYE_OF_GREED
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EYE_OF_GREED,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EYE_OF_GREED,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 451.COLLECTIBLE_TAROT_CLOTH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TAROT_CLOTH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TAROT_CLOTH,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 452.COLLECTIBLE_VARICOSE_VEINS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VARICOSE_VEINS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VARICOSE_VEINS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 453.COLLECTIBLE_COMPOUND_FRACTURE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_COMPOUND_FRACTURE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_COMPOUND_FRACTURE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 454.COLLECTIBLE_POLYDACTYLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POLYDACTYLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POLYDACTYLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 455.COLLECTIBLE_DADS_LOST_COIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DADS_LOST_COIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DADS_LOST_COIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 456.COLLECTIBLE_MIDNIGHT_SNACK
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_MIDNIGHT_SNACK" or "COLLECTIBLE_MOLDY_BREAD"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MIDNIGHT_SNACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 457.COLLECTIBLE_CONE_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CONE_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CONE_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 458.COLLECTIBLE_BELLY_BUTTON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BELLY_BUTTON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BELLY_BUTTON,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 459.COLLECTIBLE_SINUS_INFECTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SINUS_INFECTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SINUS_INFECTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 460.COLLECTIBLE_GLAUCOMA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLAUCOMA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLAUCOMA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 461.COLLECTIBLE_PARASITOID
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PARASITOID,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PARASITOID,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 463.COLLECTIBLE_EYE_OF_BELIAL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EYE_OF_BELIAL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EYE_OF_BELIAL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 462.COLLECTIBLE_SULFURIC_ACID
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SULFURIC_ACID,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SULFURIC_ACID,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 464.COLLECTIBLE_GLYPH_OF_BALANCE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLYPH_OF_BALANCE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLYPH_OF_BALANCE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 465.COLLECTIBLE_ANALOG_STICK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANALOG_STICK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANALOG_STICK,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 466.COLLECTIBLE_CONTAGION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CONTAGION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CONTAGION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 467.COLLECTIBLE_FINGER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FINGER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FINGER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 468.COLLECTIBLE_SHADE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SHADE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SHADE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 469.COLLECTIBLE_DEPRESSION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEPRESSION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEPRESSION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 470.COLLECTIBLE_HUSHY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HUSHY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HUSHY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 471.COLLECTIBLE_LIL_MONSTRO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_MONSTRO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_MONSTRO,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 473.COLLECTIBLE_KING_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KING_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KING_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 472.COLLECTIBLE_BIG_CHUBBY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BIG_CHUBBY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BIG_CHUBBY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
if REPENTANCE then -- 474.COLLECTIBLE_BROKEN_GLASS_CANNON
Encyclopedia.AddItem({ -- 474.COLLECTIBLE_BROKEN_GLASS_CANNON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BROKEN_GLASS_CANNON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BROKEN_GLASS_CANNON,
})
else
Encyclopedia.AddItem({ -- 474.COLLECTIBLE_TONSIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TONSIL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TONSIL,
})
end
Encyclopedia.AddItem({ -- 475.COLLECTIBLE_PLAN_C
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PLAN_C,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PLAN_C,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 476.COLLECTIBLE_D1
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_D1,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D1,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 477.COLLECTIBLE_VOID
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VOID,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VOID,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 478.COLLECTIBLE_PAUSE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PAUSE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PAUSE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 479.COLLECTIBLE_SMELTER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SMELTER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SMELTER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 480.COLLECTIBLE_COMPOST
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_COMPOST,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_COMPOST,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 481.COLLECTIBLE_DATAMINER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DATAMINER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DATAMINER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 482.COLLECTIBLE_CLICKER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CLICKER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CLICKER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 483.COLLECTIBLE_MAMA_MEGA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAMA_MEGA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAMA_MEGA,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 484.COLLECTIBLE_WAIT_WHAT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WAIT_WHAT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WAIT_WHAT,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 485.COLLECTIBLE_CROOKED_PENNY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CROOKED_PENNY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CROOKED_PENNY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 486.COLLECTIBLE_DULL_RAZOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DULL_RAZOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DULL_RAZOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 487.COLLECTIBLE_POTATO_PEELER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POTATO_PEELER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POTATO_PEELER,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 488.COLLECTIBLE_METRONOME
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_METRONOME,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_METRONOME,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 489.COLLECTIBLE_D_INFINITY
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_D_INFINITY" or "COLLECTIBLE_DINF"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_D_INFINITY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 490.COLLECTIBLE_EDENS_SOUL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EDENS_SOUL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EDENS_SOUL,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 491.COLLECTIBLE_ACID_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ACID_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ACID_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 492.COLLECTIBLE_YO_LISTEN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_YO_LISTEN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_YO_LISTEN,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 493.COLLECTIBLE_ADRENALINE
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_ADRENALINE" or "COLLECTIBLE_ADDERLINE"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ADRENALINE,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 494.COLLECTIBLE_JACOBS_LADDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JACOBS_LADDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JACOBS_LADDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 495.COLLECTIBLE_GHOST_PEPPER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GHOST_PEPPER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GHOST_PEPPER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 496.COLLECTIBLE_EUTHANASIA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EUTHANASIA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EUTHANASIA,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 497.COLLECTIBLE_CAMO_UNDIES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CAMO_UNDIES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CAMO_UNDIES,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 498.COLLECTIBLE_DUALITY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DUALITY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DUALITY,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 499.COLLECTIBLE_EUCHARIST
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EUCHARIST,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EUCHARIST,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 500.COLLECTIBLE_SACK_OF_SACKS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACK_OF_SACKS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACK_OF_SACKS,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 501.COLLECTIBLE_GREEDS_GULLET
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GREEDS_GULLET,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GREEDS_GULLET,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 502.COLLECTIBLE_LARGE_ZIT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LARGE_ZIT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LARGE_ZIT,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 503.COLLECTIBLE_LITTLE_HORN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LITTLE_HORN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LITTLE_HORN,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 504.COLLECTIBLE_BROWN_NUGGET
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BROWN_NUGGET,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BROWN_NUGGET,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 505.COLLECTIBLE_POKE_GO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POKE_GO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POKE_GO,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 506.COLLECTIBLE_BACKSTABBER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BACKSTABBER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BACKSTABBER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 507.COLLECTIBLE_SHARP_STRAW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SHARP_STRAW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SHARP_STRAW,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 508.COLLECTIBLE_MOMS_RAZOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_RAZOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_RAZOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 509.COLLECTIBLE_BLOODSHOT_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOODSHOT_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOODSHOT_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 510.COLLECTIBLE_DELIRIOUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DELIRIOUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DELIRIOUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 511.COLLECTIBLE_ANGRY_FLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANGRY_FLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANGRY_FLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 512.COLLECTIBLE_BLACK_HOLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLACK_HOLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLACK_HOLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 513.COLLECTIBLE_BOZO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOZO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOZO,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 514.COLLECTIBLE_BROKEN_MODEM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BROKEN_MODEM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BROKEN_MODEM,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 515.COLLECTIBLE_MYSTERY_GIFT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MYSTERY_GIFT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MYSTERY_GIFT,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 516.COLLECTIBLE_SPRINKLER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPRINKLER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPRINKLER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 518.COLLECTIBLE_FAST_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FAST_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FAST_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 517.COLLECTIBLE_BUDDY_IN_A_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BUDDY_IN_A_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BUDDY_IN_A_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 519.COLLECTIBLE_LIL_DELIRIUM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_DELIRIUM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_DELIRIUM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 520.COLLECTIBLE_JUMPER_CABLES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JUMPER_CABLES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JUMPER_CABLES,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 521.COLLECTIBLE_COUPON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_COUPON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_COUPON,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 524.COLLECTIBLE_MOVING_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOVING_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOVING_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 522.COLLECTIBLE_TECHNOLOGY_ZERO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TECHNOLOGY_ZERO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TECHNOLOGY_ZERO,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 523.COLLECTIBLE_TELEKINESIS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TELEKINESIS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TELEKINESIS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 525.COLLECTIBLE_LEPROSY
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_LEPROSY" or "COLLECTIBLE_LEPROCY"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LEPROSY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 526.COLLECTIBLE_7_SEALS
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_7_SEALS" or "COLLECTIBLE_LIL_HARBINGERS"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_7_SEALS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 527.COLLECTIBLE_MR_ME
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MR_ME,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MR_ME,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 528.COLLECTIBLE_ANGELIC_PRISM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANGELIC_PRISM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANGELIC_PRISM,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 529.COLLECTIBLE_POP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POP,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 530.COLLECTIBLE_DEATHS_LIST
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_DEATHS_LIST" or "COLLECTIBLE_DEATH_LIST"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEATHS_LIST,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 531.COLLECTIBLE_HAEMOLACRIA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HAEMOLACRIA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HAEMOLACRIA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 532.COLLECTIBLE_LACHRYPHAGY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LACHRYPHAGY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LACHRYPHAGY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 533.COLLECTIBLE_TRISAGION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TRISAGION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TRISAGION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 534.COLLECTIBLE_SCHOOLBAG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCHOOLBAG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCHOOLBAG,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 535.COLLECTIBLE_BLANKET
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLANKET,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLANKET,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 536.COLLECTIBLE_SACRIFICIAL_ALTAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACRIFICIAL_ALTAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACRIFICIAL_ALTAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 537.COLLECTIBLE_LIL_SPEWER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_SPEWER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_SPEWER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 538.COLLECTIBLE_MARBLES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MARBLES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MARBLES,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 539.COLLECTIBLE_MYSTERY_EGG
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MYSTERY_EGG,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MYSTERY_EGG,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 540.COLLECTIBLE_FLAT_STONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FLAT_STONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FLAT_STONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 541.COLLECTIBLE_MARROW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MARROW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MARROW,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 542.COLLECTIBLE_SLIPPED_RIB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SLIPPED_RIB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SLIPPED_RIB,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 543.COLLECTIBLE_HALLOWED_GROUND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HALLOWED_GROUND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HALLOWED_GROUND,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 544.COLLECTIBLE_POINTY_RIB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POINTY_RIB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POINTY_RIB,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 545.COLLECTIBLE_BOOK_OF_THE_DEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOK_OF_THE_DEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_THE_DEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 546.COLLECTIBLE_DADS_RING
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DADS_RING,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DADS_RING,
    Pools = {
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 547.COLLECTIBLE_DIVORCE_PAPERS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DIVORCE_PAPERS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DIVORCE_PAPERS,
    Pools = {
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_BOSS,
    },
})
Encyclopedia.AddItem({ -- 548.COLLECTIBLE_JAW_BONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JAW_BONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JAW_BONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 549.COLLECTIBLE_BRITTLE_BONES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BRITTLE_BONES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BRITTLE_BONES,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 552.COLLECTIBLE_BROKEN_SHOVEL_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BROKEN_SHOVEL_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BROKEN_SHOVEL_2,
})
Encyclopedia.AddItem({ -- 550.COLLECTIBLE_MOMS_SHOVEL
    Class = "vanilla",
	ID = CollectibleType[REPENTANCE and "COLLECTIBLE_MOMS_SHOVEL" or "COLLECTIBLE_BROKEN_SHOVEL"],
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_SHOVEL,
})

if REPENTANCE then
Encyclopedia.AddItem({ -- 553.COLLECTIBLE_MUCORMYCOSIS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MUCORMYCOSIS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MUCORMYCOSIS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 554.COLLECTIBLE_2SPOOKY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_2SPOOKY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_2SPOOKY,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 555.COLLECTIBLE_GOLDEN_RAZOR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GOLDEN_RAZOR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GOLDEN_RAZOR,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 557.COLLECTIBLE_SULFUR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SULFUR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SULFUR,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 556.COLLECTIBLE_FORTUNE_COOKIE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FORTUNE_COOKIE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FORTUNE_COOKIE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 558.COLLECTIBLE_EYE_SORE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EYE_SORE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EYE_SORE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 559.COLLECTIBLE_120_VOLT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_120_VOLT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_120_VOLT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 560.COLLECTIBLE_IT_HURTS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IT_HURTS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IT_HURTS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 562.COLLECTIBLE_ALMOND_MILK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ALMOND_MILK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ALMOND_MILK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 561.COLLECTIBLE_ROCK_BOTTOM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROCK_BOTTOM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROCK_BOTTOM,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 563.COLLECTIBLE_NANCY_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NANCY_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NANCY_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 564.COLLECTIBLE_BAR_OF_SOAP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BAR_OF_SOAP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BAR_OF_SOAP,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 565.COLLECTIBLE_BLOOD_PUPPY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOOD_PUPPY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_PUPPY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 566.COLLECTIBLE_DREAM_CATCHER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DREAM_CATCHER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DREAM_CATCHER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 567.COLLECTIBLE_PASCHAL_CANDLE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PASCHAL_CANDLE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PASCHAL_CANDLE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 568.COLLECTIBLE_DIVINE_INTERVENTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DIVINE_INTERVENTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DIVINE_INTERVENTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 569.COLLECTIBLE_BLOOD_OATH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOOD_OATH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_OATH,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 570.COLLECTIBLE_PLAYDOUGH_COOKIE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PLAYDOUGH_COOKIE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PLAYDOUGH_COOKIE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 571.COLLECTIBLE_SOCKS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SOCKS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SOCKS,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 572.COLLECTIBLE_EYE_OF_THE_OCCULT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EYE_OF_THE_OCCULT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EYE_OF_THE_OCCULT,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 573.COLLECTIBLE_IMMACULATE_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IMMACULATE_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IMMACULATE_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 574.COLLECTIBLE_MONSTRANCE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MONSTRANCE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONSTRANCE,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 575.COLLECTIBLE_INTRUDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INTRUDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INTRUDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 576.COLLECTIBLE_DIRTY_MIND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DIRTY_MIND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DIRTY_MIND,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 577.COLLECTIBLE_DAMOCLES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DAMOCLES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DAMOCLES,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 578.COLLECTIBLE_FREE_LEMONADE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FREE_LEMONADE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FREE_LEMONADE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 579.COLLECTIBLE_SPIRIT_SWORD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIRIT_SWORD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIRIT_SWORD,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 580.COLLECTIBLE_RED_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RED_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RED_KEY,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
    },
})
Encyclopedia.AddItem({ -- 581.COLLECTIBLE_PSY_FLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PSY_FLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PSY_FLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 582.COLLECTIBLE_WAVY_CAP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WAVY_CAP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WAVY_CAP,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 583.COLLECTIBLE_ROCKET_IN_A_JAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROCKET_IN_A_JAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROCKET_IN_A_JAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 584.COLLECTIBLE_BOOK_OF_VIRTUES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOK_OF_VIRTUES,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 585.COLLECTIBLE_ALABASTER_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ALABASTER_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ALABASTER_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 586.COLLECTIBLE_STAIRWAY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STAIRWAY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STAIRWAY,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 588.COLLECTIBLE_SOL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SOL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SOL,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 589.COLLECTIBLE_LUNA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LUNA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LUNA,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 590.COLLECTIBLE_MERCURIUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MERCURIUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MERCURIUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 591.COLLECTIBLE_VENUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VENUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VENUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 592.COLLECTIBLE_TERRA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TERRA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TERRA,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 593.COLLECTIBLE_MARS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MARS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MARS,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 594.COLLECTIBLE_JUPITER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JUPITER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JUPITER,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 595.COLLECTIBLE_SATURNUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SATURNUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SATURNUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 596.COLLECTIBLE_URANUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_URANUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_URANUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 597.COLLECTIBLE_NEPTUNUS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_NEPTUNUS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_NEPTUNUS,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 598.COLLECTIBLE_PLUTO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PLUTO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PLUTO,
    Pools = {
        Encyclopedia.ItemPools.POOL_PLANETARIUM,
    },
})
Encyclopedia.AddItem({ -- 599.COLLECTIBLE_VOODOO_HEAD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VOODOO_HEAD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VOODOO_HEAD,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 600.COLLECTIBLE_EYE_DROPS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EYE_DROPS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EYE_DROPS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 601.COLLECTIBLE_ACT_OF_CONTRITION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ACT_OF_CONTRITION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ACT_OF_CONTRITION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 602.COLLECTIBLE_MEMBER_CARD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MEMBER_CARD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MEMBER_CARD,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 603.COLLECTIBLE_BATTERY_PACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BATTERY_PACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BATTERY_PACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 604.COLLECTIBLE_MOMS_BRACELET
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_BRACELET,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_BRACELET,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_OLD_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 605.COLLECTIBLE_SCOOPER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SCOOPER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SCOOPER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 606.COLLECTIBLE_OCULAR_RIFT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_OCULAR_RIFT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_OCULAR_RIFT,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 607.COLLECTIBLE_BOILED_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOILED_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOILED_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 608.COLLECTIBLE_FREEZER_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FREEZER_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FREEZER_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 609.COLLECTIBLE_ETERNAL_D6
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ETERNAL_D6,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ETERNAL_D6,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 611.COLLECTIBLE_BIRD_CAGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BIRD_CAGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BIRD_CAGE,
    Pools = {
		Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 610.COLLECTIBLE_LARYNX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LARYNX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LARYNX,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 612.COLLECTIBLE_LOST_SOUL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LOST_SOUL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LOST_SOUL,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 614.COLLECTIBLE_BLOOD_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOOD_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOOD_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 615.COLLECTIBLE_LIL_DUMPY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_DUMPY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_DUMPY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 616.COLLECTIBLE_BIRDS_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BIRDS_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BIRDS_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 617.COLLECTIBLE_LODESTONE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LODESTONE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LODESTONE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 618.COLLECTIBLE_ROTTEN_TOMATO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ROTTEN_TOMATO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ROTTEN_TOMATO,
    Pools = {
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 619.COLLECTIBLE_BIRTHRIGHT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BIRTHRIGHT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BIRTHRIGHT,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 621.COLLECTIBLE_RED_STEW
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RED_STEW,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RED_STEW,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 622.COLLECTIBLE_GENESIS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GENESIS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GENESIS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 624.COLLECTIBLE_SHARP_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SHARP_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SHARP_KEY,
    Pools = {
		Encyclopedia.ItemPools.POOL_SHOP,
		Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 623.COLLECTIBLE_BOOSTER_PACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOOSTER_PACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOOSTER_PACK,
    Pools = {
		Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 625.COLLECTIBLE_MEGA_MUSH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MEGA_MUSH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MEGA_MUSH,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 626.COLLECTIBLE_KNIFE_PIECE_1
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KNIFE_PIECE_1,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KNIFE_PIECE_1,
})
Encyclopedia.AddItem({ -- 627.COLLECTIBLE_KNIFE_PIECE_2
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KNIFE_PIECE_2,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KNIFE_PIECE_2,
})
Encyclopedia.AddItem({ -- 628.COLLECTIBLE_DEATH_CERTIFICATE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DEATH_CERTIFICATE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DEATH_CERTIFICATE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 629.COLLECTIBLE_BOT_FLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BOT_FLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BOT_FLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 631.COLLECTIBLE_MEAT_CLEAVER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MEAT_CLEAVER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MEAT_CLEAVER,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 632.COLLECTIBLE_EVIL_CHARM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EVIL_CHARM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EVIL_CHARM,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 633.COLLECTIBLE_DOGMA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DOGMA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DOGMA,
})
Encyclopedia.AddItem({ -- 634.COLLECTIBLE_PURGATORY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PURGATORY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PURGATORY,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 635.COLLECTIBLE_STITCHES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STITCHES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STITCHES,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 636.COLLECTIBLE_R_KEY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_R_KEY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_R_KEY,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 637.COLLECTIBLE_KNOCKOUT_DROPS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KNOCKOUT_DROPS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KNOCKOUT_DROPS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 638.COLLECTIBLE_ERASER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ERASER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ERASER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 639.COLLECTIBLE_YUCK_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_YUCK_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_YUCK_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_ROTTEN_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 640.COLLECTIBLE_URN_OF_SOULS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_URN_OF_SOULS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_URN_OF_SOULS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 641.COLLECTIBLE_AKELDAMA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_AKELDAMA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_AKELDAMA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 642.COLLECTIBLE_MAGIC_SKIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MAGIC_SKIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MAGIC_SKIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
    },
})
Encyclopedia.AddItem({ -- 643.COLLECTIBLE_REVELATION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_REVELATION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_REVELATION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 644.COLLECTIBLE_CONSOLATION_PRIZE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CONSOLATION_PRIZE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CONSOLATION_PRIZE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 645.COLLECTIBLE_TINYTOMA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TINYTOMA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TINYTOMA,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 646.COLLECTIBLE_BRIMSTONE_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BRIMSTONE_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BRIMSTONE_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 647.COLLECTIBLE_4_5_VOLT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_4_5_VOLT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_4_5_VOLT,
    Pools = {
        Encyclopedia.ItemPools.POOL_BATTERY_BUM,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 649.COLLECTIBLE_FRUITY_PLUM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FRUITY_PLUM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FRUITY_PLUM,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 650.COLLECTIBLE_PLUM_FLUTE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_PLUM_FLUTE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_PLUM_FLUTE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 651.COLLECTIBLE_STAR_OF_BETHLEHEM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STAR_OF_BETHLEHEM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STAR_OF_BETHLEHEM,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 652.COLLECTIBLE_CUBE_BABY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CUBE_BABY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CUBE_BABY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 653.COLLECTIBLE_VADE_RETRO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VADE_RETRO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VADE_RETRO,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 654.COLLECTIBLE_FALSE_PHD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FALSE_PHD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FALSE_PHD,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 655.COLLECTIBLE_SPIN_TO_WIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIN_TO_WIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIN_TO_WIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 657.COLLECTIBLE_VASCULITIS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VASCULITIS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VASCULITIS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 658.COLLECTIBLE_GIANT_CELL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GIANT_CELL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GIANT_CELL,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 659.COLLECTIBLE_TROPICAMIDE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TROPICAMIDE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TROPICAMIDE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 660.COLLECTIBLE_CARD_READING
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CARD_READING,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CARD_READING,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 661.COLLECTIBLE_QUINTS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_QUINTS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_QUINTS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 663.COLLECTIBLE_TOOTH_AND_NAIL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TOOTH_AND_NAIL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TOOTH_AND_NAIL,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 664.COLLECTIBLE_BINGE_EATER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BINGE_EATER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BINGE_EATER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 665.COLLECTIBLE_GUPPYS_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GUPPYS_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GUPPYS_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_RED_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 667.COLLECTIBLE_STRAW_MAN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STRAW_MAN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STRAW_MAN,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 668.COLLECTIBLE_DADS_NOTE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DADS_NOTE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DADS_NOTE,
})
Encyclopedia.AddItem({ -- 670.COLLECTIBLE_SAUSAGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SAUSAGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SAUSAGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
    },
})
Encyclopedia.AddItem({ -- 669.COLLECTIBLE_OPTIONS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_OPTIONS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_OPTIONS,
    Pools = {
        Encyclopedia.ItemPools.POOL_SHOP,
    },
})
Encyclopedia.AddItem({ -- 671.COLLECTIBLE_CANDY_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CANDY_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CANDY_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 672.COLLECTIBLE_POUND_OF_FLESH
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_POUND_OF_FLESH,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_POUND_OF_FLESH,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 673.COLLECTIBLE_REDEMPTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_REDEMPTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_REDEMPTION,
})
Encyclopedia.AddItem({ -- 674.COLLECTIBLE_SPIRIT_SHACKLES
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPIRIT_SHACKLES,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPIRIT_SHACKLES,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 675.COLLECTIBLE_CRACKED_ORB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_CRACKED_ORB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_CRACKED_ORB,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 676.COLLECTIBLE_EMPTY_HEART
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EMPTY_HEART,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EMPTY_HEART,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEMON_BEGGAR,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 677.COLLECTIBLE_ASTRAL_PROJECTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ASTRAL_PROJECTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ASTRAL_PROJECTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 678.COLLECTIBLE_C_SECTION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_C_SECTION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_C_SECTION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 679.COLLECTIBLE_LIL_ABADDON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_ABADDON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_ABADDON,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 680.COLLECTIBLE_MONTEZUMAS_REVENGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MONTEZUMAS_REVENGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MONTEZUMAS_REVENGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 681.COLLECTIBLE_LIL_PORTAL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LIL_PORTAL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LIL_PORTAL,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 682.COLLECTIBLE_WORM_FRIEND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_WORM_FRIEND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_WORM_FRIEND,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 683.COLLECTIBLE_BONE_SPURS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BONE_SPURS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BONE_SPURS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 684.COLLECTIBLE_HUNGRY_SOUL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HUNGRY_SOUL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HUNGRY_SOUL,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 685.COLLECTIBLE_JAR_OF_WISPS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JAR_OF_WISPS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JAR_OF_WISPS,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 686.COLLECTIBLE_SOUL_LOCKET
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SOUL_LOCKET,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SOUL_LOCKET,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 687.COLLECTIBLE_FRIEND_FINDER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FRIEND_FINDER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FRIEND_FINDER,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 688.COLLECTIBLE_INNER_CHILD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_INNER_CHILD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_INNER_CHILD,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 689.COLLECTIBLE_GLITCHED_CROWN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLITCHED_CROWN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLITCHED_CROWN,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 690.COLLECTIBLE_JELLY_BELLY
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_JELLY_BELLY,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_JELLY_BELLY,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 691.COLLECTIBLE_SACRED_ORB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SACRED_ORB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SACRED_ORB,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 692.COLLECTIBLE_SANGUINE_BOND
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SANGUINE_BOND,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SANGUINE_BOND,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 693.COLLECTIBLE_SWARM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SWARM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SWARM,
    Pools = {
        Encyclopedia.ItemPools.POOL_KEY_MASTER,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 694.COLLECTIBLE_HEARTBREAK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HEARTBREAK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HEARTBREAK,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 695.COLLECTIBLE_BLOODY_GUST
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BLOODY_GUST,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BLOODY_GUST,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 696.COLLECTIBLE_SALVATION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SALVATION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SALVATION,
    Pools = {
        Encyclopedia.ItemPools.POOL_ANGEL,
        Encyclopedia.ItemPools.POOL_GREED_ANGEL,
    },
})
Encyclopedia.AddItem({ -- 697.COLLECTIBLE_VANISHING_TWIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VANISHING_TWIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VANISHING_TWIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_SECRET,
    },
})
Encyclopedia.AddItem({ -- 698.COLLECTIBLE_TWISTED_PAIR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TWISTED_PAIR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TWISTED_PAIR,
    Pools = {
        Encyclopedia.ItemPools.POOL_BABY_SHOP,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 699.COLLECTIBLE_AZAZELS_RAGE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_AZAZELS_RAGE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_AZAZELS_RAGE,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 700.COLLECTIBLE_ECHO_CHAMBER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ECHO_CHAMBER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ECHO_CHAMBER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 701.COLLECTIBLE_ISAACS_TOMB
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ISAACS_TOMB,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ISAACS_TOMB,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 702.COLLECTIBLE_VENGEFUL_SPIRIT
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_VENGEFUL_SPIRIT,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_VENGEFUL_SPIRIT,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_CURSE,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 703.COLLECTIBLE_ESAU_JR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ESAU_JR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ESAU_JR,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 704.COLLECTIBLE_BERSERK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BERSERK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BERSERK,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 705.COLLECTIBLE_DARK_ARTS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DARK_ARTS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DARK_ARTS,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 706.COLLECTIBLE_ABYSS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ABYSS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ABYSS,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 707.COLLECTIBLE_SUPPER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SUPPER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SUPPER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BEGGAR,
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 708.COLLECTIBLE_STAPLER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STAPLER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STAPLER,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 709.COLLECTIBLE_SUPLEX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SUPLEX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SUPLEX,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 710.COLLECTIBLE_BAG_OF_CRAFTING
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_BAG_OF_CRAFTING,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_BAG_OF_CRAFTING,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 711.COLLECTIBLE_FLIP
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_FLIP,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_FLIP,
    Pools = {
        Encyclopedia.ItemPools.POOL_CURSE,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
Encyclopedia.AddItem({ -- 712.COLLECTIBLE_LEMEGETON
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_LEMEGETON,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_LEMEGETON,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_LIBRARY,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
    },
})
Encyclopedia.AddItem({ -- 713.COLLECTIBLE_SUMPTORIUM
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SUMPTORIUM,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SUMPTORIUM,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 714.COLLECTIBLE_RECALL
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_RECALL,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_RECALL,
})
Encyclopedia.AddItem({ -- 716.COLLECTIBLE_HOLD
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HOLD,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HOLD,
})
Encyclopedia.AddItem({ -- 715.COLLECTIBLE_KEEPERS_SACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KEEPERS_SACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KEEPERS_SACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 717.COLLECTIBLE_KEEPERS_KIN
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KEEPERS_KIN,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KEEPERS_KIN,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 719.COLLECTIBLE_KEEPERS_BOX
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_KEEPERS_BOX,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_KEEPERS_BOX,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_SHOP,
        Encyclopedia.ItemPools.POOL_WOODEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 720.COLLECTIBLE_EVERYTHING_JAR
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_EVERYTHING_JAR,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_EVERYTHING_JAR,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 721.COLLECTIBLE_TMTRAINER
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_TMTRAINER,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_TMTRAINER,
    Pools = {
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 722.COLLECTIBLE_ANIMA_SOLA
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_ANIMA_SOLA,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_ANIMA_SOLA,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 723.COLLECTIBLE_SPINDOWN_DICE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_SPINDOWN_DICE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_SPINDOWN_DICE,
    Pools = {
        Encyclopedia.ItemPools.POOL_CRANE_GAME,
        Encyclopedia.ItemPools.POOL_SECRET,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 724.COLLECTIBLE_HYPERCOAGULATION
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HYPERCOAGULATION,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HYPERCOAGULATION,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 725.COLLECTIBLE_IBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_IBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_IBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 726.COLLECTIBLE_HEMOPTYSIS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_HEMOPTYSIS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_HEMOPTYSIS,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 727.COLLECTIBLE_GHOST_BOMBS
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GHOST_BOMBS,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GHOST_BOMBS,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOMB_BUM,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 728.COLLECTIBLE_GELLO
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GELLO,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GELLO,
    Pools = {
        Encyclopedia.ItemPools.POOL_DEVIL,
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_ULTRA_SECRET,
        Encyclopedia.ItemPools.POOL_GREED_DEVIL,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 729.COLLECTIBLE_DECAP_ATTACK
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_DECAP_ATTACK,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_DECAP_ATTACK,
    Pools = {
        Encyclopedia.ItemPools.POOL_TREASURE,
        Encyclopedia.ItemPools.POOL_GREED_TREASURE,
    },
})
Encyclopedia.AddItem({ -- 730.COLLECTIBLE_GLASS_EYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_GLASS_EYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_GLASS_EYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 731.COLLECTIBLE_STYE
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_STYE,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_STYE,
    Pools = {
        Encyclopedia.ItemPools.POOL_BOSS,
        Encyclopedia.ItemPools.POOL_GREED_BOSS,
    },
})
Encyclopedia.AddItem({ -- 732.COLLECTIBLE_MOMS_RING
    Class = "vanilla",
    ID = CollectibleType.COLLECTIBLE_MOMS_RING,
    WikiDesc = Encyclopedia.ItemsWiki.COLLECTIBLE_MOMS_RING,
    Pools = {
        Encyclopedia.ItemPools.POOL_GOLDEN_CHEST,
        Encyclopedia.ItemPools.POOL_MOMS_CHEST,
        Encyclopedia.ItemPools.POOL_GREED_SHOP,
    },
})
end
