Encyclopedia.AddCharacter({ -- 0.PLAYER_ISAAC
    Class = "vanilla",
	Name = "Isaac",
	--Description = "Gambler",
    ID = PlayerType.PLAYER_ISAAC,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "01_Isaac", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_ISAAC,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_ISAAC] = true

Encyclopedia.AddCharacter({ -- 1.PLAYER_MAGDALENA
    Class = "vanilla",
	Name = "Magdalene",
	--Description = "Heart Lady",
    ID = PlayerType.PLAYER_MAGDALENA,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "02_Magdalene", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_MAGDALENA,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_MAGDALENA] = true

Encyclopedia.AddCharacter({ -- 2.PLAYER_CAIN
    Class = "vanilla",
	Name = "Cain",
	--Description = "Best lad",
    ID = PlayerType.PLAYER_CAIN,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "03_Cain", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_CAIN,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_CAIN] = true

Encyclopedia.AddCharacter({ -- 3.PLAYER_JUDAS
    Class = "vanilla",
	Name = "Judas",
	--Description = "Stronk as shit",
    ID = PlayerType.PLAYER_JUDAS,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "04_Judas", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_JUDAS,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_JUDAS] = true

Encyclopedia.AddCharacter({ -- 3.1.PLAYER_BLACKJUDAS
    Class = "vanilla",
	Name = "Dark Judas",
	--Description = "Stronk as shit",
    ID = PlayerType.PLAYER_BLACKJUDAS,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/encyclopedia.characterportraits.anm2", "Dark Judas", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_BLACKJUDAS,
	CompletionTrackerFuncs = {
		function()
			local Notes = Encyclopedia.CompletionNotes_a.Judas
			return Notes.Unlocks
		end,
	}
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_BLACKJUDAS] = true

Encyclopedia.AddCharacter({ -- 4.PLAYER_EVE
    Class = "vanilla",
	Name = "Eve",
	--Description = "Goth chick",
    ID = PlayerType.PLAYER_EVE,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "05_Eve", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_EVE,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_EVE] = true

Encyclopedia.AddCharacter({ -- 5.PLAYER_XXX
    Class = "vanilla",
	Name = "???",
	--Description = "Shitass, poopybutthole even",
    ID = PlayerType.PLAYER_XXX,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "06_Bluebaby", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_XXX,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_XXX] = true

Encyclopedia.AddCharacter({ -- 6.PLAYER_SAMSON
    Class = "vanilla",
	Name = "Samson",
	--Description = "Self damage go brr",
    ID = PlayerType.PLAYER_SAMSON,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "07_Samson", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_SAMSON,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_SAMSON] = true

Encyclopedia.AddCharacter({ -- 7.PLAYER_AZAZEL
    Class = "vanilla",
	Name = "Azazel",
	--Description = "Ez Pz lemon Squeezy",
    ID = PlayerType.PLAYER_AZAZEL,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "08_Azazel", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_AZAZEL,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_AZAZEL] = true

Encyclopedia.AddCharacter({ -- 8.PLAYER_LAZARUS
    Class = "vanilla",
	Name = "Lazarus",
	--Description = "Fuck you in particular",
    ID = PlayerType.PLAYER_LAZARUS,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "09_Lazarus", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_LAZARUS,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_LAZARUS] = true

Encyclopedia.AddCharacter({ -- 8.1.PLAYER_LAZARUS2
    Class = "vanilla",
	Name = "Lazarus Risen",
	--Description = "Fuck you in particular",
    ID = PlayerType.PLAYER_LAZARUS2,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/encyclopedia.characterportraits.anm2", "Lazarus II", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_LAZARUS2,
	CompletionTrackerFuncs = {
		function()
			local Notes = Encyclopedia.CompletionNotes_a.Lazarus
			return Notes.Unlocks
		end,
	}
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_LAZARUS2] = true

Encyclopedia.AddCharacter({ -- 9.PLAYER_EDEN
    Class = "vanilla",
	Name = "Eden",
	--Description = "'B'",
    ID = PlayerType.PLAYER_EDEN,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "10_Eden", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_EDEN,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_EDEN] = true

Encyclopedia.AddCharacter({ -- 10.PLAYER_THELOST
    Class = "vanilla",
	Name = "The Lost",
	--Description = "Skill issue",
    ID = PlayerType.PLAYER_THELOST,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "11_TheLost", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_THELOST,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_THELOST] = true

Encyclopedia.AddCharacter({ -- 11.PLAYER_LILITH
    Class = "vanilla",
	Name = "Lilith",
	--Description = "Hot demon girl I think",
    ID = PlayerType.PLAYER_LILITH,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "12_Lilith", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_LILITH,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_LILITH] = true

Encyclopedia.AddCharacter({ -- 12.PLAYER_KEEPER
    Class = "vanilla",
	Name = "Keeper",
	--Description = "I like money",
    ID = PlayerType.PLAYER_KEEPER,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "13_Keeper", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_KEEPER,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_KEEPER] = true

Encyclopedia.AddCharacter({ -- 13.PLAYER_APOLLYON
    Class = "vanilla",
	Name = "Apollyon",
	--Description = "Zzzzz...",
    ID = PlayerType.PLAYER_APOLLYON,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "15_Apollyon", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_APOLLYON,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_APOLLYON] = true

Encyclopedia.AddCharacter({ -- 14.PLAYER_THEFORGOTTEN
    Class = "vanilla",
	Name = "The Forgotten",
	--Description = "Boner, haha I said it",
    ID = PlayerType.PLAYER_THEFORGOTTEN,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "16_TheForgotten", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_THEFORGOTTEN,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_THEFORGOTTEN] = true

if REPENTANCE then

Encyclopedia.AddCharacter({ -- 15.PLAYER_BETHANY
    Class = "vanilla",
	Name = "Bethany",
	--Description = "Tainted Ohyo",
    ID = PlayerType.PLAYER_BETHANY,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "17_Bethany", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_BETHANY,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_BETHANY] = true

Encyclopedia.AddCharacter({ -- 16.PLAYER_JACOB
    Class = "vanilla",
	Name = "Jacob and Esau",
	--Description = "Eternal suffering",
    ID = PlayerType.PLAYER_JACOB,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraits.anm2", "18_JacobEsau", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_JACOB,
})
Encyclopedia.TrackerTables.characters_aNames[PlayerType.PLAYER_JACOB] = true


Encyclopedia.AddCharacterTainted({ -- 0.PLAYER_ISAAC_B
    Class = "vanilla",
	Name = "Isaac",
	Description = "The Broken",
    ID = PlayerType.PLAYER_ISAAC_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "01_Isaac", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_ISAAC_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_ISAAC_B] = true

Encyclopedia.AddCharacterTainted({ -- 1.PLAYER_MAGDALENA_B
    Class = "vanilla",
	Name = "Magdalene",
	Description = "The Dauntless",
    ID = PlayerType.PLAYER_MAGDALENA_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "02_Magdalene", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_MAGDALENA_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_MAGDALENA_B] = true

Encyclopedia.AddCharacterTainted({ -- 2.PLAYER_CAIN_B
    Class = "vanilla",
	Name = "Cain",
	Description = "The Hoarder",
    ID = PlayerType.PLAYER_CAIN_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "03_Cain", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_CAIN_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_CAIN_B] = true

Encyclopedia.AddCharacterTainted({ -- 3.PLAYER_JUDAS_B
    Class = "vanilla",
	Name = "Judas",
	Description = "The Deceiver",
    ID = PlayerType.PLAYER_JUDAS_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "04_Judas", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_JUDAS_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_JUDAS_B] = true

Encyclopedia.AddCharacterTainted({ -- 4.PLAYER_EVE_B
    Class = "vanilla",
	Name = "Eve",
	Description = "The Curdled",
    ID = PlayerType.PLAYER_EVE_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "05_Eve", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_EVE_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_EVE_B] = true

Encyclopedia.AddCharacterTainted({ -- 5.PLAYER_XXX_B
    Class = "vanilla",
	Name = "???",
	Description = "The Soiled",
    ID = PlayerType.PLAYER_XXX_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "06_Bluebaby", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_XXX_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_XXX_B] = true

Encyclopedia.AddCharacterTainted({ -- 6.PLAYER_SAMSON_B
    Class = "vanilla",
	Name = "Samson",
	Description = "The Savage",
    ID = PlayerType.PLAYER_SAMSON_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "07_Samson", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_SAMSON_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_SAMSON_B] = true

Encyclopedia.AddCharacterTainted({ -- 7.PLAYER_AZAZEL_B
    Class = "vanilla",
	Name = "Azazel",
	Description = "The Benighted",
    ID = PlayerType.PLAYER_AZAZEL_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "08_Azazel", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_AZAZEL_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_AZAZEL_B] = true

Encyclopedia.AddCharacterTainted({ -- 8.PLAYER_LAZARUS_B
    Class = "vanilla",
	Name = "Lazarus",
	Description = "The Enigma",
    ID = PlayerType.PLAYER_LAZARUS_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "09_Lazarus", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_LAZARUS_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_LAZARUS_B] = true

Encyclopedia.AddCharacterTainted({ -- 9.PLAYER_EDEN_B
    Class = "vanilla",
	Name = "Eden",
	Description = "The Capricious",
    ID = PlayerType.PLAYER_EDEN_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "10_Eden", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_EDEN_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_EDEN_B] = true

Encyclopedia.AddCharacterTainted({ -- 10.PLAYER_THELOST_B
    Class = "vanilla",
	Name = "The Lost",
	Description = "The Baleful",
    ID = PlayerType.PLAYER_THELOST_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "11_TheLost", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_THELOST_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_THELOST_B] = true

Encyclopedia.AddCharacterTainted({ -- 11.PLAYER_LILITH_B
    Class = "vanilla",
	Name = "Lilith",
	Description = "The Harlot",
    ID = PlayerType.PLAYER_LILITH_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "12_Lilith", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_LILITH_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_LILITH_B] = true

Encyclopedia.AddCharacterTainted({ -- 12.PLAYER_KEEPER_B
    Class = "vanilla",
	Name = "Keeper",
	Description = "The Miser",
    ID = PlayerType.PLAYER_KEEPER_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "13_Keeper", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_KEEPER_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_KEEPER_B] = true

Encyclopedia.AddCharacterTainted({ -- 13.PLAYER_APOLLYON_B
    Class = "vanilla",
	Name = "Apollyon",
	Description = "The Empty",
    ID = PlayerType.PLAYER_APOLLYON_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "15_Apollyon", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_APOLLYON_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_APOLLYON_B] = true

Encyclopedia.AddCharacterTainted({ -- 14.PLAYER_THEFORGOTTEN_B
    Class = "vanilla",
	Name = "The Forgotten",
	Description = "The Fettered",
    ID = PlayerType.PLAYER_THEFORGOTTEN_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "16_TheForgotten", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_THEFORGOTTEN_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_THEFORGOTTEN_B] = true

Encyclopedia.AddCharacterTainted({ -- 15.PLAYER_BETHANY_B
    Class = "vanilla",
	Name = "Bethany",
	Description = "The Zealot",
    ID = PlayerType.PLAYER_BETHANY_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "17_Bethany", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_BETHANY_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_BETHANY_B] = true

Encyclopedia.AddCharacterTainted({ -- 16.PLAYER_JACOB_B
    Class = "vanilla",
	Name = "Jacob",
	Description = "The Deserter",
    ID = PlayerType.PLAYER_JACOB_B,
	Sprite = Encyclopedia.RegisterSprite("gfx/ui/main menu/characterportraitsalt.anm2", "18_JacobEsau", 0),
    WikiDesc = Encyclopedia.CharactersWiki.PLAYER_JACOB_B,
})
Encyclopedia.TrackerTables.characters_bNames[PlayerType.PLAYER_JACOB_B] = true


end
