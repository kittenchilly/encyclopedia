Encyclopedia.AddTrinket({ -- 1.TRINKET_SWALLOWED_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_SWALLOWED_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SWALLOWED_PENNY,
})
Encyclopedia.AddTrinket({ -- 2.TRINKET_PETRIFIED_POOP
    Class = "vanilla",
    ID = TrinketType.TRINKET_PETRIFIED_POOP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PETRIFIED_POOP,
})
Encyclopedia.AddTrinket({ -- 3.TRINKET_AAA_BATTERY
    Class = "vanilla",
    ID = TrinketType.TRINKET_AAA_BATTERY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_AAA_BATTERY,
})
Encyclopedia.AddTrinket({ -- 6.TRINKET_PURPLE_HEART
    Class = "vanilla",
    ID = TrinketType.TRINKET_PURPLE_HEART,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PURPLE_HEART,
})
Encyclopedia.AddTrinket({ -- 4.TRINKET_BROKEN_MAGNET
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROKEN_MAGNET,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROKEN_MAGNET,
})
Encyclopedia.AddTrinket({ -- 5.TRINKET_BROKEN_REMOTE
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROKEN_REMOTE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROKEN_REMOTE,
})
Encyclopedia.AddTrinket({ -- 7.TRINKET_ROSARY_BEAD
    Class = "vanilla",
    ID = TrinketType.TRINKET_ROSARY_BEAD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ROSARY_BEAD,
})
Encyclopedia.AddTrinket({ -- 8.TRINKET_CARTRIDGE
    Class = "vanilla",
    ID = TrinketType.TRINKET_CARTRIDGE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CARTRIDGE,
})
Encyclopedia.AddTrinket({ -- 9.TRINKET_PULSE_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_PULSE_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PULSE_WORM,
})
Encyclopedia.AddTrinket({ -- 10.TRINKET_WIGGLE_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_WIGGLE_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WIGGLE_WORM,
})
Encyclopedia.AddTrinket({ -- 11.TRINKET_RING_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_RING_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RING_WORM,
})
Encyclopedia.AddTrinket({ -- 12.TRINKET_FLAT_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_FLAT_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FLAT_WORM,
})
Encyclopedia.AddTrinket({ -- 13.TRINKET_STORE_CREDIT
    Class = "vanilla",
    ID = TrinketType.TRINKET_STORE_CREDIT,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_STORE_CREDIT,
})
Encyclopedia.AddTrinket({ -- 14.TRINKET_CALLUS
    Class = "vanilla",
    ID = TrinketType.TRINKET_CALLUS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CALLUS,
})
Encyclopedia.AddTrinket({ -- 15.TRINKET_LUCKY_ROCK
    Class = "vanilla",
    ID = TrinketType.TRINKET_LUCKY_ROCK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LUCKY_ROCK,
})
Encyclopedia.AddTrinket({ -- 16.TRINKET_MOMS_TOENAIL
    Class = "vanilla",
    ID = TrinketType.TRINKET_MOMS_TOENAIL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MOMS_TOENAIL,
})
Encyclopedia.AddTrinket({ -- 17.TRINKET_BLACK_LIPSTICK
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLACK_LIPSTICK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLACK_LIPSTICK,
})
Encyclopedia.AddTrinket({ -- 18.TRINKET_BIBLE_TRACT
    Class = "vanilla",
    ID = TrinketType.TRINKET_BIBLE_TRACT,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BIBLE_TRACT,
})
Encyclopedia.AddTrinket({ -- 19.TRINKET_PAPER_CLIP
    Class = "vanilla",
    ID = TrinketType.TRINKET_PAPER_CLIP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PAPER_CLIP,
})
Encyclopedia.AddTrinket({ -- 20.TRINKET_MONKEY_PAW
    Class = "vanilla",
    ID = TrinketType.TRINKET_MONKEY_PAW,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MONKEY_PAW,
})
Encyclopedia.AddTrinket({ -- 21.TRINKET_MYSTERIOUS_PAPER
    Class = "vanilla",
    ID = TrinketType.TRINKET_MYSTERIOUS_PAPER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MYSTERIOUS_PAPER,
})
Encyclopedia.AddTrinket({ -- 22.TRINKET_DAEMONS_TAIL
    Class = "vanilla",
    ID = TrinketType.TRINKET_DAEMONS_TAIL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DAEMONS_TAIL,
})
Encyclopedia.AddTrinket({ -- 23.TRINKET_MISSING_POSTER
    Class = "vanilla",
    ID = TrinketType.TRINKET_MISSING_POSTER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MISSING_POSTER,
})
Encyclopedia.AddTrinket({ -- 24.TRINKET_BUTT_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_BUTT_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BUTT_PENNY,
})
Encyclopedia.AddTrinket({ -- 25.TRINKET_MYSTERIOUS_CANDY
    Class = "vanilla",
    ID = TrinketType.TRINKET_MYSTERIOUS_CANDY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MYSTERIOUS_CANDY,
})
Encyclopedia.AddTrinket({ -- 26.TRINKET_HOOK_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_HOOK_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_HOOK_WORM,
})
Encyclopedia.AddTrinket({ -- 27.TRINKET_WHIP_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_WHIP_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WHIP_WORM,
})
Encyclopedia.AddTrinket({ -- 28.TRINKET_BROKEN_ANKH
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROKEN_ANKH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROKEN_ANKH,
})
Encyclopedia.AddTrinket({ -- 29.TRINKET_FISH_HEAD
    Class = "vanilla",
    ID = TrinketType.TRINKET_FISH_HEAD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FISH_HEAD,
})
Encyclopedia.AddTrinket({ -- 30.TRINKET_PINKY_EYE
    Class = "vanilla",
    ID = TrinketType.TRINKET_PINKY_EYE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PINKY_EYE,
})
Encyclopedia.AddTrinket({ -- 31.TRINKET_PUSH_PIN
    Class = "vanilla",
    ID = TrinketType.TRINKET_PUSH_PIN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PUSH_PIN,
})
Encyclopedia.AddTrinket({ -- 32.TRINKET_LIBERTY_CAP
    Class = "vanilla",
    ID = TrinketType.TRINKET_LIBERTY_CAP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LIBERTY_CAP,
})
Encyclopedia.AddTrinket({ -- 33.TRINKET_UMBILICAL_CORD
    Class = "vanilla",
    ID = TrinketType.TRINKET_UMBILICAL_CORD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_UMBILICAL_CORD,
})
Encyclopedia.AddTrinket({ -- 34.TRINKET_CHILDS_HEART
    Class = "vanilla",
    ID = TrinketType.TRINKET_CHILDS_HEART,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CHILDS_HEART,
})
Encyclopedia.AddTrinket({ -- 35.TRINKET_CURVED_HORN
    Class = "vanilla",
    ID = TrinketType.TRINKET_CURVED_HORN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CURVED_HORN,
})
Encyclopedia.AddTrinket({ -- 36.TRINKET_RUSTED_KEY
    Class = "vanilla",
    ID = TrinketType.TRINKET_RUSTED_KEY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RUSTED_KEY,
})
Encyclopedia.AddTrinket({ -- 37.TRINKET_GOAT_HOOF
    Class = "vanilla",
    ID = TrinketType.TRINKET_GOAT_HOOF,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_GOAT_HOOF,
})
Encyclopedia.AddTrinket({ -- 38.TRINKET_MOMS_PEARL
    Class = "vanilla",
    ID = TrinketType.TRINKET_MOMS_PEARL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MOMS_PEARL,
})
Encyclopedia.AddTrinket({ -- 39.TRINKET_CANCER
    Class = "vanilla",
    ID = TrinketType.TRINKET_CANCER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CANCER,
})
Encyclopedia.AddTrinket({ -- 40.TRINKET_RED_PATCH
    Class = "vanilla",
    ID = TrinketType.TRINKET_RED_PATCH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RED_PATCH,
})
Encyclopedia.AddTrinket({ -- 41.TRINKET_MATCH_STICK
    Class = "vanilla",
    ID = TrinketType.TRINKET_MATCH_STICK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MATCH_STICK,
})
Encyclopedia.AddTrinket({ -- 42.TRINKET_LUCKY_TOE
    Class = "vanilla",
    ID = TrinketType.TRINKET_LUCKY_TOE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LUCKY_TOE,
})
Encyclopedia.AddTrinket({ -- 43.TRINKET_CURSED_SKULL
    Class = "vanilla",
    ID = TrinketType.TRINKET_CURSED_SKULL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CURSED_SKULL,
})
Encyclopedia.AddTrinket({ -- 44.TRINKET_SAFETY_CAP
    Class = "vanilla",
    ID = TrinketType.TRINKET_SAFETY_CAP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SAFETY_CAP,
})
Encyclopedia.AddTrinket({ -- 45.TRINKET_ACE_SPADES
    Class = "vanilla",
    ID = TrinketType.TRINKET_ACE_SPADES,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ACE_SPADES,
})
Encyclopedia.AddTrinket({ -- 46.TRINKET_ISAACS_FORK
    Class = "vanilla",
    ID = TrinketType.TRINKET_ISAACS_FORK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ISAACS_FORK,
})
--[[Encyclopedia.AddTrinket({ -- 47.TRINKET_POLAROID_OBSOLETE
    Class = "vanilla",
    ID = TrinketType.TRINKET_POLAROID_OBSOLETE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_POLAROID_OBSOLETE,
})]]
Encyclopedia.AddTrinket({ -- 48.TRINKET_MISSING_PAGE
    Class = "vanilla",
    ID = TrinketType.TRINKET_MISSING_PAGE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MISSING_PAGE,
})
Encyclopedia.AddTrinket({ -- 49.TRINKET_BLOODY_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLOODY_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLOODY_PENNY,
})
Encyclopedia.AddTrinket({ -- 50.TRINKET_BURNT_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_BURNT_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BURNT_PENNY,
})
Encyclopedia.AddTrinket({ -- 52.TRINKET_FLAT_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_FLAT_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FLAT_PENNY,
})
Encyclopedia.AddTrinket({ -- 51.TRINKET_COUNTERFEIT_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_COUNTERFEIT_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_COUNTERFEIT_PENNY,
})
Encyclopedia.AddTrinket({ -- 53.TRINKET_TICK
    Class = "vanilla",
    ID = TrinketType.TRINKET_TICK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TICK,
})
Encyclopedia.AddTrinket({ -- 54.TRINKET_ISAACS_HEAD
    Class = "vanilla",
    ID = TrinketType.TRINKET_ISAACS_HEAD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ISAACS_HEAD,
})
Encyclopedia.AddTrinket({ -- 55.TRINKET_MAGGYS_FAITH
    Class = "vanilla",
    ID = TrinketType.TRINKET_MAGGYS_FAITH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MAGGYS_FAITH,
})
Encyclopedia.AddTrinket({ -- 56.TRINKET_JUDAS_TONGUE
    Class = "vanilla",
    ID = TrinketType.TRINKET_JUDAS_TONGUE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_JUDAS_TONGUE,
})
Encyclopedia.AddTrinket({ -- 57.TRINKET_SOUL
    Class = "vanilla",
    ID = TrinketType.TRINKET_SOUL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SOUL,
})
Encyclopedia.AddTrinket({ -- 58.TRINKET_SAMSONS_LOCK
    Class = "vanilla",
    ID = TrinketType.TRINKET_SAMSONS_LOCK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SAMSONS_LOCK,
})
Encyclopedia.AddTrinket({ -- 59.TRINKET_CAINS_EYE
    Class = "vanilla",
    ID = TrinketType.TRINKET_CAINS_EYE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CAINS_EYE,
})
Encyclopedia.AddTrinket({ -- 60.TRINKET_EVES_BIRD_FOOT
    Class = "vanilla",
    ID = TrinketType.TRINKET_EVES_BIRD_FOOT,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_EVES_BIRD_FOOT,
})
Encyclopedia.AddTrinket({ -- 61.TRINKET_LEFT_HAND
    Class = "vanilla",
    ID = TrinketType.TRINKET_LEFT_HAND,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LEFT_HAND,
})
Encyclopedia.AddTrinket({ -- 62.TRINKET_SHINY_ROCK
    Class = "vanilla",
    ID = TrinketType.TRINKET_SHINY_ROCK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SHINY_ROCK,
})
Encyclopedia.AddTrinket({ -- 63.TRINKET_SAFETY_SCISSORS
    Class = "vanilla",
    ID = TrinketType.TRINKET_SAFETY_SCISSORS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SAFETY_SCISSORS,
})
Encyclopedia.AddTrinket({ -- 64.TRINKET_RAINBOW_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_RAINBOW_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RAINBOW_WORM,
})
Encyclopedia.AddTrinket({ -- 65.TRINKET_TAPE_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_TAPE_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TAPE_WORM,
})
Encyclopedia.AddTrinket({ -- 66.TRINKET_LAZY_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_LAZY_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LAZY_WORM,
})
Encyclopedia.AddTrinket({ -- 67.TRINKET_CRACKED_DICE
    Class = "vanilla",
    ID = TrinketType.TRINKET_CRACKED_DICE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CRACKED_DICE,
})
Encyclopedia.AddTrinket({ -- 68.TRINKET_SUPER_MAGNET
    Class = "vanilla",
    ID = TrinketType.TRINKET_SUPER_MAGNET,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SUPER_MAGNET,
})
Encyclopedia.AddTrinket({ -- 69.TRINKET_FADED_POLAROID
    Class = "vanilla",
    ID = TrinketType.TRINKET_FADED_POLAROID,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FADED_POLAROID,
})
Encyclopedia.AddTrinket({ -- 70.TRINKET_LOUSE
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOUSE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOUSE,
})
Encyclopedia.AddTrinket({ -- 71.TRINKET_BOBS_BLADDER
    Class = "vanilla",
    ID = TrinketType.TRINKET_BOBS_BLADDER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BOBS_BLADDER,
})
Encyclopedia.AddTrinket({ -- 72.TRINKET_WATCH_BATTERY
    Class = "vanilla",
    ID = TrinketType.TRINKET_WATCH_BATTERY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WATCH_BATTERY,
})
Encyclopedia.AddTrinket({ -- 73.TRINKET_BLASTING_CAP
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLASTING_CAP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLASTING_CAP,
})
Encyclopedia.AddTrinket({ -- 74.TRINKET_STUD_FINDER
    Class = "vanilla",
    ID = TrinketType.TRINKET_STUD_FINDER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_STUD_FINDER,
})
Encyclopedia.AddTrinket({ -- 75.TRINKET_ERROR
    Class = "vanilla",
    ID = TrinketType.TRINKET_ERROR,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ERROR,
})
Encyclopedia.AddTrinket({ -- 76.TRINKET_POKER_CHIP
    Class = "vanilla",
    ID = TrinketType.TRINKET_POKER_CHIP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_POKER_CHIP,
})
Encyclopedia.AddTrinket({ -- 77.TRINKET_BLISTER
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLISTER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLISTER,
})
Encyclopedia.AddTrinket({ -- 79.TRINKET_SECOND_HAND
    Class = "vanilla",
    ID = TrinketType.TRINKET_SECOND_HAND,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SECOND_HAND,
})
Encyclopedia.AddTrinket({ -- 78.TRINKET_ENDLESS_NAMELESS
    Class = "vanilla",
    ID = TrinketType.TRINKET_ENDLESS_NAMELESS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ENDLESS_NAMELESS,
})
Encyclopedia.AddTrinket({ -- 80.TRINKET_BLACK_FEATHER
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLACK_FEATHER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLACK_FEATHER,
})
Encyclopedia.AddTrinket({ -- 81.TRINKET_BLIND_RAGE
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLIND_RAGE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLIND_RAGE,
})
Encyclopedia.AddTrinket({ -- 82.TRINKET_GOLDEN_HORSE_SHOE
    Class = "vanilla",
    ID = TrinketType.TRINKET_GOLDEN_HORSE_SHOE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_GOLDEN_HORSE_SHOE,
})
Encyclopedia.AddTrinket({ -- 83.TRINKET_STORE_KEY
    Class = "vanilla",
    ID = TrinketType.TRINKET_STORE_KEY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_STORE_KEY,
})
Encyclopedia.AddTrinket({ -- 84.TRINKET_RIB_OF_GREED
    Class = "vanilla",
    ID = TrinketType.TRINKET_RIB_OF_GREED,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RIB_OF_GREED,
})
Encyclopedia.AddTrinket({ -- 85.TRINKET_KARMA
    Class = "vanilla",
    ID = TrinketType.TRINKET_KARMA,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_KARMA,
})
Encyclopedia.AddTrinket({ -- 86.TRINKET_LIL_LARVA
    Class = "vanilla",
    ID = TrinketType.TRINKET_LIL_LARVA,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LIL_LARVA,
})
Encyclopedia.AddTrinket({ -- 87.TRINKET_MOMS_LOCKET
    Class = "vanilla",
    ID = TrinketType.TRINKET_MOMS_LOCKET,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MOMS_LOCKET,
})
Encyclopedia.AddTrinket({ -- 88.TRINKET_NO
    Class = "vanilla",
    ID = TrinketType.TRINKET_NO,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_NO,
})
Encyclopedia.AddTrinket({ -- 89.TRINKET_CHILD_LEASH
    Class = "vanilla",
    ID = TrinketType.TRINKET_CHILD_LEASH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CHILD_LEASH,
})
Encyclopedia.AddTrinket({ -- 93.TRINKET_CRACKED_CROWN
    Class = "vanilla",
    ID = TrinketType.TRINKET_CRACKED_CROWN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CRACKED_CROWN,
})
Encyclopedia.AddTrinket({ -- 90.TRINKET_USED_DIAPER
    Class = "vanilla",
    ID = TrinketType.TRINKET_USED_DIAPER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_USED_DIAPER,
})
Encyclopedia.AddTrinket({ -- 91.TRINKET_BROWN_CAP
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROWN_CAP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROWN_CAP,
})
Encyclopedia.AddTrinket({ -- 92.TRINKET_MECONIUM
    Class = "vanilla",
    ID = TrinketType.TRINKET_MECONIUM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MECONIUM,
})
Encyclopedia.AddTrinket({ -- 94.TRINKET_FISH_TAIL
    Class = "vanilla",
    ID = TrinketType.TRINKET_FISH_TAIL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FISH_TAIL,
})
Encyclopedia.AddTrinket({ -- 95.TRINKET_BLACK_TOOTH
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLACK_TOOTH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLACK_TOOTH,
})
Encyclopedia.AddTrinket({ -- 96.TRINKET_OUROBOROS_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_OUROBOROS_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_OUROBOROS_WORM,
})
Encyclopedia.AddTrinket({ -- 97.TRINKET_TONSIL
    Class = "vanilla",
    ID = TrinketType.TRINKET_TONSIL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TONSIL,
})
Encyclopedia.AddTrinket({ -- 98.TRINKET_NOSE_GOBLIN
    Class = "vanilla",
    ID = TrinketType.TRINKET_NOSE_GOBLIN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_NOSE_GOBLIN,
})
Encyclopedia.AddTrinket({ -- 99.TRINKET_SUPER_BALL
    Class = "vanilla",
    ID = TrinketType.TRINKET_SUPER_BALL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SUPER_BALL,
})
Encyclopedia.AddTrinket({ -- 100.TRINKET_VIBRANT_BULB
    Class = "vanilla",
    ID = TrinketType.TRINKET_VIBRANT_BULB,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_VIBRANT_BULB,
})
Encyclopedia.AddTrinket({ -- 101.TRINKET_DIM_BULB
    Class = "vanilla",
    ID = TrinketType.TRINKET_DIM_BULB,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DIM_BULB,
})
Encyclopedia.AddTrinket({ -- 102.TRINKET_FRAGMENTED_CARD
    Class = "vanilla",
    ID = TrinketType.TRINKET_FRAGMENTED_CARD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FRAGMENTED_CARD,
})
Encyclopedia.AddTrinket({ -- 103.TRINKET_EQUALITY
    Class = "vanilla",
    ID = TrinketType.TRINKET_EQUALITY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_EQUALITY,
})
Encyclopedia.AddTrinket({ -- 104.TRINKET_WISH_BONE
    Class = "vanilla",
    ID = TrinketType.TRINKET_WISH_BONE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WISH_BONE,
})
Encyclopedia.AddTrinket({ -- 105.TRINKET_BAG_LUNCH
    Class = "vanilla",
    ID = TrinketType.TRINKET_BAG_LUNCH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BAG_LUNCH,
})
Encyclopedia.AddTrinket({ -- 106.TRINKET_LOST_CORK
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOST_CORK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOST_CORK,
})
Encyclopedia.AddTrinket({ -- 107.TRINKET_CROW_HEART
    Class = "vanilla",
    ID = TrinketType.TRINKET_CROW_HEART,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CROW_HEART,
})
Encyclopedia.AddTrinket({ -- 108.TRINKET_WALNUT
    Class = "vanilla",
    ID = TrinketType.TRINKET_WALNUT,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WALNUT,
})
Encyclopedia.AddTrinket({ -- 109.TRINKET_DUCT_TAPE
    Class = "vanilla",
    ID = TrinketType.TRINKET_DUCT_TAPE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DUCT_TAPE,
})
Encyclopedia.AddTrinket({ -- 110.TRINKET_SILVER_DOLLAR
    Class = "vanilla",
    ID = TrinketType.TRINKET_SILVER_DOLLAR,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SILVER_DOLLAR,
})
Encyclopedia.AddTrinket({ -- 111.TRINKET_BLOODY_CROWN
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLOODY_CROWN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLOODY_CROWN,
})
Encyclopedia.AddTrinket({ -- 112.TRINKET_PAY_TO_WIN
    Class = "vanilla",
    ID = TrinketType.TRINKET_PAY_TO_WIN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PAY_TO_WIN,
})
Encyclopedia.AddTrinket({ -- 113.TRINKET_LOCUST_OF_WRATH
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOCUST_OF_WRATH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOCUST_OF_WRATH,
})
Encyclopedia.AddTrinket({ -- 114.TRINKET_LOCUST_OF_PESTILENCE
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOCUST_OF_PESTILENCE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOCUST_OF_PESTILENCE,
})
Encyclopedia.AddTrinket({ -- 115.TRINKET_LOCUST_OF_FAMINE
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOCUST_OF_FAMINE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOCUST_OF_FAMINE,
})
Encyclopedia.AddTrinket({ -- 116.TRINKET_LOCUST_OF_DEATH
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOCUST_OF_DEATH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOCUST_OF_DEATH,
})
Encyclopedia.AddTrinket({ -- 117.TRINKET_LOCUST_OF_CONQUEST
    Class = "vanilla",
    ID = TrinketType.TRINKET_LOCUST_OF_CONQUEST,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LOCUST_OF_CONQUEST,
})
Encyclopedia.AddTrinket({ -- 118.TRINKET_BAT_WING
    Class = "vanilla",
    ID = TrinketType.TRINKET_BAT_WING,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BAT_WING,
})
Encyclopedia.AddTrinket({ -- 119.TRINKET_STEM_CELL
    Class = "vanilla",
    ID = TrinketType.TRINKET_STEM_CELL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_STEM_CELL,
})
Encyclopedia.AddTrinket({ -- 120.TRINKET_HAIRPIN
    Class = "vanilla",
    ID = TrinketType.TRINKET_HAIRPIN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_HAIRPIN,
})
Encyclopedia.AddTrinket({ -- 121.TRINKET_WOODEN_CROSS
    Class = "vanilla",
    ID = TrinketType.TRINKET_WOODEN_CROSS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WOODEN_CROSS,
})
Encyclopedia.AddTrinket({ -- 122.TRINKET_BUTTER
    Class = "vanilla",
    ID = TrinketType.TRINKET_BUTTER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BUTTER,
})
Encyclopedia.AddTrinket({ -- 123.TRINKET_FILIGREE_FEATHERS
    Class = "vanilla",
    ID = TrinketType.TRINKET_FILIGREE_FEATHERS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FILIGREE_FEATHERS,
})
Encyclopedia.AddTrinket({ -- 124.TRINKET_DOOR_STOP
    Class = "vanilla",
    ID = TrinketType.TRINKET_DOOR_STOP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DOOR_STOP,
})
Encyclopedia.AddTrinket({ -- 125.TRINKET_EXTENSION_CORD
    Class = "vanilla",
    ID = TrinketType.TRINKET_EXTENSION_CORD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_EXTENSION_CORD,
})
Encyclopedia.AddTrinket({ -- 126.TRINKET_ROTTEN_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_ROTTEN_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ROTTEN_PENNY,
})
Encyclopedia.AddTrinket({ -- 127.TRINKET_BABY_BENDER
    Class = "vanilla",
    ID = TrinketType.TRINKET_BABY_BENDER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BABY_BENDER,
})
Encyclopedia.AddTrinket({ -- 128.TRINKET_FINGER_BONE
    Class = "vanilla",
    ID = TrinketType.TRINKET_FINGER_BONE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FINGER_BONE,
})

if REPENTANCE then
Encyclopedia.AddTrinket({ -- 129.TRINKET_JAW_BREAKER
    Class = "vanilla",
    ID = TrinketType.TRINKET_JAW_BREAKER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_JAW_BREAKER,
})
Encyclopedia.AddTrinket({ -- 132.TRINKET_BLESSED_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLESSED_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLESSED_PENNY,
})
Encyclopedia.AddTrinket({ -- 130.TRINKET_BROKEN_SYRINGE
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROKEN_SYRINGE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROKEN_SYRINGE,
})
Encyclopedia.AddTrinket({ -- 131.TRINKET_CHEWED_PEN
    Class = "vanilla",
    ID = TrinketType.TRINKET_CHEWED_PEN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CHEWED_PEN,
})
Encyclopedia.AddTrinket({ -- 133.TRINKET_FIRECRACKER
    Class = "vanilla",
    ID = TrinketType.TRINKET_FIRECRACKER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FIRECRACKER,
})
Encyclopedia.AddTrinket({ -- 134.TRINKET_GIANT_BEAN
    Class = "vanilla",
    ID = TrinketType.TRINKET_GIANT_BEAN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_GIANT_BEAN,
})
Encyclopedia.AddTrinket({ -- 135.TRINKET_LIGHTER
    Class = "vanilla",
    ID = TrinketType.TRINKET_LIGHTER,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LIGHTER,
})
Encyclopedia.AddTrinket({ -- 137.TRINKET_BROKEN_PADLOCK
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROKEN_PADLOCK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROKEN_PADLOCK,
})
Encyclopedia.AddTrinket({ -- 136.TRINKET_MYOSOTIS
    Class = "vanilla",
    ID = TrinketType.TRINKET_MYOSOTIS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MYOSOTIS,
})
Encyclopedia.AddTrinket({ -- 138.TRINKET_M
    Class = "vanilla",
    ID = TrinketType.TRINKET_M,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_M,
})
Encyclopedia.AddTrinket({ -- 139.TRINKET_TEARDROP_CHARM
    Class = "vanilla",
    ID = TrinketType.TRINKET_TEARDROP_CHARM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TEARDROP_CHARM,
})
Encyclopedia.AddTrinket({ -- 140.TRINKET_APPLE_OF_SODOM
    Class = "vanilla",
    ID = TrinketType.TRINKET_APPLE_OF_SODOM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_APPLE_OF_SODOM,
})
Encyclopedia.AddTrinket({ -- 141.TRINKET_FORGOTTEN_LULLABY
    Class = "vanilla",
    ID = TrinketType.TRINKET_FORGOTTEN_LULLABY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FORGOTTEN_LULLABY,
})
Encyclopedia.AddTrinket({ -- 142.TRINKET_BETHS_FAITH
    Class = "vanilla",
    ID = TrinketType.TRINKET_BETHS_FAITH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BETHS_FAITH,
})
Encyclopedia.AddTrinket({ -- 143.TRINKET_OLD_CAPACITOR
    Class = "vanilla",
    ID = TrinketType.TRINKET_OLD_CAPACITOR,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_OLD_CAPACITOR,
})
Encyclopedia.AddTrinket({ -- 144.TRINKET_BRAIN_WORM
    Class = "vanilla",
    ID = TrinketType.TRINKET_BRAIN_WORM,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BRAIN_WORM,
})
Encyclopedia.AddTrinket({ -- 145.TRINKET_PERFECTION
    Class = "vanilla",
    ID = TrinketType.TRINKET_PERFECTION,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PERFECTION,
})
Encyclopedia.AddTrinket({ -- 146.TRINKET_DEVILS_CROWN
    Class = "vanilla",
    ID = TrinketType.TRINKET_DEVILS_CROWN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DEVILS_CROWN,
})
Encyclopedia.AddTrinket({ -- 147.TRINKET_CHARGED_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_CHARGED_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CHARGED_PENNY,
})
Encyclopedia.AddTrinket({ -- 148.TRINKET_FRIENDSHIP_NECKLACE
    Class = "vanilla",
    ID = TrinketType.TRINKET_FRIENDSHIP_NECKLACE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FRIENDSHIP_NECKLACE,
})
Encyclopedia.AddTrinket({ -- 149.TRINKET_PANIC_BUTTON
    Class = "vanilla",
    ID = TrinketType.TRINKET_PANIC_BUTTON,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_PANIC_BUTTON,
})
Encyclopedia.AddTrinket({ -- 150.TRINKET_BLUE_KEY
    Class = "vanilla",
    ID = TrinketType.TRINKET_BLUE_KEY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BLUE_KEY,
})
Encyclopedia.AddTrinket({ -- 151.TRINKET_FLAT_FILE
    Class = "vanilla",
    ID = TrinketType.TRINKET_FLAT_FILE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FLAT_FILE,
})
Encyclopedia.AddTrinket({ -- 152.TRINKET_TELESCOPE_LENS
    Class = "vanilla",
    ID = TrinketType.TRINKET_TELESCOPE_LENS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TELESCOPE_LENS,
})
Encyclopedia.AddTrinket({ -- 153.TRINKET_MOMS_LOCK
    Class = "vanilla",
    ID = TrinketType.TRINKET_MOMS_LOCK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MOMS_LOCK,
})
Encyclopedia.AddTrinket({ -- 154.TRINKET_DICE_BAG
    Class = "vanilla",
    ID = TrinketType.TRINKET_DICE_BAG,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DICE_BAG,
})
Encyclopedia.AddTrinket({ -- 155.TRINKET_HOLY_CROWN
    Class = "vanilla",
    ID = TrinketType.TRINKET_HOLY_CROWN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_HOLY_CROWN,
})
Encyclopedia.AddTrinket({ -- 156.TRINKET_MOTHERS_KISS
    Class = "vanilla",
    ID = TrinketType.TRINKET_MOTHERS_KISS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MOTHERS_KISS,
})
Encyclopedia.AddTrinket({ -- 157.TRINKET_TORN_CARD
    Class = "vanilla",
    ID = TrinketType.TRINKET_TORN_CARD,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TORN_CARD,
})
Encyclopedia.AddTrinket({ -- 158.TRINKET_TORN_POCKET
    Class = "vanilla",
    ID = TrinketType.TRINKET_TORN_POCKET,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TORN_POCKET,
})
Encyclopedia.AddTrinket({ -- 159.TRINKET_GILDED_KEY
    Class = "vanilla",
    ID = TrinketType.TRINKET_GILDED_KEY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_GILDED_KEY,
})
Encyclopedia.AddTrinket({ -- 160.TRINKET_LUCKY_SACK
    Class = "vanilla",
    ID = TrinketType.TRINKET_LUCKY_SACK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LUCKY_SACK,
})
Encyclopedia.AddTrinket({ -- 161.TRINKET_WICKED_CROWN
    Class = "vanilla",
    ID = TrinketType.TRINKET_WICKED_CROWN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_WICKED_CROWN,
})
Encyclopedia.AddTrinket({ -- 162.TRINKET_AZAZELS_STUMP
    Class = "vanilla",
    ID = TrinketType.TRINKET_AZAZELS_STUMP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_AZAZELS_STUMP,
})
Encyclopedia.AddTrinket({ -- 163.TRINKET_DINGLE_BERRY
    Class = "vanilla",
    ID = TrinketType.TRINKET_DINGLE_BERRY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_DINGLE_BERRY,
})
Encyclopedia.AddTrinket({ -- 164.TRINKET_RING_CAP
    Class = "vanilla",
    ID = TrinketType.TRINKET_RING_CAP,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RING_CAP,
})
Encyclopedia.AddTrinket({ -- 165.TRINKET_NUH_UH
    Class = "vanilla",
    ID = TrinketType.TRINKET_NUH_UH,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_NUH_UH,
})
Encyclopedia.AddTrinket({ -- 166.TRINKET_MODELING_CLAY
    Class = "vanilla",
    ID = TrinketType.TRINKET_MODELING_CLAY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_MODELING_CLAY,
})
Encyclopedia.AddTrinket({ -- 167.TRINKET_POLISHED_BONE
    Class = "vanilla",
    ID = TrinketType.TRINKET_POLISHED_BONE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_POLISHED_BONE,
})
Encyclopedia.AddTrinket({ -- 168.TRINKET_HOLLOW_HEART
    Class = "vanilla",
    ID = TrinketType.TRINKET_HOLLOW_HEART,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_HOLLOW_HEART,
})
Encyclopedia.AddTrinket({ -- 169.TRINKET_KIDS_DRAWING
    Class = "vanilla",
    ID = TrinketType.TRINKET_KIDS_DRAWING,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_KIDS_DRAWING,
})
Encyclopedia.AddTrinket({ -- 170.TRINKET_CRYSTAL_KEY
    Class = "vanilla",
    ID = TrinketType.TRINKET_CRYSTAL_KEY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CRYSTAL_KEY,
})
Encyclopedia.AddTrinket({ -- 171.TRINKET_KEEPERS_BARGAIN
    Class = "vanilla",
    ID = TrinketType.TRINKET_KEEPERS_BARGAIN,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_KEEPERS_BARGAIN,
})
Encyclopedia.AddTrinket({ -- 172.TRINKET_CURSED_PENNY
    Class = "vanilla",
    ID = TrinketType.TRINKET_CURSED_PENNY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CURSED_PENNY,
})
Encyclopedia.AddTrinket({ -- 173.TRINKET_YOUR_SOUL
    Class = "vanilla",
    ID = TrinketType.TRINKET_YOUR_SOUL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_YOUR_SOUL,
})
Encyclopedia.AddTrinket({ -- 174.TRINKET_NUMBER_MAGNET
    Class = "vanilla",
    ID = TrinketType.TRINKET_NUMBER_MAGNET,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_NUMBER_MAGNET,
})
Encyclopedia.AddTrinket({ -- 176.TRINKET_STRANGE_KEY
    Class = "vanilla",
    ID = TrinketType.TRINKET_STRANGE_KEY,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_STRANGE_KEY,
})
Encyclopedia.AddTrinket({ -- 175.TRINKET_LIL_CLOT
    Class = "vanilla",
    ID = TrinketType.TRINKET_LIL_CLOT,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_LIL_CLOT,
})
Encyclopedia.AddTrinket({ -- 177.TRINKET_TEMPORARY_TATTOO
    Class = "vanilla",
    ID = TrinketType.TRINKET_TEMPORARY_TATTOO,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_TEMPORARY_TATTOO,
})
Encyclopedia.AddTrinket({ -- 178.TRINKET_SWALLOWED_M80
    Class = "vanilla",
    ID = TrinketType.TRINKET_SWALLOWED_M80,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SWALLOWED_M80,
})
Encyclopedia.AddTrinket({ -- 179.TRINKET_RC_REMOTE
    Class = "vanilla",
    ID = TrinketType.TRINKET_RC_REMOTE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_RC_REMOTE,
})
Encyclopedia.AddTrinket({ -- 180.TRINKET_FOUND_SOUL
    Class = "vanilla",
    ID = TrinketType.TRINKET_FOUND_SOUL,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_FOUND_SOUL,
})
Encyclopedia.AddTrinket({ -- 181.TRINKET_EXPANSION_PACK
    Class = "vanilla",
    ID = TrinketType.TRINKET_EXPANSION_PACK,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_EXPANSION_PACK,
})
Encyclopedia.AddTrinket({ -- 182.TRINKET_BETHS_ESSENCE
    Class = "vanilla",
    ID = TrinketType.TRINKET_BETHS_ESSENCE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BETHS_ESSENCE,
})
Encyclopedia.AddTrinket({ -- 183.TRINKET_THE_TWINS
    Class = "vanilla",
    ID = TrinketType.TRINKET_THE_TWINS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_THE_TWINS,
})
Encyclopedia.AddTrinket({ -- 184.TRINKET_ADOPTION_PAPERS
    Class = "vanilla",
    ID = TrinketType.TRINKET_ADOPTION_PAPERS,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ADOPTION_PAPERS,
})
Encyclopedia.AddTrinket({ -- 185.TRINKET_CRICKET_LEG
    Class = "vanilla",
    ID = TrinketType.TRINKET_CRICKET_LEG,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_CRICKET_LEG,
})
Encyclopedia.AddTrinket({ -- 186.TRINKET_APOLLYONS_BEST_FRIEND
    Class = "vanilla",
    ID = TrinketType.TRINKET_APOLLYONS_BEST_FRIEND,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_APOLLYONS_BEST_FRIEND,
})
Encyclopedia.AddTrinket({ -- 187.TRINKET_BROKEN_GLASSES
    Class = "vanilla",
    ID = TrinketType.TRINKET_BROKEN_GLASSES,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_BROKEN_GLASSES,
})
Encyclopedia.AddTrinket({ -- 188.TRINKET_ICE_CUBE
    Class = "vanilla",
    ID = TrinketType.TRINKET_ICE_CUBE,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_ICE_CUBE,
})
Encyclopedia.AddTrinket({ -- 189.TRINKET_SIGIL_OF_BAPHOMET
    Class = "vanilla",
    ID = TrinketType.TRINKET_SIGIL_OF_BAPHOMET,
    WikiDesc = Encyclopedia.TrinketsWiki.TRINKET_SIGIL_OF_BAPHOMET,
})
end